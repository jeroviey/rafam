$(function(){ 
	$("[data-toggle='tooltip']").tooltip(); 

	$('#confirm-modal').on('show.bs.modal', function(e) {
		if($(e.relatedTarget).data('boton'))
		{
			$(this).find('.danger').html($(e.relatedTarget).data('boton'));
		}
		if($(e.relatedTarget).data('callback'))
		{
			$(this).find('.danger').unbind('click');
			$(this).find('.danger').click(function() {
				borrar_callback($(e.relatedTarget).data('callback'), $(e.relatedTarget).data('callbackparam'));
				$('#confirm-modal').modal("hide")
			});
		}
		else
		{
	        $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
		}
        $('.modal-body').html($(e.relatedTarget).data('confirm'));    
    });

    $('#copy-modal').on('show.bs.modal', function(e) {
    	$(this).find('#copy_input').val($(e.relatedTarget).data('link'));  
    });

    function borrar_callback(funcion, parametro)
	{
		window[funcion](parametro);
	}
});