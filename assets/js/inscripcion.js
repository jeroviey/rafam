$(function(){
		$("#tipo_trabajo_1").on("change", function(e) {
			if($(this).is(':checked')){
				$("#campo_municipio").show();
				$("#campo_lugar_trabajo").hide();
			}
			else{
				$("#campo_municipio").hide();
				$("#campo_lugar_trabajo").show();
			}

		});
		$("#tipo_trabajo_2").on("change", function(e) {
			if($(this).is(':checked')){
				$("#campo_municipio").hide();
				$("#campo_lugar_trabajo").show();
			}
			else{
				$("#campo_municipio").show();
				$("#campo_lugar_trabajo").hide();
			}
		});
		
		$("#curso").on("change", function(e) {
			var info = $(this).find("option:selected").data('infoclases');
			$('#clase').empty();
			if($(this).val() != '0'){
				$('#clase').prop('disabled', false);
				$('#clase').append('<option value="0" >Clase</option>');
			}
			/*else{
				$('#clase').prop('disabled', 'disabled');
			}*/
			var info_partes = info.split('|');
			var selected = $('#clase').data('selected');
        	for(var i = 0; i < info_partes.length; i++)
        	{
        		var clase = info_partes[i].split('=');
        		$('#clase').append('<option value="'+clase[0]+'" '+((clase[0] == selected) ? 'selected' : '')+'> '+clase[1]+' </option>');
        	}
		});

		if($("#curso").val() != '0')
		{
			$("#curso").change();
		}
});