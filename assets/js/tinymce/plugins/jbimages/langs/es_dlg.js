/**
 * Justboil.me - a TinyMCE image upload plugin
 * jbimages/langs/fr_dlg.js
 *
 * Released under Creative Commons Attribution 3.0 Unported License
 *
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: http://justboil.me/
 * Author: Viktor Kuzhelnyi
 *
 * Version: 2.3 released 23/06/2013
 */

 tinyMCE.addI18n('es.jbimages_dlg',{
	title : 'Cargar una imagen desde su computadora',
	select_an_image : 'Seleccione una imagen',
	upload_in_progress : 'Upload en progreso',
	upload_complete : 'Carga completa',
	upload : 'Cargar',
	longer_than_usual : 'Esto está tardando más de lo habitual.',
	maybe_an_error : 'Puede haber producido un error.',
	view_output : 'Ver la salida de secuencia de comandos',
	
	lang_id : 'spanish' /* php-side language files are in: ci/application/language/{lang_id}; and in ci/system/language/{lang_id} */
});