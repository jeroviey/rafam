<?php

$lang['imglib_source_image_required'] = "You must specify a source image in your preferences.";
$lang['imglib_gd_required'] = "The GD image library is required for this feature.";
$lang['imglib_gd_required_for_props'] = "Your server must support the GD image library in order to determine the image properties.";
$lang['imglib_unsupported_imagecreate'] = "Your server does not support the GD function required to process this type of image.";
$lang['imglib_gif_not_supported'] = "GIF images are often not supported due to licensing restrictions.  You may have to use JPG or PNG images instead.";
$lang['imglib_jpg_not_supported'] = "JPG images are not supported.";
$lang['imglib_png_not_supported'] = "PNG images are not supported.";
$lang['imglib_jpg_or_png_required'] = "El protocolo de cambio de tamaño de imagen especificado en las preferencias sólo funciona con JPEG o PNG tipos de imágenes.";
$lang['imglib_copy_error'] = "Se ha producido un error al intentar reemplazar el archivo. Por favor, asegúrese de que su directorio de archivos se puede escribir.";
$lang['imglib_rotate_unsupported'] = "Rotación de la imagen no parece ser soportada por su servidor.";
$lang['imglib_libpath_invalid'] = "El camino a la biblioteca de la imagen no es correcto. Por favor, establecer la ruta correcta en las preferencias de la imagen.";
$lang['imglib_image_process_failed'] = "El procesamiento de imágenes falló. Por favor, compruebe que su servidor soporta el protocolo elegido y que el camino a la biblioteca de la imagen es correcta.";
$lang['imglib_rotation_angle_required'] = "Se requiere un ángulo de rotación para girar la imagen.";
$lang['imglib_writing_failed_gif'] = "imagen GIF.";
$lang['imglib_invalid_path'] = "La ruta a la imagen no es correcta.";
$lang['imglib_copy_failed'] = "La copia de rutina imagen fracasó.";
$lang['imglib_missing_font'] = "No se puede encontrar un tipo de letra a utilizar.";
$lang['imglib_save_failed'] = "La carpeta de destino de la carga no parece tener permisos de escritura.";


/* End of file imglib_lang.php */
/* Location: ./system/language/english/imglib_lang.php */