<?php

$lang['jb_blankpage_message'] = "El proceso de inicio aún no ha comenzado o ha comenzado y todavía no ha terminado, o simplemente no hay conexión con el servidor remoto.";

/* End of file jbstrings_lang.php */
/* Location: ./application/language/russian/jbstrings_lang.php */
