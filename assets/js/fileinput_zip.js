$("#file-1").fileinput({
	previewFileType:'any',
    allowedFileExtensions : ['zip', 'rar'],
    overwriteInitial: false,
    maxFileSize: 1000000,
    slugCallback: function(filename) 
    {
        return filename.replace('(', '_').replace(']', '_');
    }
});