function newMessage(resp) 
{
  var $tpl = $($('#message-view').html()); 
  $('.panel_json').append($tpl);      
  var obj = JSON.parse(resp);
  $('#de').append(obj['de']);
  $('#lugar_trabajo').append(obj['lugar_trabajo']);
  $('#oficina').append(obj['oficina']);
  $('#cargo').append(obj['cargo']);
  $('#asunto').append(obj['asunto']);
  $('#cuerpo').append(obj['cuerpo']);
  $('#link_solucionado').attr('data-href', $('#link_solucionado').data('url')+obj['id_contacto']);
  if(obj['esta_solucionado'])
  {
    $('#solucionado').show();
    $('#no_solucionado').hide();
  }
  else
  {
    $('#no_solucionado').show();
    $('#solucionado').hide();
  }
}

$(function()
  {
    $(".mostrar_mensaje").on('click', function()
    {
      var enlace = $(this);

      $('li.active').removeClass('active');
      $('li.'+enlace.data('identificador')).addClass('active');
      $.ajax({
          url: enlace.data('url')
      })
      .done(function(resp) {
          $(".panel_json").empty();
          newMessage(resp);
      })
      .fail(function() {
          alert( "error" );
          $("#dvloader").hide();
      });
    });
  }
);