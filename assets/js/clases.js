$(function(){
		$("#agregar_clase").on("click", function(e) {

		    var clase = $("tr[id='template_clase']").clone();
		    var index = $("input[id='clase_index']").val();
		    var index_name = '['+index+']';
		    var next = +index+1;
		    
		    $("input[id='clase_index']").val(next);

		    var select_a = clase.find('select#clase_anio_');
		    select_a.prop('id', select_a.prop('id')+index);
		    select_a.prop('name', select_a.prop('name')+index_name);

		    var select_m = clase.find('select#clase_mes_');
		    select_m.prop('id', select_m.prop('id')+index);
		    select_m.prop('name', select_m.prop('name')+index_name);

		    var input_i = clase.find('input#clase_id_');
		    input_i.prop('id', input_i.prop('id')+index);
		    input_i.prop('name', input_i.prop('name')+index_name);

		    var input_d = clase.find('input#clase_dias_');
		    input_d.prop('id', input_d.prop('id')+index);
		    input_d.prop('name', input_d.prop('name')+index_name);

		    var input_l = clase.find('input#clase_lugar_');
		    input_l.prop('id', input_l.prop('id')+index);
		    input_l.prop('name', input_l.prop('name')+index_name);

		    var button_d = clase.find('.delete_clase');
		    //button_d.data('callbackparam', index);
		    //button_d.data('claseindex', index);
		    button_d.attr('data-claseindex', index);

		    clase.show();
		    clase.prop('id', 'tr-clase-'+index);
		    clase.appendTo($("tbody[id='clases']"));

		});

		$("#agregar_profesor").on("click", function(e) {

		    var profesor = $("tr[id='template_profesor']").clone();
		    var index = $("input[id='profesor_index']").val();
		    var index_name = '['+index+']';
		    var next = +index+1;
		    
		    $("input[id='profesor_index']").val(next);

		    var select_p = profesor.find('select#profesor_prof_');
		    select_p.prop('id', select_p.prop('id')+index);
		    select_p.prop('name', select_p.prop('name')+index_name);

		    var button_d = profesor.find('.delete_profesor');
		    button_d.data('callbackparam', index);

		    profesor.show();
		    profesor.prop('id', 'tr-profesor-'+index);
		    profesor.appendTo($("tbody[id='profesores']"));

		});
		if($('#clase_index').val() == 1)
		{
			$("#agregar_clase").click();
		}
		if($('#profesor_index').val() == 1)
		{
			$("#agregar_profesor").click();
		}

		$(document).on("click", "[class*='delete_clase']", function(e) {
			var id = $(this).data('claseid');
			var index = $(this).data('claseindex');
			var mensaje = $(this).data('confirm');
			var borrar = true;
			if(id != 0)
			{
				//clase persistida, se tiene que hacer el chequeo
				var validacion_url = $('#validar_borrado_clase').val()+'/'+$(this).data('claseid');
				
				$.ajax({
			    	url: validacion_url
			    })
			    .done(function(resp) {
			    	var res = JSON.parse(resp);
			        if(res['estado'] == 0)
			        {
			        	//no puede borrarlo
			        	borrar = false;
			        	mensaje = 'No puede eliminar esta clase por que se encuentra en estado pendiente. Confirme si esta clase se dictara o no para poder eliminarla.';
			        }
			        else
			        {
			        	//puede borrarlo se le advierte que se eliminará el historial de inscripciones
			        	mensaje = mensaje + ' Si elimina la clase se perdera el historial con '+res['inscriptos']+' inscriptos aceptados.';
			        }
					$('#confirm-modal').find('.danger').unbind('click');
					if(borrar)
					{
						$('#confirm-modal').find('.danger').click(function() {
							curso_borrar_clase(index);
							$('#confirm-modal').modal("hide");
						});
					}
					else
					{
						$('#confirm-modal').find('.danger').click(function() {
							$('#confirm-modal').modal("hide");
						});
						$('#confirm-modal').find('button').hide();
						$('#confirm-modal').find('.danger').html('Aceptar');
					}
					$('.modal-body').html(mensaje);    
					$('#confirm-modal').modal();
			    })
			}
			else
			{
				//clase no persistida puede borrarla
				$('#confirm-modal').find('.danger').unbind('click');
				$('#confirm-modal').find('.danger').click(function() {
					curso_borrar_clase(index);
					$('#confirm-modal').modal("hide");
				});

				$('.modal-body').html(mensaje);    
				$('#confirm-modal').modal();
			}
		});

		$('#actualizar_archivo').on('click', function(){
			$('#show_archivo').toggle();
			$('#carga_archivo').toggle();
		});
		$('#cancelar_archivo').on('click', function(){
			$('#show_archivo').toggle();
			$('#carga_archivo').toggle();
			
			$("#archivo_curso").fileinput('reset');
		});

		$("#archivo_curso").fileinput({
		    allowedFileExtensions : ['pdf', 'doc'],
		    overwriteInitial: false,
		    maxFileSize: 100000,
		    slugCallback: function(filename) 
		    {
		        return filename.replace('(', '_').replace(']', '_');
		    }
		});

});
function curso_borrar_clase(clase_index)
{
	$("tr[id='tr-clase-"+clase_index+"']").remove();
}
function curso_borrar_profesor(profesor_index)
{
	$("tr[id='tr-profesor-"+profesor_index+"']").remove();
}