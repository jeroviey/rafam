$("#file-1").fileinput({
	previewFileType:'any',
    allowedFileExtensions : ['pdf'],
    overwriteInitial: false,
    maxFileSize: 10000,
    slugCallback: function(filename) 
    {
        return filename.replace('(', '_').replace(']', '_');
    }
});