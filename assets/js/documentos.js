$(document).ready(function($) {
  var list_target_id = 'list-target'; //first select list ID
  var list_select_id = 'modulo_id'; //second select list ID
  var initial_target_html = '<option value="">Seleccione un documento ya cargado</option>'; //Initial prompt for target select
 
  $('#'+list_target_id).html(initial_target_html); //Give the target select the prompt option
 
  $('#'+list_select_id).change(function(e) {
    //Grab the chosen value on first select list change
    var selectvalue = $(this).val();

    //Display 'loading' status in the target select list
    $('#'+list_target_id).html('<option value="">Loading...</option>');
 
    if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
       $('#'+list_target_id).html(initial_target_html);
    } else {
        $.ajax({
          url: $(this).data('ajaxurl')+'/'+selectvalue
        })
          .done(function( data ) {
            $('#'+list_target_id).html(data);
          });
        }
    });
});