$(function(){ 
    var touch   = $('.touch-menu');
    var menu    = $('#menu');
    var menu_azul = $('.menu_azul');
    var logo = $('#logo_min_eco_header');
 
    $(touch).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });
    
    $(window).resize(function(){
        var w = $(window).width();
        if(w > 767 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
    if(logo.length > 0){
        menu_azul.addClass('original').clone().insertAfter(menu_azul).addClass('cloned').addClass('navbar-fixed-top').removeClass('original').hide();
        var num = 90; //number of pixels before modifying styles
        var original = $('.original');
        var cloned = $('.cloned');
        var second_touch = cloned.find('.row').find('.touch-menu').addClass('secondary-menu').removeClass('touch-menu');
        var second_menu = cloned.find('#menu').attr('id','second-menu');
        
        $(second_touch).on('click', function(e) {
            e.preventDefault();
            second_menu.slideToggle();
        });

        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > num) {
                cloned.show();
                original.css('visibility','hidden');
            } else {
                cloned.hide();
                original.css('visibility','visible');
            }
        });  
    }
});