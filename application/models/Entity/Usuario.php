<?php

namespace Entity;

/**
 * @Entity
 * Usuario Model
 * @ORM\Entity(repositoryClass="Entity\Repository\UsuarioRepository")
 * @Table(name="usuario")
 */

class Usuario extends Object
{
    /**
     * @Column(type="string", length=20, nullable=false)
     */
    protected $username;
    
    /**
     * @Column(type="string", length=300, nullable=false)
     */
    protected $password;

    /**
     * @Column(type="boolean", length=1)
     */
    protected $estado;
    
    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $apellido;

    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="boolean", length=1)
     */
    protected $password_generada;


    /** @PrePersist */ 
    function onPrePersist() 
    { 
        parent::onPrePersist();
        if($this->password_generada == null){ $this->password_generada = false; }
    }

    public function __toString()
    {
        return $this->nombre.' '.$this->apellido;
    }

    public function newRecord()
    {
        return !isset($this->id);
    }

    public function get_username()
    {
        return $this->username;
    }

     public function get_password()
    {
        return $this->password;
    }

    public function get_estado()
    {
        return $this->estado;
    }

    public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_apellido()
    {
        return $this->apellido;
    }

    public function get_email()
    {
        return $this->email;
    }

    public function get_password_generada()
    {
        return $this->password_generada;
    }

    
    public function set_username($username)
    {
        $this->username = $username;
        return $this;
    }

    public function set_password($password)
    {
        $this->password = $password;
        return $this;
    }

    public function set_estado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    public function set_nombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function set_apellido($apellido)
    {
        $this->apellido = $apellido;
        return $this;
    }

    public function set_telefono($telefono)
    {
        $this->telefono = $telefono;
        return $this;
    }

    public function set_email($email)
    {
        $this->email = $email;
        return $this;
    }

    public function set_password_generada($password_generada)
    {
        $this->password_generada = $password_generada;
        return $this;
    }
}
?>