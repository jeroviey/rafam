<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="noticia")
 */

class Noticia extends Object
{
    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $titulo;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $texto;

    public function __toString()
    {
        return $this->texto;
    }

	public function get_titulo()
    {
    	return $this->titulo;
	}

	public function get_texto()
    {
    	return $this->texto;
	}

    public function get_fecha()
    {
        setlocale(LC_TIME, 'spanish');
        return date_format( $this->get_created_at(), 'd/m/Y');
    }

	public function set_titulo($titulo)
	{
		return $this->titulo = $titulo;
	}

	public function set_texto($texto)
	{
		$this->texto = $texto;
	}
}