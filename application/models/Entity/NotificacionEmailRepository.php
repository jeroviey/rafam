<?php

namespace Entity;

use Doctrine\ORM\EntityRepository;

class NotificacionEmailRepository extends EntityRepository
{

    public function emailsGetBusqueda($busqueda)
    {
        $CI =& get_instance();
        $query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('e');
        $query = $query->from('Entity\NotificacionEmail','e');
        
        if(count($busqueda) > 0)
        {   
            $query = $query->where('e.nombre LIKE \'%'.$busqueda['nombre'].'%\'');
            $query = $query->andWhere('e.email LIKE \'%'.$busqueda['email'].'%\'');

            if(isset($busqueda['id_municipio']))
            {
                if ($busqueda['id_municipio'] != 0)
                {
                    $query = $query->join('e.municipio','m'); 
                    $query = $query->andWhere("m.id=".$busqueda['id_municipio']);
                }
                else
                {
                    $query = $query->andWhere("e.municipio is null");
                }
            }
        }
        return $query->getQuery()->getResult();
    }
}

?>