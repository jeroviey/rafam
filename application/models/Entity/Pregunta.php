<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="pregunta")
 */

class Pregunta extends Object
{

    /**
     * @Column(type="string", length=500, unique=true, nullable=false)
     */
    protected $pregunta;

    /**
     * @Column(type="string", length=3000, unique=true, nullable=false)
     */
    protected $respuesta;

    /**
     * @Column(type="integer", length=11)
     */
    protected $orden;

    /**
     * @ManyToOne(targetEntity="Tema", inversedBy="preguntas")
     * @JoinColumn(name="tema_id", referencedColumnName="id")
     **/
    private $tema;


    public function __construct() {
        $this->orden = 999;
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_pregunta()
    {
    	return $this->pregunta;
	}

	public function set_pregunta($pregunta)
	{
		return $this->pregunta = $pregunta;
	}

    public function get_respuesta()
    {
        return $this->respuesta;
    }

    public function set_respuesta($respuesta)
    {
        return $this->respuesta = $respuesta;
    }

    public function get_orden()
    {
        return $this->orden;
    }
    public function set_orden($orden)
    {
        $this->orden = $orden;
    }

    public function get_tema()
    {
        return $this->tema;
    }

    public function set_tema(Tema $tema)
    {
        $this->tema = $tema;
    }
}