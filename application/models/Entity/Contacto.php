<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="contacto")
 */

class Contacto extends Object
{

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $apellido;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $cargo;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $oficina;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $lugar_trabajo;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $asunto;

    /**
     * @Column(type="string", length=1000, unique=true, nullable=false)
     */
    protected $mensaje;

    /**
     * @Column(type="boolean")
     */
    protected $solucionado;

    /**
     * @Column(type="boolean")
     */
    protected $leido;

    /**
     * @Column(type="date")
     */
    protected $fecha_solucionado;

    /**
     * @OneToOne(targetEntity="Municipio")
     * @JoinColumn(name="municipio_id", referencedColumnName="id")
     **/
    private $municipio;

    public function __construct() {
        $this->solucionado = 0;
        $this->leido = 0;
    }

    public function __toString()
    {
        return $this->nombre . $this->apellido .' - '. $this->asunto;
    }

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function set_apellido($apellido)
    {
        return $this->apellido = $apellido;
    }

    public function set_email($email)
    {
        return $this->email = $email;
    }

    public function set_cargo($cargo)
    {
        return $this->cargo = $cargo;
    }

    public function set_oficina($oficina)
    {
        return $this->oficina = $oficina;
    }

    public function set_lugar_trabajo($lugar_trabajo)
    {
        return $this->lugar_trabajo = $lugar_trabajo;
    }

    public function set_asunto($asunto)
    {
        return $this->asunto = $asunto;
    }

    public function set_mensaje($mensaje)
    {
        return $this->mensaje = $mensaje;
    }

    public function set_municipio(Municipio $municipio)
    {
        $this->municipio = $municipio;
    }

    public function set_solucionado($solucionado)
    {
        return $this->solucionado = $solucionado;
    }

    public function set_leido($leido)
    {
        return $this->leido = $leido;
    }

    public function set_fecha_solucionado($value)
    {
        $this->fecha_solucionado = $value;
    }



    public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_apellido()
    {
        return $this->apellido;
    }

    public function get_email()
    {
        return $this->email;
    }

    public function get_cargo()
    {
        return $this->cargo;
    }

    public function get_oficina()
    {
        return $this->oficina;
    }

    public function get_lugar_trabajo()
    {
        return $this->lugar_trabajo;
    }

    public function get_asunto()
    {
        return $this->asunto;
    }
    
    public function get_solucionado()
    {
        return $this->solucionado;
    }

    public function get_leido()
    {
        return $this->leido;
    }

    public function get_autor()
    {
        $trabajo = $this->municipio ? $this->municipio->get_nombre() : $this->lugar_trabajo;
        return $this->nombre.' '.$this->apellido.' - '.$trabajo;
    }

    public function get_mensaje()
    {
        return $this->mensaje;
    }   

    public function get_municipio()
    {
        return $this->municipio;
    }

    public function esta_leido()
    {
        if ($this->leido == 0) 
        {
            return false;
        }
        return true;
    }

    public function get_fecha_solucionado()
    {
        return $this->fecha_solucionado;
    }

}