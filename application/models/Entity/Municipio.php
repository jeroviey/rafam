<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="municipio")
 */

class Municipio extends Object
{
    /**
     * @Column(type="integer", length=11, unique=true, nullable=false)
     */
    protected $codigo_municipio;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $nombre;

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

	public function get_codigo_municipio()
    {
    	return $this->codigo_municipio;
	}

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

	public function set_codigo_municipio($codigo_municipio)
	{
		return $this->codigo_municipio = $codigo_municipio;
	}
}