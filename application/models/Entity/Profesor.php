<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="profesor")
 */

class Profesor extends Object
{


    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $apellido;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $email;

    /**
     * @ManyToMany(targetEntity="Curso", mappedBy="profesores")
     **/
    protected $cursos;

    public function __construct() 
    {
        $this->cursos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre.' '. $this->apellido;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

    public function get_email()
    {
        return $this->email;
    }

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function get_apellido()
    {
        return $this->apellido;
    }

    public function set_apellido($apellido)
    {
        return $this->apellido = $apellido;
    }

    public function set_email($email)
    {
        return $this->email = $email;
    }
}