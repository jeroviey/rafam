<?php

namespace Entity;

use Doctrine\ORM\EntityRepository;

class InscripcionRepository extends EntityRepository
{

    public function inscripcionesDeUnCurso($id_curso)
    {
        $CI =& get_instance();

    	$query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('i');
        $query = $query->from('Entity\Inscripcion','i');
        $query = $query->join('i.curso','c'); 
        $query = $query->where("c.id = ".$id_curso);
        $query = $query->andWhere("i.aceptada = 1");

        return $query->getQuery()->getResult();
    }

    public function inscripcionesDeUnCursoYUnaClase($id_curso, $id_clase)
    {
        $CI =& get_instance();

        $query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('i');
        $query = $query->from('Entity\Inscripcion','i');
        $query = $query->join('i.curso','cu');
        $query = $query->join('i.clase','cla');
        $query = $query->where("cu.id = ".$id_curso);
        $query = $query->andWhere("cla.id = ".$id_clase);
        $query = $query->andWhere("i.aceptada = 1");

        return $query->getQuery()->getResult();
    }

    public function inscripcionGetBusqueda($busqueda)
    {
        $CI =& get_instance();
        $query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('i');
        $query = $query->from('Entity\Inscripcion','i');
        $query = $query->join('i.clase','cla');    
        $query = $query->orderBy("i.created_at","DESC");
        
        if(count($busqueda) > 0)
        {    
            $query = $query->where('i.nombre LIKE \'%'.$busqueda['inscripcion'].'%\'');
            $query = $query->orWhere('i.apellido LIKE \'%'.$busqueda['inscripcion'].'%\'');
            if(isset($busqueda['id_municipio']))
            {
                if ($busqueda['id_municipio'] != 0)
                {
                    $query = $query->join('i.municipio','m');    
                    $query = $query->andWhere("m.id=".$busqueda['id_municipio']);
                }
                else
                {
                    $query = $query->andWhere("i.lugar_trabajo <> '' ");
                }
            }
            if(isset($busqueda['curso']))
            { 
                $query = $query->join('i.curso','cu');    
                $query = $query->andWhere('cu.id = '.$busqueda['curso']); 
            }
            if(isset($busqueda['clase']))
            { 
                $query = $query->andWhere('cla.id = '.$busqueda['clase']); 
            }
            if(isset($busqueda['estado']))
            { 
                $query = $query->andWhere('i.aceptada = '.$busqueda['estado']); 
            }
            if(isset($busqueda['asistencia']))
            { 
                $query = $query->andWhere('i.asistencia = '.$busqueda['asistencia']); 
            }
        }
        return $query->getQuery()->getResult();
    }
}

?>