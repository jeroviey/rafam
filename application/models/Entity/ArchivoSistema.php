<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="archivo_sistema")
 */

class ArchivoSistema extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $archivo;


    public function __construct() 
    {

    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function get_nombre()
    {
        return $this->nombre;
    }

	public function get_archivo()
    {
    	return $this->archivo;
	}

    public function get_nombre_archivo()
    {
        $archivo = str_replace("\\","/",$this->archivo);
        $datos = explode("/", $archivo);
        return $datos[6];
    }


    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

	public function set_archivo($value)
	{
		$this->archivo = $value;
	}

}