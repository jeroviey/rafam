<?php

namespace Entity;

use Doctrine\ORM\EntityRepository;

class DocumentacionRepository extends EntityRepository
{

    public function documentacionDeUnTipo($tipo_id)
    {
        $CI =& get_instance();

    	$query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('d');
        $query = $query->from('Entity\Documentacion','d');
        $query = $query->join('d.tipo','t');  
        $query = $query->where("t.id = ".$tipo_id."");

        return $query->getQuery()->getResult();
    }
}

?>