<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="documento")
 */

class Documento extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $path;

    /**
     * @ManyToMany(targetEntity="Actualizacion", mappedBy="documentos", cascade={"persist"})
     **/
    protected $actualizaciones;


    public function __construct() 
    {
        $this->actualizaciones = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_path()
    {
    	return $this->path;
	}

    public function get_actualizaciones()
    {
        return $this->actualizaciones;
    }


    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

	public function set_path($value)
	{
		$this->path = $value;
	}

    public function add_actualizacion(Actualizacion $actualizacion)
    {
        if(!$this->get_actualizaciones()->contains($actualizacion))
        {
            $this->actualizaciones[] = $actualizacion;
        }
    }

    public function puedo_eliminar(Actualizacion $actualizacion)
    {
        //elimino la tabla intermedia
        $actualizacion->get_documentos()->removeElement($this);
        if ( count($this->get_actualizaciones()) > 1 )
        {
            //no puedo eliminar el documento
            return FALSE;
        }
        else
        {
            //puedo eliminar el documento, solo entra aca cuando el doc solo pertenezaca a 1 actaulizacion
            return TRUE;
        }

    }

}