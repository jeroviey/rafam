<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="titulo")
 */

class Titulo extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $clave;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /** 
    *@OneToMany(targetEntity="Contenido", mappedBy="titulo_padre", cascade={"persist"} ) 
    **/
    protected $contenidos;


    public function __construct() 
    {
        $this->contenidos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function get_clave()
    {
        return $this->clave;
    }

	public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_contenidos()
    {
        return $this->contenidos;
    }


    public function set_clave($value)
    {
        $this->clave = $value;
    }

    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

    public function add_contenidos(Contenido $contenido)
    {
        if(!$this->get_contenidos()->contains($contenido))
        {
            $this->contenidos[] = $contenido;  
            $contenido->add_titulo_padre($this);
        }
    }



}