<?php

namespace Entity;

/**
 * @Entity(repositoryClass="InscripcionRepository")
 * @Table(name="inscripcion")
 */

class Inscripcion extends Object
{

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $apellido;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $dni;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $telefono;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $cargo;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     */
    protected $oficina;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $lugar_trabajo;

    /**
     * @Column(type="boolean")
     */
    protected $asistencia;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $aceptada;

    /**
     * @ManyToOne(targetEntity="Curso")
     * @JoinColumn(name="curso_id", referencedColumnName="id")
     **/
    private $curso;

    /**
     * @ManyToOne(targetEntity="Clase", inversedBy="inscripciones")
     * @JoinColumn(name="clase_id", referencedColumnName="id")
     **/
    private $clase;

    /**
     * @OneToOne(targetEntity="Municipio")
     * @JoinColumn(name="municipio_id", referencedColumnName="id")
     **/
    private $municipio;

    public function __construct() {
        $this->asistencia = 0;
        $this->aceptada = 0;
    }

    public function __toString()
    {
        return $this->apellido.', '.$this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function get_apellido()
    {
        return $this->apellido;
    }

    public function set_apellido($apellido)
    {
        return $this->apellido = $apellido;
    }

    public function get_dni()
    {
        return $this->dni;
    }

    public function set_dni($dni)
    {
        return $this->dni = $dni;
    }

    public function get_email()
    {
        return $this->email;
    }

    public function set_email($email)
    {
        return $this->email = $email;
    }

    public function get_telefono()
    {
        return $this->telefono;
    }

    public function set_telefono($telefono)
    {
        return $this->telefono = $telefono;
    }

    public function get_cargo()
    {
        return $this->cargo;
    }

    public function set_cargo($cargo)
    {
        return $this->cargo = $cargo;
    }

    public function get_oficina()
    {
        return $this->oficina;
    }

    public function set_oficina($oficina)
    {
        return $this->oficina = $oficina;
    }

    public function get_lugar_trabajo()
    {
        return $this->lugar_trabajo;
    }

    public function set_lugar_trabajo($lugar_trabajo)
    {
        return $this->lugar_trabajo = $lugar_trabajo;
    }

    public function get_asistencia()
    {
        return $this->asistencia;
    }

    public function set_asistencia($asistencia)
    {
        return $this->asistencia = $asistencia;
    }

    public function get_aceptada()
    {
        return $this->aceptada;
    }

    public function set_aceptada($aceptada)
    {
        return $this->aceptada = $aceptada;
    }

    public function get_curso()
    {
        return $this->curso;
    }

    public function set_curso(Curso $curso)
    {
        $this->curso = $curso;
    }

    public function get_clase()
    {
        return $this->clase;
    }

    public function set_clase(Clase $clase)
    {
        $this->clase = $clase;
    }

    public function get_municipio()
    {
        return $this->municipio;
    }

    public function set_municipio(Municipio $municipio)
    {
        $this->municipio = $municipio;
    }

}