<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="curso")
 */

class Curso extends Object
{

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $nombre;

    /**
     * @OneToMany(targetEntity="Clase", mappedBy="curso", cascade={"all"})
     * @OrderBy({"mes" = "ASC"})
     **/
    private $clases;

    /**
     * @ManyToMany(targetEntity="Profesor", inversedBy="cursos")
     * @JoinTable(name="curso_profesor",
     *    joinColumns={@JoinColumn(name="curso_id", referencedColumnName="id")},
     *    inverseJoinColumns={@JoinColumn(name="profesor_id", referencedColumnName="id")}
     * )
     **/
    private $profesores;

    /**
     * @OneToOne(targetEntity="Modulo")
     * @JoinColumn(name="modulo_id", referencedColumnName="id")
     **/
    private $modulo;

    /**
     * @OneToMany(targetEntity="ArchivoCurso", mappedBy="curso", cascade={"all"})
     **/
    private $archivos;

    public function __construct() {
        $this->clases = new ArrayCollection();
        $this->profesores = new ArrayCollection();
        $this->archivos = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function get_clases()
    {
        return $this->clases;
    }

    public function add_clase(Clase $clase)
    {
        if(!$this->clases->contains($clase))
        {
            $this->clases[]= $clase;
        }
    }

    public function remove_clase(Clase $clase)
    {
        if($this->clases->contains($clase))
        {
            $this->clases->removeElement($clase);
        }
    }

    public function get_id_clases()
    {
        $array = array();
        foreach($this->clases as $clase)
        {
            $array[] = $clase->get_id();
        }
        return $array;
    }

    public function get_archivo_id()
    {
        if(count($this->archivos->toArray()) > 0)
        {
            return $this->archivos->toArray()[0]->get_id();
        }
        else
        {
            return 0;
        }
    }

    public function get_archivo_nombre()
    {
        if(count($this->archivos->toArray()) > 0)
        {
            return $this->archivos->toArray()[0]->get_nombre();
        }
        else
        {
            return null;
        }
    }

    public function get_archivo_path()
    {
        if(count($this->archivos->toArray()) > 0)
        {
            return $this->archivos->toArray()[0]->get_path();
        }
        else
        {
            return null;
        }
    }

    public function get_archivo_filename()
    {
        if(count($this->archivos->toArray()) > 0)
        {
            $path = $this->archivos->toArray()[0]->get_path();
            $partes = explode("/", $path);
            return array_pop($partes);
        }
        else
        {
            return null;
        }
    }

    public function get_archivos()
    {
        return $this->archivos;
    }

    public function add_archivo(ArchivoCurso $archivo)
    {
        if(!$this->archivos->contains($archivo))
        {
            $this->archivos[]= $archivo;
        }
    }

    public function remove_archivo(ArchivoCurso $archivo)
    {
        if($this->archivos->contains($archivo))
        {
            $this->archivos->removeElement($archivo);
        }
    }

    public function get_profesores()
    {
        return $this->profesores;
    }

    public function add_profesor(Profesor $profesor)
    {
        if(!$this->profesores->contains($profesor))
        {
            $this->profesores[]= $profesor;
        }
    }

    public function remove_profesor(Profesor $profesor)
    {
        if($this->profesores->contains($profesor))
        {
            $this->profesores->removeElement($profesor);
        }
    }

    public function remove_all_profesores()
    {
        foreach($this->profesores as $profesor)
        {
            $this->profesores->removeElement($profesor);
        }
    }

    public function get_id_profesores()
    {
        $array = array();
        foreach($this->profesores as $profesor)
        {
            $array[] = $profesor->get_id();
        }
        return $array;
    }

    public function get_modulo()
    {
        return $this->modulo;
    }

    public function set_modulo(Modulo $modulo)
    {
        $this->modulo = $modulo;
    }

    public function get_info_clases()
    {
        $resultado = "";
        $first = true;
        foreach($this->clases->toArray() as $clase)
        {
            if($first){
                $first = false;
            }
            else{
                $resultado .= '|';
            }
            $resultado .= $clase->get_id().'='.$clase;
        }
        return $resultado;
    }

    public function get_info_clases_pendientes()
    {
        $resultado = "";
        $first = true;
        foreach($this->clases->toArray() as $clase)
        {
            if($clase->get_estado() == 0)
            {
                if($first){
                    $first = false;
                }
                else{
                    $resultado .= '|';
                }
                $resultado .= $clase->get_id().'='.$clase;
            }
        }
        $resultado = ($resultado == '') ? '-1=No existen clases para este curso.' : $resultado;

        return $resultado;
    }

    public function lista_profesores()
    {
        $resultado = "";
        $first = true;
        foreach($this->profesores->toArray() as $profesor)
        {
            if($first){
                $first = false;
            }
            else{
                $resultado .= ' - ';
            }
            $resultado .= $profesor->get_nombre().' '.$profesor->get_apellido();
        }
        return $resultado;
    }
}