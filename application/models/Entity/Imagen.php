<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="imagen")
 */

class Imagen extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $imagen;


    public function __construct() 
    {

    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function get_nombre()
    {
        return $this->nombre;
    }

	public function get_imagen()
    {
        $CI =& get_instance();
        return $CI->config->item('path_imagenes').$this->imagen;
    }

    public function get_ubicacion()
    {
        $CI =& get_instance();
        return base_url().$CI->config->item('path_imagenes_ubicacion').$this->imagen;
    }



    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

    public function set_imagen($value)
    {
        $this->imagen = $value;
    }


}