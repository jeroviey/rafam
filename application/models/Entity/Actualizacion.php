<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(repositoryClass="ActualizacionRepository")
 * @Table(name="actualizacion")
 */

class Actualizacion extends Object
{
    /**
     * @Column(type="date", nullable=false)
     */
    protected $fecha_subida;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $archivo;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $tipo;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $version;

    /** 
    *@ManyToOne(targetEntity="Modulo", inversedBy="actualizaciones") 
    * @JoinColumn(name="modulo_id", referencedColumnName="id")
    **/
    protected $modulo;

    /** 
    * Bidirectional * 
    * @ManyToMany(targetEntity="Documento", inversedBy="actualizaciones", cascade={"persist"}) 
    * @JoinTable(name="actualizacion_documento") 
    */
    protected $documentos;


    public function __construct() 
    {
        $this->documentos = new ArrayCollection();
    }

    public function __toString()
    {
        return '';
    }

    public function get_fecha_subida()
    {
        return $this->fecha_subida;
    }

	public function get_archivo()
    {
    	return $this->archivo;
	}

    public function get_tipo()
    {
        return $this->tipo;
    }

    public function get_version()
    {
        return $this->version;
    }

    public function get_modulo()
    {
        return $this->modulo;
    }

    public function get_documentos()
    {
        return $this->documentos;
    }

    public function get_nombre_archivo()
    {
        $archivo = str_replace("\\","/",$this->archivo);
        $datos = explode("/", $archivo);
        return $datos[6];
    }


    public function set_fecha_subida($value)
    {
        $this->fecha_subida = $value;
    }

	public function set_archivo($value)
	{
		$this->archivo = $value;
	}

    public function set_tipo($value)
    {
        $this->tipo = $value;
    }

    public function set_version($value)
    {
        $this->version = $value;
    }

    public function add_documento(Documento $documento)
    {
        if(!$this->get_documentos()->contains($documento))
        {
            $this->documentos[] = $documento;
            $documento->add_actualizacion($this);
        }
    }

    public function add_modulo(Modulo $modulo)
    {
        $this->modulo = $modulo;
    }

}