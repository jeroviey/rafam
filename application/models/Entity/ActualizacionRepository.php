<?php

namespace Entity;

use Doctrine\ORM\EntityRepository;

class ActualizacionRepository extends EntityRepository
{

    public function actualizacionesOrdenadas()
    {
        $CI =& get_instance();

    	$query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('a');
        $query = $query->from('Entity\Actualizacion','a');
        $query = $query->orderBy("a.version","DESC");

        return $query->getQuery()->getResult();
    }
}

?>