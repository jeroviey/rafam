<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="archivo_curso")
 */

class ArchivoCurso extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $path;

    /**
     * @ManyToOne(targetEntity="Curso", inversedBy="archivos")
     * @JoinColumn(name="curso_id", referencedColumnName="id")
     **/
    protected $curso;


    public function __construct() 
    {
        
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_path()
    {
    	return $this->path;
	}

    public function get_curso()
    {
        return $this->curso;
    }

    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

    public function set_path($value)
    {
        $this->path = $value;
    }

    public function set_curso(Curso $curso)
    {
        $curso->add_archivo($this);
        return $this->curso = $curso;
    }

}