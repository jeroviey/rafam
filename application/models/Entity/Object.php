<?php

namespace Entity;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 */

class Object
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @var datetime $created_at
    * @Column(type="datetime")
    */
    private $created_at;
    
    /**
     * @var \DateTime $updated_at
     * @Column(type="datetime")
     */
    private $updated_at;

    /** @PrePersist */ 
    function onPrePersist() { 
    //using Doctrine DateTime here 
        $this->created_at = new \DateTime('now');
        $this->updated_at = new \DateTime('now');
    }
     
    /** @PreUpdate */ 
    function onPreUpdate() { 
        $this->updated_at = new \DateTime('now');
    }

    function get_id()
    {
        return $this->id;
    }

    function get_created_at()
    {
        return $this->created_at;
    }

    function get_updated_at()
    {
        return $this->updated_at;
    }
}