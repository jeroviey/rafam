<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="clase")
 */

class Clase extends Object
{
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $ano;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $mes;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $dias;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $lugar;

    /**
     * @ManyToOne(targetEntity="Curso", inversedBy="clases")
     * @JoinColumn(name="curso_id", referencedColumnName="id")
     **/
    protected $curso;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $estado;

    /**
     * @OneToMany(targetEntity="Inscripcion", mappedBy="clase", cascade={"all"})
     **/
    private $inscripciones;

    protected $meses = array(4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre');

    public function __construct() 
    {
        $this->estado = 0;
    }

    public function __toString()
    {
        return $this->dias.' de '.$this->meses[$this->mes].' de '.$this->ano.', '.$this->lugar;
    }

	public function get_dias()
    {
    	return $this->dias;
	}

	public function set_dias($dias)
	{
		return $this->dias = $dias;
	}

    public function get_mes()
    {
        return $this->mes;
    }

    public function get_mes_texto()
    {
        return $this->meses[$this->mes];
    }

    public function get_meses()
    {
        return $this->meses;
    }

    public function set_mes($mes)
    {
        return $this->mes = $mes;
    }

    public function get_ano()
    {
        return $this->ano;
    }

    public function set_ano($ano)
    {
        return $this->ano = $ano;
    }

    public function get_lugar()
    {
        return $this->lugar;
    }

    public function set_lugar($lugar)
    {
        return $this->lugar = $lugar;
    }

    public function get_curso()
    {
        return $this->curso;
    }

    public function set_curso(Curso $curso)
    {
        $curso->add_clase($this);
        return $this->curso = $curso;
    }

    public function get_estado_string()
    {
        switch ($this->estado) {
            case '0':
                return 'Pendiente';
                break;
            case '1':
                return 'Realizado con practica';
                break;
            case '2':
                return 'Realizado sin practica';
                break;
            case '3':
                return 'Cancelado';
                break;
        }
    }

    public function get_estado()
    {
        return $this->estado;
    }

    public function set_estado($estado)
    {
        return $this->estado = $estado;
    }
}