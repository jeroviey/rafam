<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="contenido")
 */

class Contenido extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $titulo;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $descripcion;

    /**
     * @Column(type="integer", length=11)
     */
    protected $orden;

    /**
     * @ManyToOne(targetEntity="Titulo", inversedBy="contenidos")
     * @JoinColumn(name="titulo_id", referencedColumnName="id")
     */
    protected $titulo_padre;


    public function __construct() 
    {
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_titulo()
    {
        return $this->titulo;
    }

    public function get_descripcion()
    {
    	return $this->descripcion;
	}

    public function get_orden()
    {
        return $this->orden;
    }

    public function get_titulo_padre()
    {
        return $this->titulo_padre;
    }


    public function set_titulo($value)
    {
        $this->titulo = $value;
    }

	public function set_descripcion($value)
	{
		$this->descripcion = $value;
	}

    public function set_orden($value)
    {
        $this->orden = $value;
    }

    public function add_titulo_padre(Titulo $titulo)
    {
        $this->titulo_padre = $titulo;
    }
}