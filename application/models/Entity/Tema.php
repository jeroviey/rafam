<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="tema")
 */

class Tema extends Object
{

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="integer", length=11)
     */
    protected $orden;

    /**
     * @OneToMany(targetEntity="Pregunta", mappedBy="tema")
     **/
    private $preguntas;

    public function __construct() {
        $this->orden = 999;
        $this->preguntas = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function get_orden()
    {
        return $this->orden;
    }
    public function set_orden($orden)
    {
        $this->orden = $orden;
    }
    
    public function get_preguntas()
    {
        return $this->preguntas;
    }

    public function add_pregunta(Pregunta $pregunta)
    {
        if(!$this->preguntas->contains($pregunta))
        {
            $this->preguntas[]= $pregunta;
        }
    }

    public function remove_pregunta(Clase $pregunta)
    {
        if($this->preguntas->contains($pregunta))
        {
            $this->preguntas->removeElement($pregunta);
        }
    }

    private function faq_cmp($a, $b)
    {
        if ($a->get_orden() == $b->get_orden()) {
            return 0;
        }
        return ($a->get_orden() < $b->get_orden()) ? -1 : 1;
    }

    public function get_preguntas_ordenadas(){
        $faqs = $this->get_preguntas()->toArray();
        
        usort($faqs, array($this, "faq_cmp"));

        return $faqs;
    }
}