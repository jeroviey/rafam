<?php

namespace Entity;

/**
 * @Entity(repositoryClass="DocumentacionRepository")
 * @Table(name="documentacion")
 */

class Documentacion extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="text", nullable=false)
     */
    protected $path;

    /**
     * @Column(type="date")
     */
    protected $fecha_subida;


    /**
     * @ManyToOne(targetEntity="TipoDocumentacion", inversedBy="documentaciones")
     * @JoinColumn(name="tipo_documentacion_id", referencedColumnName="id", nullable=false)
     */
    protected $tipo;


    public function __construct() 
    {
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_path()
    {
    	return $this->path;
	}

    public function get_fecha_subida()
    {
        return $this->fecha_subida;
    }

    public function get_tipo()
    {
        return $this->tipo;
    }


    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

	public function set_path($value)
	{
		$this->path = $value;
	}

    public function set_fecha_subida($value)
    {
        $this->fecha_subida = $value;
    }

    public function set_tipo($value)
    {
        $this->tipo = $value;
    }


}