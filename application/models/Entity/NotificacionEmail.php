<?php

namespace Entity;

/**
 * @Entity(repositoryClass="NotificacionEmailRepository")
 * @Table(name="notificacion_email")
 */

class NotificacionEmail extends Object
{
    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $nombre;

    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $email;

    /**
     * @OneToOne(targetEntity="Municipio")
     * @JoinColumn(name="municipio_id", referencedColumnName="id", nullable=true)
     **/
    private $municipio;

    public function __construct() 
    {
        $this->municipio = NULL;
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

	public function set_nombre($nombre)
	{
		return $this->nombre = $nombre;
	}

    public function get_email()
    {
        return $this->email;
    }

	public function set_email($email)
	{
		$this->email = $email;
	}

    public function get_municipio()
    {
        return $this->municipio;
    }

    public function set_municipio(Municipio $municipio)
    {
        $this->municipio = $municipio;
    }

    public function desvincular_municipio()
    {
        $this->municipio = NULL;
    }

    public function tiene_municipio()
    {
        if ($this->municipio == NULL) 
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}