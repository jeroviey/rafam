<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="modulo")
 */

class Modulo extends Object
{

    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /**
     * @OneToMany(targetEntity="Actualizacion", mappedBy="modulo", cascade={"persist"})
     * @OrderBy({"version" = "DESC"})
     *
     */
    protected $actualizaciones;


    public function __construct()
    {
        $this->actualizaciones = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
    	return $this->nombre;
	}

    public function get_actualizaciones()
    {
        return $this->actualizaciones;
    }


	public function set_nombre($nombre)
	{
		$this->nombre = ($nombre);
	}

    public function add_actualizacion(Actualizacion $actualizacion)
    {
        if(!$this->get_actualizaciones()->contains($actualizacion))
        {
            $this->actualizaciones[] = $actualizacion;
            $actualizacion->add_modulo($this);
        }
    }

    public function documentos_del_modulo()
    {
        $arreglo  = array('' => 'Seleccione un documento ya cargado');
        foreach ($this->actualizaciones as $actualizacion)
        {
            foreach ($actualizacion->get_documentos() as $documento)
            {
                $arreglo[$documento->get_id()]=$documento->get_nombre(); 
            }
        }
        return($arreglo);
    }

    public function ultima_actualizacion()
    {
        if(!empty($this->actualizaciones))
        {
            return $this->actualizaciones->first();
        }
        else
        {
            return '';
        }
    }
}