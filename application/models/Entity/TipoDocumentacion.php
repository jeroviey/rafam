<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="tipo_documentacion")
 */

class TipoDocumentacion extends Object
{
    /**
     * @Column(type="text", nullable=false)
     */
    protected $nombre;

    /** 
    *@OneToMany(targetEntity="Documentacion", mappedBy="tipo", cascade={"persist"} ) 
    **/
    protected $documentaciones;


    public function __construct() 
    {
        $this->documentaciones = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

	public function get_nombre()
    {
        return $this->nombre;
    }

    public function get_documentaciones()
    {
        return $this->documentaciones;
    }


    public function set_nombre($value)
    {
        $this->nombre = $value;
    }

    public function add_documentacion(Documentacion $doc)
    {
        if(!$this->get_documentaciones()->contains($doc))
        {
            $this->documentaciones[] = $doc;  
            $doc->set_tipo($this);
        }
    }



}