<?php

/**
 * /application/core/MY_Loader.php
 *
 */
class MY_Loader extends MX_Loader 
{
    public function template_backend($template_name, $vars = array(), $return = FALSE)
    {
        $vars = array_merge($vars, array('backend' => true));
        $content  = $this->view('plantillas/header', $vars, $return);
        $content  = $this->view('plantillas/menu_backend', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);

        if ($return)
        {
            return $content;
        }
    }

    public function template_frontend($template_name, $vars = array(), $return = FALSE)
    {
        $vars = array_merge($vars, array('usuario_logeado' => $this->session->userdata('session_usuario'), 'backend' => false));
        $content  = $this->view('plantillas/header', $vars, $return);
        $content  = $this->view('plantillas/menu_frontend', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('plantillas/footer', $vars, $return);

        if ($return)
        {
            return $content;
        }
    }
}