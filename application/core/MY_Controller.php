<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";
class MY_Controller extends MX_Controller 
{
	function __construct()
	{
	    parent::__construct();
		$this->em = $this->doctrine->em;
		modules::run('backend/login/is_logged_in');
		$cant_mensajes = modules::run('backend/contacto/hay_mensajes');
        $this->session->set_userdata('cant_mensajes', $cant_mensajes);
	}
}
?>