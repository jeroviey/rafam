<?php
    function enviar_email($to,$subject,$message,$attachment=false,$form=false)
    {
        $CI =& get_instance();
        $CI->load->library(array('email'));
        if ($form)
        {
            $CI->email->from($form, 'RAFAM');
        }
        else
        {
            $CI->email->from('cai@ec.gba.gov.ar', 'RAFAM');
        }
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);
        $imagen = FCPATH.'assets/imagenes/logo_rafam_azul_sinfondo.png';
        $CI->email->attach($imagen, 'inline');
        if($attachment)
        {
          $CI->email->attach($attachment, 'inline');
        }
        $CI->email->send();
    }


    function meses_cursos()
    {
        return array('4' => 'Abril', '5' => 'Mayo', '6' => 'Junio', '7' => 'Julio', '8' => 'Agosto', '9' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre');
    }

    function obtener_dias($dias)
    {
        $clases = str_replace(array(",", "y", "Y"), "", $dias);
        $clases = explode ( ' ' , $clases);
        for ($i=0; $i < count($clases) ; $i++) 
        { 
            if ($clases[$i] == '')
            {
                unset($clases[$i]);
            }
        }
        return array_values($clases);
    }

    function obtener_clases_de_un_anio($anio, $curso, $clase_no_agregar)
    {
        $clases = array();
        foreach ($curso->get_clases() as $clase)
        {
            if ($clase->get_ano() == $anio)
            {
                if ($clase->get_mes() > $clase_no_agregar->get_mes()) 
                {
                    $clases[] = $clase;
                }
            }
        }
        return $clases;
    }


    //CALLBACKS
    function municipio_check($municipio, $form,$controlador)
    {
        $insc = $controlador->input->post($form, true);
        if(array_key_exists('tipo_trabajo', $insc))
        {
            if($insc['tipo_trabajo'] == '1')
            {
                if($municipio == '0')
                {
                    $controlador->form_validation->set_message('municipio_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe seleccionar un municipio');
                    return false;
                }
            }
            return true;
        }
    }

    function lugar_trabajo_check($lugar_trabajo, $form, $controlador)
    {
        $insc = $controlador->input->post($form, true);
        if(array_key_exists('tipo_trabajo', $insc))
        {
            if($insc['tipo_trabajo'] == '2')
            {
                if($lugar_trabajo == '')
                {
                    $controlador->form_validation->set_message('lugar_trabajo_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe indicar su lugar de trabajo');
                    return false;
                }
            }
            return true;
        }
    }

    function dni_check($dni, $form, $controlador)
    {
        $insc = $controlador->input->post($form, true);

        $CI =& get_instance();
        $query = $CI->doctrine->em->createQueryBuilder();
        $query = $query->select('count(i)');
        $query = $query->from('Entity\Inscripcion','i');
        $query = $query->join('i.clase', 'c');
        $query = $query->where('i.dni = '.$dni);
        $query = $query->andWhere('c.id = '.$insc['clase']);
        $cantidad = $query->getQuery()->getSingleScalarResult();

        if($cantidad > 0)
        {
            $controlador->form_validation->set_message('dni_check', '<span class="glyphicon glyphicon-remove-sign"></span> Ya existe una inscripción a esta clase con el mismo número de documento.');
            return false;
        }
        return true;
    }
?>