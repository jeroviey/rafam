<?php
    function init_pagination($uri,$total_rows,$per_page=10,$segment=4, $busqueda=null)
    {
        $ci =& get_instance();
        $config['per_page'] = $per_page;
        $config['uri_segment'] = $segment;
        $config['base_url'] = base_url().$uri;
        $config['total_rows'] = $total_rows;
        $config['suffix'] = $busqueda;
        $config['use_page_numbers']  = TRUE;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="#"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = '&laquo;';
        $config['last_link'] = '&raquo;';
        $config['prev_link'] = 'Anterior';
        $config['next_link'] = 'Siguiente';
        $config['first_url'] = '1'.$busqueda; //para que en los links cuando haga el volver a la primera no pierda la BUSQUEDA

        $ci->pagination->initialize($config);
        return $config;    
    }

    function encriptar_contra($contra = false)
    {
        if (!$contra) 
        {
          $contra = random_string('alnum', 8);
        }
        return sha1($contra);
    }

    function descargar($fullpath, $url)
    {
        $disposition = "attachment";
        archivos($fullpath,$url,$disposition);
    }

    function ver($fullpath,$url)
    {
        $disposition = "inline";
        archivos($fullpath,$url,$disposition);
    }

    function archivos($fullpath,$url,$disposition)
    {
        $ci =& get_instance();
        if( is_readable($fullpath) && (file_exists($fullpath)))
        {
            $fsize = filesize($fullpath);
            $path_parts = pathinfo($fullpath);
            $ext = strtolower($path_parts["extension"]);
            switch ($ext)
            {
                case 'pdf': $ctype="application/pdf"; break;
                case 'zip': $ctype="application/zip"; break;
                case 'rar': $ctype="application/zip"; break;
                default: $ctype="application/force-download"; 
            }
            set_time_limit(7200);

            header("Pragma: public");
            header("Content-Disposition: ".$disposition."; filename=\"".$path_parts["basename"]."\"");
            header("Content-length: $fsize");
            header('Content-Description: File Transfer');
            header('Cache-Control: public');
            header("Content-Transfer-Encoding: binary");
            header("Content-Type: $ctype"); 
            ob_clean();
            flush();
            // si FSIZE es mas grande que 2MB se sustituye la funcion Readfile por readfile_chunked($file) 
            // xq en muchos casos el buffer de descarga no aceptaba más de 2MB de tamaño
            if ($fsize > 2097152)
            {
                readfile_chunked($fullpath);
            }
            else
            {
                readfile($fullpath);
            }
            exit;
        }
        else
        {
            $ci->session->set_flashdata('error','El archivo al que quiere acceder no existe.');
            return redirect($url);
        }
    }

    function readfile_chunked($filename, $retbytes = TRUE)
    {
        $chunksize = 1*(1024*1024); // how many bytes per chunk 256MB
        $buffer = "";
        $cnt =0;
        $handle = fopen($filename, "rb");
        if ($handle === false)
        {
          return false;
        }
        while (!feof($handle))
        {
          $buffer = fread($handle, $chunksize);
          echo $buffer;
          ob_flush();
          flush();
          if ($retbytes)
          {
            $cnt += strlen($buffer);
          }
        }
        $status = fclose($handle);
        if ($retbytes && $status)
        {
          return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
    }

    function eliminar_archivo($path)
    {
        if (file_exists($path))
        {
            unlink($path);
        }
    }

    function quitar_tildes($cadena) 
    {
        $no_permitidas = array("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas = array("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }

?>