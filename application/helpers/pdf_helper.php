<?php
	require_once (APPPATH.'/third_party/snappy/src/autoload.php');

	use Knp\Snappy\Pdf;

	function crearPdf($html,$nombre)    
  	{
		$snappy = new Pdf();
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename='.sprintf("%s.pdf",$nombre));
		if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
			$snappy->setBinary('c:/wkhtmltopdf/bin/wkhtmltopdf.exe');
		}else{
			$snappy->setBinary('/usr/local/bin/wkhtmltopdf');
		}
		echo $snappy->getOutputFromHtml($html);
	}

	function exportarPdfAutoridadesMunicipales($municipios)
	{
		$ci =& get_instance();
		$ci->load->view('plantillas/header', array('logueado' => false, 'template_name'=>'','export_pdf' => true ));
		foreach ($municipios as $municipio)
	    {
		    $data = array('entidad' => $municipio,'export_pdf' => true);
	        $ci->load->view('entidad/exportar_entidad_pdf', $data, false);
		}
		//TODO debería cargar el footer
	    $html = $ci->output->get_output();
	    crearPdf($html,'contactos_municipales');
	}
?>