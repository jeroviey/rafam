<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Reforma extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    public function index()
    {
        redirect('inicio');
    }
    
    public function evolucion()
    {
        $data['imagen'] = $this->em->getRepository('Entity\Imagen')->findOneBy(array('nombre' => 'organigrama'));
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'reforma_evolucion'));
        $this->load->template_frontend('reforma/evolucion', $data);           
    }

    public function mision_y_vision()
    {
        $data['imagen'] = $this->em->getRepository('Entity\Imagen')->findOneBy(array('nombre' => 'mision_y_funcion'));
        $data['titulo_mision'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'reforma_mision'));
        $data['titulo_vision'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'reforma_vision'));
        $this->load->template_frontend('reforma/mision_y_vision', $data);           
    }

    public function alcance()
    {
        $data['imagen'] = $this->em->getRepository('Entity\Imagen')->findOneBy(array('nombre' => 'prestaciones'));
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'reforma_alcance'));
        $data['titulo_introduccion'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'reforma_alcance_introduccion'));
        $this->load->template_frontend('reforma/alcance', $data);
    }
}