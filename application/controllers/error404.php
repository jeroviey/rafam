<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');

class Error404 extends MX_Controller { 
   
   public function index()
   {
      $this->load->template_frontend('error/error_404.php');
   }

}