<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Noticia extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index($page = 1)
    {
        $datos['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'noticias'));
        $rows = $this->em->getRepository('Entity\Noticia')->findBy(array(),array('created_at' => 'DESC'));
        $config = init_pagination("noticia/index",count($rows), 10, 4);
        $datos['noticias'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $datos["links"] = $this->pagination->create_links();
        $this->load->template_frontend('noticia/index', $datos);
    }

    public function detalles($id)
    {
        $datos['noticia'] = $this->em->getRepository('Entity\Noticia')->findOneById($id);
        $this->load->template_frontend('noticia/detalles',$datos);           
    }    
}