<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Actualizacion extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index()
    {
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'software_actualizaciones'));
        $modulos = $this->em->getRepository('Entity\Modulo')->findAll();
        $actualizaciones = array();
        foreach ($modulos as $modulo)
        {
            $actualizacion = $modulo->ultima_actualizacion();
            if ($actualizacion)
            {
                $actualizaciones[] = $actualizacion;
            }
        }
        $data['actualizaciones'] = $actualizaciones;

        $this->load->template_frontend('actualizacion/software_actualizaciones',$data);           
    }

    public function versiones_anteriores()
    {
        $data['actualizaciones'] = $this->em->getRepository('Entity\Actualizacion')->actualizacionesOrdenadas();
        $this->load->template_frontend('actualizacion/versiones_anteriores',$data); 
    }

    public function descargar_zip($id_actualizacion)
    {
        $actualizacion = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);

        $estado = descargar($actualizacion->get_archivo(),base_url().'actualizacion');
    }

    public function ver_pdf($id_documento)
    {
        $documento = $this->em->getRepository('Entity\Documento')->find($id_documento);
        ver($documento->get_path(), base_url().'actualizacion');
    }
    
}