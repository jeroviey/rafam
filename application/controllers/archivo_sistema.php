<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Archivo_sistema extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    public function descargar($id)
    {
        $data['archivo'] = $this->em->getRepository('Entity\ArchivoSistema')->find($id);
        $this->load->template_frontend('archivos_sistema/descarga',$data);
    }

    public function descargar_archivo($id)
    {
        $archivo = $this->em->getRepository('Entity\ArchivoSistema')->find($id);
        $path = $this->config->item('path_archivos_sistema').$archivo->get_archivo();

        descargar($path, base_url());
    }
    
}