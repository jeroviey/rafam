<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Mesa_ayuda extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
        $this->load->helper('recaptchalib');
    }
    
    public function index()
    {
    	$data['municipios'] = $this->em->getRepository('Entity\Municipio')->findAll();
        $data['contacto'] = new Entity\Contacto;
        $this->cargar_datos_contacto($data['contacto']);
        $form = $this->input->post('contacto', true);
        $data['tipo_trabajo_selected'] =  ($form && array_key_exists('tipo_trabajo', $form)) ? $form['tipo_trabajo'] : 0;
        $data['municipio_selected'] = $form ? $form['municipio'] : '0';
        $data['recapcha_key'] = $this->config->item('public_key');

        $this->load->template_frontend('mesa_ayuda/mesa_ayuda', $data);
    }

    public function cargar_datos_contacto($contacto)
    {
        $formulario = $this->input->post('contacto', true);
        if($formulario)
        {
            $contacto->set_nombre($formulario['nombre']);
            $contacto->set_apellido($formulario['apellido']);
            $contacto->set_email($formulario['email']);
            $contacto->set_cargo($formulario['cargo']);
            $contacto->set_oficina($formulario['oficina']);
            $contacto->set_lugar_trabajo($formulario['lugar_trabajo']);
            $municipio = $this->em->getRepository('Entity\Municipio')->find($formulario['municipio']);
            if($municipio)
            {
                $contacto->set_municipio($municipio);
            }
            $contacto->set_asunto($formulario['asunto']);
            $contacto->set_mensaje($formulario['mensaje']);
        }
    }

    public function procesar_contacto()
    {
        if ($this->form_validation->run($this, 'contacto'))
        {
            $contacto = new Entity\Contacto;
            $this->cargar_datos_contacto($contacto);
            $this->em->persist($contacto);
            $this->em->flush();

            $email = 'rafam@ec.gba.gov.ar';
            $subject = 'RAFAM - Nuevo mensaje mesa de ayuda';
            $message = $this->load->view('template/mesa_ayuda/contacto', array('contacto' => $contacto), true);
            enviar_email($email,$subject,$message);
            
            $this->load->template_frontend('mesa_ayuda/mesa_ayuda_confirmacion');
        }
        else
        {
            $this->index();
        }
    }


    //CALLBACKS
    public function municipio_check($municipio, $form)
    {
        // die(var_dump('llegue a municipio check'));
    	return municipio_check($municipio, $form, $this);
    }

    public function lugar_trabajo_check($lugar_trabajo, $form)
    {
        return lugar_trabajo_check($lugar_trabajo, $form, $this);
    }

    public function valid_captcha()
    {
        $reCaptcha = new ReCaptcha($this->config->item('private_key'));
        // Was there a reCAPTCHA response?
        if ($_POST["g-recaptcha-response"])
        {
            $resp = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }
        else
        {
            $this->form_validation->set_message('valid_captcha', 'El campo es obligatorio'); 
            return false;
        }

        if ($resp != null && $resp->success)
        { 
            return true;
        }
        else
        { 
            $this->form_validation->set_message('valid_captcha', 'El captcha ingresado es incorrecto');
            return false;
        }
    }
    
}