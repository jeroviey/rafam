<?php

if (!defined('BASEPATH'))
    exit("No direct script access allowed");

class Migrate extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('migration');
        $this->em = $this->doctrine->em;
    }

    public function index() 
    {
        if (!$this->migration->latest()) 
        {
            show_error($this->migration->error_string());     
        }
    }

    public function load_municipios()
    {   
        $csv = array();
        $lines = file(base_url().'/files/migracion/municipios.csv', FILE_IGNORE_NEW_LINES);
        foreach ($lines as $key => $value)
        {
            $datos = explode(";", str_getcsv($value)[0]);
            $municipio = new Entity\Municipio;
            $municipio->set_codigo_municipio($datos[0]);
            $municipio->set_nombre(utf8_encode($datos[1]));
            $this->em->persist($municipio);
        }
        $this->em->flush();   
        echo "se cargaron los ".count($this->em->getRepository('Entity\Municipio')->findAll())." municipios <br>";
    }

    public function load_usuarios()
    {
        $usuario = new Entity\Usuario;
        $usuario->set_username('webrafam');
        $usuario->set_password(sha1('cai319'));
        $usuario->set_nombre('usuario administrativo');
        $usuario->set_apellido('usuario');
        $usuario->set_email('admin@admin.com');
        $usuario->set_estado(1);

        $this->em->persist($usuario);

        $usuario = new Entity\Usuario;
        $usuario->set_username('guillermina.belli');
        $usuario->set_password(sha1('super'));
        $usuario->set_nombre('usuario administrativo');
        $usuario->set_apellido('usuario');
        $usuario->set_email('admin@admin.com');
        $usuario->set_estado(1);

        $this->em->persist($usuario);
        
        $this->em->flush();

        echo "se cargaron ".count($this->em->getRepository('Entity\Usuario')->findAll())." usuarios en la base de datos <br>";
    }

    public function load_modulos()
    {
        $modulo = new Entity\Modulo;
        $modulo->set_nombre('CAS');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('PRESUPUESTO');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('CONTRATACIONES');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('BIENES FISICOS');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('CONTABILIDAD');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('TESORERÍA');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('ADMINISTRACIÓN DE INGRESOS PÚBLICOS');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('ADMINISTRACIÓN DEL PERSONAL');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('CRÉDITO PÚBLICO');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('INVERSIÓN PÚBLICA');
        $this->em->persist($modulo);

        $modulo = new Entity\Modulo;
        $modulo->set_nombre('ESTADÍSTICA');
        $this->em->persist($modulo);

        $this->em->flush();

        echo "se cargaron los ".count($this->em->getRepository('Entity\Modulo')->findAll())." modulos en la base de datos <br>";
    }

    public function load_profesores()
    {
        $profe1 = new Entity\Profesor;
        $profe1->set_nombre('Felipe');
        $profe1->set_apellido('Morales');

        $profe2 = new Entity\Profesor;
        $profe2->set_nombre('Daniel');
        $profe2->set_apellido('Molinari');

        $profe3 = new Entity\Profesor;
        $profe3->set_nombre('Pablo');
        $profe3->set_apellido('Britos');

        $profe4 = new Entity\Profesor;
        $profe4->set_nombre('Esteban');
        $profe4->set_apellido('Álvarez');

        $profe5 = new Entity\Profesor;
        $profe5->set_nombre('Mariana');
        $profe5->set_apellido('Chile');

        $profe6 = new Entity\Profesor;
        $profe6->set_nombre('Silvia');
        $profe6->set_apellido('Castillo');

        $profe7 = new Entity\Profesor;
        $profe7->set_nombre('Mauro');
        $profe7->set_apellido('Ricarde');

        $profe8 = new Entity\Profesor;
        $profe8->set_nombre('Magdalena');
        $profe8->set_apellido('Del Val');

        $this->em->persist($profe1);
        $this->em->persist($profe2);
        $this->em->persist($profe3);
        $this->em->persist($profe4);
        $this->em->persist($profe5);
        $this->em->persist($profe6);
        $this->em->persist($profe7);
        $this->em->persist($profe8);
        $this->em->flush();

        echo "se cargaron ".count($this->em->getRepository('Entity\Profesor')->findAll())." profesores en la base de datos <br>";
    }

    public function load_cursos()
    {
        $c1 = new Entity\Curso;
        $c1->set_nombre('CAS');
        $c1->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'CAS')));
        $clase1 = new Entity\Clase;
        $clase1->set_mes(8);
        $clase1->set_dias('6 y 7');
        $clase1->set_lugar('aula 322');
        $clase1->set_curso($c1);
        $c1->add_clase($clase1);
        $c1->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Britos')));
        $c1->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Álvarez')));

        $c2 = new Entity\Curso;
        $c2->set_nombre('PRESUPUESTO');
        $c2->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'PRESUPUESTO')));
        $clase2 = new Entity\Clase;
        $clase2->set_mes(4);
        $clase2->set_dias('14 y 15');
        $clase2->set_lugar('aula 322');
        $clase2->set_curso($c2);
        $clase3 = new Entity\Clase;
        $clase3->set_mes(9);
        $clase3->set_dias('10 y 11');
        $clase3->set_lugar('aula 322');
        $clase3->set_curso($c2);
        $c2->add_clase($clase2);
        $c2->add_clase($clase3);
        $c2->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Molinari')));

        $c3 = new Entity\Curso;
        $c3->set_nombre('CONTRATACIONES Y BIENES FISICOS');
        $c3->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'CONTRATACIONES')));
        $clase4 = new Entity\Clase;
        $clase4->set_mes(5);
        $clase4->set_dias('28 y 29');
        $clase4->set_lugar('aula 322');
        $clase4->set_curso($c3);
        $clase5 = new Entity\Clase;
        $clase5->set_mes(9);
        $clase5->set_dias('24 y 25');
        $clase5->set_lugar('aula 322');
        $clase5->set_curso($c3);
        $c3->add_clase($clase4);
        $c3->add_clase($clase5);
        $c3->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Chile')));

        $c4 = new Entity\Curso;
        $c4->set_nombre('CONTABILIDAD');
        $c4->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'CONTABILIDAD')));
        $clase6 = new Entity\Clase;
        $clase6->set_mes(6);
        $clase6->set_dias('10, 11 y 12');
        $clase6->set_lugar('aula 322');
        $clase6->set_curso($c4);
        $clase7 = new Entity\Clase;
        $clase7->set_mes(10);
        $clase7->set_dias('7, 8 y 9');
        $clase7->set_lugar('aula 322');
        $clase7->set_curso($c4);
        $c4->add_clase($clase6);
        $c4->add_clase($clase7);
        $c4->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Molinari')));

        $c5 = new Entity\Curso;
        $c5->set_nombre('TESORERÍA');
        $c5->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'TESORERÍA')));
        $clase8 = new Entity\Clase;
        $clase8->set_mes(7);
        $clase8->set_dias('2 y 3');
        $clase8->set_lugar('aula 322');
        $clase8->set_curso($c5);
        $clase9 = new Entity\Clase;
        $clase9->set_mes(11);
        $clase9->set_dias('5 y 6');
        $clase9->set_lugar('aula 322');
        $clase9->set_curso($c5);
        $c5->add_clase($clase8);
        $c5->add_clase($clase9);
        $c5->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Molinari')));

        $c6 = new Entity\Curso;
        $c6->set_nombre('ADMINISTRACIÓN DE INGRESOS PÚBLICOS');
        $c6->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'ADMINISTRACIÓN DE INGRESOS PÚBLICOS')));
        $clase10 = new Entity\Clase;
        $clase10->set_mes(4);
        $clase10->set_dias('22, 23 y 24');
        $clase10->set_lugar('aula 322');
        $clase10->set_curso($c6);
        $clase11 = new Entity\Clase;
        $clase11->set_mes(8);
        $clase11->set_dias('19, 20 y 21');
        $clase11->set_lugar('aula 322');
        $clase11->set_curso($c6);
        $clase12 = new Entity\Clase;
        $clase12->set_mes(11);
        $clase12->set_dias('11, 12 y 13');
        $clase12->set_lugar('aula 322');
        $clase12->set_curso($c6);
        $c6->add_clase($clase10);
        $c6->add_clase($clase11);
        $c6->add_clase($clase12);
        $c6->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Morales')));
        $c6->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Castillo')));

        $c7 = new Entity\Curso;
        $c7->set_nombre('ADMINISTRACIÓN DE PERSONAL');
        $c7->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'ADMINISTRACIÓN DEL PERSONAL')));
        $clase13 = new Entity\Clase;
        $clase13->set_mes(4);
        $clase13->set_dias('14 y 15');
        $clase13->set_lugar('aula 322');
        $clase13->set_curso($c7);
        $clase14 = new Entity\Clase;
        $clase14->set_mes(8);
        $clase14->set_dias('13 y 14');
        $clase14->set_lugar('aula 322');
        $clase14->set_curso($c7);
        $clase15 = new Entity\Clase;
        $clase15->set_mes(10);
        $clase15->set_dias('15 y 16');
        $clase15->set_lugar('aula 322');
        $clase15->set_curso($c7);
        $c7->add_clase($clase13);
        $c7->add_clase($clase14);
        $c7->add_clase($clase15);
        $c7->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Ricarde')));

        $c8 = new Entity\Curso;
        $c8->set_nombre('CRÉDITO PÚBLICO');
        $c8->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'CRÉDITO PÚBLICO')));
        $clase16 = new Entity\Clase;
        $clase16->set_mes(6);
        $clase16->set_dias('5');
        $clase16->set_lugar('aula 322');
        $clase16->set_curso($c8);
        $c8->add_clase($clase16);
        $c8->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Molinari')));

        $c9 = new Entity\Curso;
        $c9->set_nombre('INVERSIÓN PÚBLICA');
        $c9->set_modulo($this->em->getRepository('Entity\Modulo')->findOneBy(array('nombre' => 'INVERSIÓN PÚBLICA')));
        $clase17 = new Entity\Clase;
        $clase17->set_mes(7);
        $clase17->set_dias('17');
        $clase17->set_lugar('aula 322');
        $clase17->set_curso($c9);
        $c9->add_clase($clase17);
        $c9->add_profesor($this->em->getRepository('Entity\Profesor')->findOneBy(array('apellido' => 'Del Val')));

        $this->em->persist($c1);
        $this->em->persist($c2);
        $this->em->persist($c3);
        $this->em->persist($c4);
        $this->em->persist($c5);
        $this->em->persist($c6);
        $this->em->persist($c7);
        $this->em->persist($c8);
        $this->em->persist($c9);

        $this->em->flush();

        echo "se cargaron ".count($this->em->getRepository('Entity\Curso')->findAll())." cursos en la base de datos <br>";
    }

    public function load_actualizaciones()
    {
        //PRESUPUESTO
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('PRESUPUESTO');
               
        $actualizacion1 = new Entity\Actualizacion;
        $actualizacion1->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion1->set_tipo('RELEASE');
        $actualizacion1->set_archivo($this->config->item('path_actualizaciones').'PRE_007_000_002.zip');
        $actualizacion1->set_version('7.0.2');
        $modulo->add_actualizacion($actualizacion1);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('PRE_007_000_002.pdf');
        $documento->set_path($this->config->item('path_documentos').'PRE_007_000_002.pdf');
        $actualizacion1->add_documento($documento);

        //CONTRATACIONES
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('CONTRATACIONES');
               
        $actualizacion2 = new Entity\Actualizacion;
        $actualizacion2->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion2->set_tipo('RELEASE');
        $actualizacion2->set_archivo($this->config->item('path_actualizaciones').'CTR_007_000_005.zip');
        $actualizacion2->set_version('7.1.5');
        $modulo->add_actualizacion($actualizacion2);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('CTR_007_000_005.pdf');
        $documento->set_path($this->config->item('path_documentos').'CTR_007_000_005.pdf');
        $actualizacion2->add_documento($documento);

        //BIENES FISICOS
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('BIENES FISICOS');

        $actualizacion3 = new Entity\Actualizacion;
        $actualizacion3->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion3->set_tipo('RELEASE');
        $actualizacion3->set_archivo($this->config->item('path_actualizaciones').'BIE_007_000_005.zip');
        $actualizacion3->set_version('7.1.5');
        $modulo->add_actualizacion($actualizacion3);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('BIE_007_000_005.pdf');
        $documento->set_path($this->config->item('path_documentos').'BIE_007_000_005.pdf');
        $actualizacion3->add_documento($documento);

        //CONTABILIDAD
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('CONTABILIDAD');
               
        $actualizacion4 = new Entity\Actualizacion;
        $actualizacion4->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion4->set_tipo('RELEASE');
        $actualizacion4->set_archivo($this->config->item('path_actualizaciones').'CTA_007_001_003.zip');
        $actualizacion4->set_version('7.1.3');
        $modulo->add_actualizacion($actualizacion4);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('CTA_007_001_003.pdf');
        $documento->set_path($this->config->item('path_documentos').'CTA_007_001_003.pdf');
        $actualizacion4->add_documento($documento);

        //TESORERÍA
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('TESORERÍA');
               
        $actualizacion5 = new Entity\Actualizacion;
        $actualizacion5->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion5->set_tipo('RELEASE');
        $actualizacion5->set_archivo($this->config->item('path_actualizaciones').'TES_007_000_004.zip');
        $actualizacion5->set_version('7.0.4');
        $modulo->add_actualizacion($actualizacion5);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('TES_007_000_004.pdf');
        $documento->set_path($this->config->item('path_documentos').'TES_007_000_004.pdf');
        $actualizacion5->add_documento($documento);

        //ADMINISTRACIÓN DEL PERSONAL
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('ADMINISTRACIÓN DEL PERSONAL');
               
        $actualizacion6 = new Entity\Actualizacion;
        $actualizacion6->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion6->set_tipo('VERSION');
        $actualizacion6->set_archivo($this->config->item('path_actualizaciones').'PER_007_003.zip');
        $actualizacion6->set_version('7.3');
        $modulo->add_actualizacion($actualizacion6);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('PER_007_003.pdf');
        $documento->set_path($this->config->item('path_documentos').'PER_007_003.pdf');
        $actualizacion6->add_documento($documento);

        //CRÉDITO PÚBLICO
        $modulo = $this->em->getRepository('Entity\Modulo')->findOneByNombre('CRÉDITO PÚBLICO');
               
        $actualizacion7 = new Entity\Actualizacion;
        $actualizacion7->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $actualizacion7->set_tipo('VERSION');
        $actualizacion7->set_archivo($this->config->item('path_actualizaciones').'CRE_006_002.zip');
        $actualizacion7->set_version('6.2');
        $modulo->add_actualizacion($actualizacion7);
        
        $documento = new Entity\Documento;
        $documento->set_nombre('CRE_006_002.pdf');
        $documento->set_path($this->config->item('path_documentos').'CRE_006_002.pdf');
        $actualizacion7->add_documento($documento);

        $this->em->flush();
        
        echo "se cargaron las ".count($this->em->getRepository('Entity\Actualizacion')->findAll())." actualizaciones en la base de datos <br>";
        echo "se cargaron los ".count($this->em->getRepository('Entity\Documento')->findAll())." documentos en la base de datos <br>";
    }

    public function load_temas()
    {
        $t1 = new Entity\Tema;
        $t1->set_nombre('CONFIGURACIÓN GENERAL');
        $t1->set_orden(0);

        $t2 = new Entity\Tema;
        $t2->set_nombre('BIENES FÍSICOS');
        $t2->set_orden(1);

        $t3 = new Entity\Tema;
        $t3->set_nombre('CONTABILIDAD');
        $t3->set_orden(2);

        $t4 = new Entity\Tema;
        $t4->set_nombre('CONTRATACIONES');
        $t4->set_orden(3);
        
        $t5 = new Entity\Tema;
        $t5->set_nombre('ADMINISTRACIÓN DE PERSONAL');
        $t5->set_orden(4);

        $t6 = new Entity\Tema;
        $t6->set_nombre('ADMINISTRACIÓN DE INGRESOS PÚBLICOS');
        $t6->set_orden(5);

        $this->em->persist($t1);
        $this->em->persist($t2);
        $this->em->persist($t3);
        $this->em->persist($t4);
        $this->em->persist($t5);
        $this->em->persist($t6);
        $this->em->flush();

        echo "se cargaron los ".count($this->em->getRepository('Entity\Tema')->findAll())." temas en la base de datos <br>";
    }

    public function load_faqs()
    {
        $this->config->load('info_faqs');
        $algo = $this->config->item('faqs');
        foreach($algo as $a){
            $p1 = new Entity\Pregunta;
            $p1->set_tema($this->em->getRepository('Entity\Tema')->find($a[0]));
            $p1->set_orden($a[1]);
            $p1->set_pregunta($a[2]);
            $p1->set_respuesta($a[3]);
            $this->em->persist($p1);
        }
        $this->em->flush();

        echo "se cargaron las ".count($this->em->getRepository('Entity\Pregunta')->findAll())." preguntas en la base de datos <br>";
    }

    public function load_tipos_documentacion()
    {
        $tipo1 = new Entity\TipoDocumentacion;
        $tipo1->set_nombre('Manual de usuario');
        $this->em->persist($tipo1);

        $tipo2 = new Entity\TipoDocumentacion;
        $tipo2->set_nombre('Informe de errores');
        $this->em->persist($tipo2);

        $tipo3 = new Entity\TipoDocumentacion;
        $tipo3->set_nombre('Migración de ingresos brutos');
        $this->em->persist($tipo3);
        
        $this->em->flush();

        echo "se cargaron los ".count($this->em->getRepository('Entity\TipoDocumentacion')->findAll())." tipos de documentacion. <br>";
    }

    public function load_documentacion()
    {
        $fecha_subida = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));
        $tipo_documentacion = $this->em->getRepository('Entity\TipoDocumentacion')->findOneBy(array('nombre' => 'Manual de usuario'));

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('01_bienes_fisicos');
        $documentacion->set_path($this->config->item('path_documentaciones').'01_bienes_fisicos.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('02_MU_CAS');
        $documentacion->set_path($this->config->item('path_documentaciones').'02_MU_CAS.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('03_credito_publico');
        $documentacion->set_path($this->config->item('path_documentaciones').'03_credito_publico.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('04_contabilidad');
        $documentacion->set_path($this->config->item('path_documentaciones').'04_contabilidad.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('05_contrataciones');
        $documentacion->set_path($this->config->item('path_documentaciones').'05_contrataciones.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('06_ingresos_publicos');
        $documentacion->set_path($this->config->item('path_documentaciones').'06_ingresos_publicos.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('07_inversion_publica');
        $documentacion->set_path($this->config->item('path_documentaciones').'07_inversion_publica.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('08_personal');
        $documentacion->set_path($this->config->item('path_documentaciones').'08_personal.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('09_presupuesto');
        $documentacion->set_path($this->config->item('path_documentaciones').'09_presupuesto.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('10_tesoreria');
        $documentacion->set_path($this->config->item('path_documentaciones').'10_tesoreria.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $tipo_documentacion = $this->em->getRepository('Entity\TipoDocumentacion')->findOneBy(array('nombre' => 'Informe de errores'));

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('IPE_Rafam');
        $documentacion->set_path($this->config->item('path_documentaciones').'IPE_Rafam.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('IPE_Rafam_Instructivo');
        $documentacion->set_path($this->config->item('path_documentaciones').'IPE_Rafam_Instructivo.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $tipo_documentacion = $this->em->getRepository('Entity\TipoDocumentacion')->findOneBy(array('nombre' => 'Migración de ingresos brutos'));

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('MIG_ING_Arquitectura');
        $documentacion->set_path($this->config->item('path_documentaciones').'MIG_ING_Arquitectura.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $documentacion = new Entity\Documentacion;
        $documentacion->set_nombre('MIG_ING_Instructivo');
        $documentacion->set_path($this->config->item('path_documentaciones').'MIG_ING_Instructivo.pdf');
        $documentacion->set_fecha_subida($fecha_subida);
        $tipo_documentacion->add_documentacion($documentacion);
        $this->em->persist($documentacion);

        $this->em->flush();

        echo "se cargaron ".count($this->em->getRepository('Entity\Documentacion')->findAll())." documentaciones. <br>";
    }

    public function load_titulos_contenidos()
    {
        // EVOLUCION
        $titulo = new Entity\Titulo;
        $titulo->set_clave('reforma_evolucion');
        $titulo->set_nombre('Evolución');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Marco Institucional y legal');
        $contenido->set_descripcion('A partir del año 2000 el Gobierno de la Provincia de Buenos Aires, a través del Ministerio de Economía, impulsó una reforma integral de la administración financiera y de los recursos reales en el ámbito de los municipios bonaerenses. Es así que surge el Decreto Provincial Nº 2980/00, el cual define la Reforma de la Administración Financiera en el Ámbito Municipal (RAFAM) incluyendo los postulados básicos desarrollados por el Estado Nacional desde el año 1992, con la sanción de la Ley N° 24.156. La consideración de una visión ampliada de la administración financiera hizo que el modelo de gestión propuesto no tenga antecedentes a nivel nacional, provincial o municipal. A los módulos tradicionales de contabilidad, presupuesto, tesorería y crédito público, se agregaron el de administración de los ingresos públicos, y los de gestión de los recursos reales tales como recursos humanos, suministros y bienes de activo fijo, incluidos o no en los proyectos de inversión.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Implementación');
        $contenido->set_descripcion('La estrategia de implementación de la mencionada reforma implicó el desarrollo y puesta en funcionamiento del Sistema Informático RAFAM, un software que cuenta con todas las funcionalidades necesarias para soportar en forma transparente, ordenada y consistente con las normas vigentes, los procesos críticos de gestión financiera y contable en un municipio, a través de sus módulos, uno por cada sistema definido en el modelo conceptual y otro estrictamente informático. El sistema fue desarrollado por personal técnico informático de la Subsecretaría de Coordinación Económica del Ministerio de Economía provincial, conservando la propiedad intelectual de los desarrollos conceptuales e informáticos. Su código fuente es mantenido en forma exclusiva por el Ministerio, estando a cargo de la Dirección de Sistemas de Información Financiera Municipal, los servicios de soporte técnico, implantación y capacitación.

        Estructuralmente, el Software RAFAM cuenta con:

        - Un núcleo administrativo contable, el cual es integrado por los siguientes módulos: Presupuesto, Contabilidad, Tesorería, Crédito Público, Contrataciones, Administración de Bienes Físicos, Inversión Pública.
        - Un sistema de Administración de Personal.
        - Un sistema de Administración de Ingresos Públicos.
        El sistema fue cedido sin costo alguno a los municipios bonaerenses, lo que hace que actualmente, el núcleo administrativo-contable se encuentre instalado en el 92 % de ellos. Por otro lado, los módulos de administración de personal y administración de ingresos públicos, se encuentran en actual expansión.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(2);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Hacia una Reforma Integral');
        $contenido->set_descripcion('A más de diez años de la implementación de la Reforma, se pueden evidenciar los exitosos resultados como el aumento del caudal y automaticidad en los procesos y salidas de información, seguridad y transparencia en la gestión financiera y contable, mejora en los procesos de control, reducción de tiempo y recursos utilizados, posibilidad de programar y evaluar el presupuesto en forma participativa y descentralizada, homogeneidad de información en todos los municipios, entre otros.

        La normativa legal – Decreto RAFAM 2980/00 - que da sustento a la Reforma, junto con el Software RAFAM, fundamental en el comienzo de su implementación, son los pilares sobre los que hoy se puede seguir construyendo. En este sentido se ha incorporado el SIMCo, plataforma de consolidación, análisis y comunicación  de información para la toma de decisiones.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(3);

        $this->em->persist($contenido);


        // MISION
        $titulo = new Entity\Titulo;
        $titulo->set_clave('reforma_mision');
        $titulo->set_nombre('Misión');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Misión');
        $contenido->set_descripcion('Promover la integración entre la provincia y los municipios coordinando acciones en pos de lograr una mejora permanente en la gestión de las finanzas municipales, brindando los instrumentos normativos, de procesos y de soporte tecnológico que permitan una ágil, eficaz y transparente tarea de administración de los recursos financieros y reales de las comunas, logrando además proveer de información certera a la Provincia y promoviendo la implementación de pautas de trabajo comunes con el fin de lograr una mejor calidad de vida de los ciudadanos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // VISION
        $titulo = new Entity\Titulo;
        $titulo->set_clave('reforma_vision');
        $titulo->set_nombre('Visión');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Visión');
        $contenido->set_descripcion('Ser referentes a nivel nacional en el desarrollo de normativas y herramientas de administración de los recursos financieros y reales de nivel municipal donde la información producida de la actividad diaria alimente un gran sistema de información consolidada que provea un enfoque integral de las comunas en su conjunto.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // ALCANCE
        $titulo = new Entity\Titulo;
        $titulo->set_clave('reforma_alcance');
        $titulo->set_nombre('Alcance');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Núcleo administrativo contable');
        $contenido->set_descripcion('El sistema informático desarrollado con base en la Reforma está compuesto por nueve módulos. El grupo principal de módulos es el que componen el Núcleo Administrativo-Contable. Lo conforman los módulos de Presupuesto, Contrataciones, Contabilidad, Tesorería, Crédito Público, Bienes Físicos e Inversión Pública. Funcionan en forma integrada e interactúan entre sí permitiendo a los usuarios una ordenada gestión de las operaciones diarias a través de una registración única y automática de los hechos ocurridos en cada uno de los pasos que componen los distintos trámites de la Administración Financiera Municipal.

        La utilización de este núcleo permite obtener resultados tales como: aumento del caudal de información financiera y automaticidad en los procesos y en las salidas de información; seguridad y transparencia en el flujo de información; reducción de tiempo y recursos utilizados; cambio en la cultura organizacional hacia una mayor participación de funcionarios y empleados en la determinación de objetivos y metas; control más eficiente de las finanzas públicas; entre otras.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de ingresos públicos');
        $contenido->set_descripcion('Este módulo se integra al Núcleo Administrativo-Contable permitiendo realizar una íntegra administración de las Tasas Municipales. Por intermedio del mismo se obtiene una eficaz identificación de los contribuyentes, mayor confiabilidad de la información captada sobre las obligaciones fiscales y una gestión eficiente de las liquidaciones y cobros de todas las Tasas, como así también de las notificaciones e intimaciones a los deudores morosos. La conformación de la Cuenta Corriente por Contribuyente constituye un instrumento que posibilita un completo seguimiento de los cargos imputados a las personas físicas y jurídicas y de las cancelaciones que se produzcan en el transcurso de los distintos ejercicios fiscales.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(2);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de personal');
        $contenido->set_descripcion('Este módulo permite en la misma aplicación realizar la administración de los Recursos Humanos del municipio, como así también la liquidación de los haberes e implementar las interfaces con las distintas entidades tanto para la acreditación de los sueldos como para la rendición de los mismos frente a los organismos provinciales.

        Recopila la información referida a los movimientos que ocurren en la planta de personal, tales como incorporaciones, bajas y promociones. Mantiene actualizada la planta permanente y contratada, antigüedad, ausencias, manejo de licencias, entre otras prestaciones.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(3);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de configuración, auditoría y seguridad');
        $contenido->set_descripcion('Este módulo permite realizar la gestión integrada de la seguridad de todo el software RAFAM. A través de un adecuado uso de este módulo se realiza la administración de los permisos y accesos de los usuarios a los distintos módulos y dentro de estos a las diferentes opciones de menú. Además, se configura qué operaciones son sujetas a auditoria para tener un estricto control sobre quiénes y cuando hicieron determinadas acciones.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(4);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Área de capacitación');
        $contenido->set_descripcion('Anualmente se diagrama un completo cronograma de capacitación para todos los agentes municipales que deseen participar. Durante todo el año se dictan cursos sobre todos los módulos que componen la Reforma, donde se combina actividades conceptuales con tareas prácticas sobre el sistema informático. Los cursos son dictados por personal propio y siguen la modalidad de actividad presencial.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(5);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Mesa de ayuda');
        $contenido->set_descripcion('Todos aquellos organismos que implementen los módulos del software RAFAM tienen el derecho al servicio de Mesa de Ayuda, al cual se puede acceder de distintas formas: de manera telefónica, a través del mail y desde el link dedicado desde esta misma página web. El objetivo principal de la mesa de ayuda es el de responder de una manera oportuna, eficiente y con alta calidad a las peticiones que los usuarios realicen, en relación a los diversos aspectos, tanto conceptuales como de utilización del software. Entre las actividades se destacan: recibir todas las inquietudes de los municipios, documentarlas y separarlas entre errores de funcionamiento y requerimientos de nuevas funcionalidades.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(6);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Soporte técnico y administración de base de datos');
        $contenido->set_descripcion('Se presta asesoramiento sobre administración, configuración y otros aspectos técnicos relacionados con Servidores de aplicación y de Base de Datos. Se colabora con el organismo que necesite la instalación o soporte tanto del hardware y software relacionados con cualquier aspecto de las aplicaciones afines al software RAFAM. A solicitud del organismo que lo necesite se diagraman capacitaciones acerca del uso de la Base de Datos y de servidores de aplicación.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(7);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Área de imágen y comunicación');
        $contenido->set_descripcion('A partir del año 2014 se ha comenzado a desarrollar un conjunto de actividades destinadas a mejorar la comunicación con los usuarios finales. El objetivo es generar diferentes vías efectivas para poder resolver en forma eficiente todas aquellas consultas que nos hagan llegar.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(8);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Procesamiento y Consolidación de datos');
        $contenido->set_descripcion('La Reforma ha logrado la homogeneización en el uso de los clasificadores presupuestarios y contables, esto permite que todos los municipios remitan información similar sobre sus ejecuciones presupuestarias, lo cual facilita su procesamiento. Mediante la aplicación de criterios adicionales de normalización se logra la comparación y la consolidación de los datos, obteniendo información individual y del conjunto de municipios, insumo fundamental en el análisis de las finanzas municipales.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(9);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Análisis de las Finanzas Municipales');
        $contenido->set_descripcion('La Reforma permite analizar información de base y elaborar indicadores e informes sobre las finanzas municipales, que involucran tanto los ingresos, especialmente los tributos, como los gastos de los municipios bonaerenses. Estos productos tienen distintas finalidades. Por un lado, la publicación de distintos informes e indicadores permite a los municipios ver más allá de su propia realidad y poder compararse con el resto de las comunas. Por otro lado, los análisis también constituyen un diagnóstico útil para la toma de decisiones de políticas públicas, que sirven para favorecer al conjunto de los municipios pero también para disminuir las asimetrías existentes entre ellos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(10);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('SIMCo – Sistema de Información Municipal Consolidada');
        $contenido->set_descripcion('Se trata de un sitio WEB que sirve de plataforma de Comunicación entre la Dirección Provincial de Coordinación Municipal y los Municipios de la Provincia de Buenos Aires, donde la Dirección Provincial pondrá a disposición de los Municipios información sobre las transferencias remitidas y diferentes tipos de documentos producidos, para que pueda ser consultada durante las 24 hs. todos los días del año.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(11);

        $this->em->persist($contenido);


        // ALCANCE REFORMA
        $titulo = new Entity\Titulo;
        $titulo->set_clave('reforma_alcance_introduccion');
        $titulo->set_nombre('Introduccion');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Introduccion');
        $contenido->set_descripcion('El mapa muestra un enfoque complementario de la Reforma de la Administración Financiera en el Ámbito Municipal, donde además de la normativa, el sistema informático y los módulos en los cuales éste está divido, se cuenta con áreas de trabajo que solicitan la información a los municipios, la normalizan y consolidan, para luego ser analizada y producir distintos tipos de informes muy importantes para la toma de decisiones.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // DESCRIPCION
        $titulo = new Entity\Titulo;
        $titulo->set_clave('software_descripcion');
        $titulo->set_nombre('Descripción del software');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Introducción');
        $contenido->set_descripcion('Estructuralmente, el Software RAFAM cuenta con:

        - Un núcleo administrativo contable está compuesto por los siguientes módulos: Presupuesto, Contabilidad, Tesorería, Crédito Público, Contrataciones, Administración de Bienes Físicos, Inversión Pública.
        - Un módulo de Administración de Personal.
        - Un módulo de Administración de Ingresos Públicos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // DESCRIPCION NUCLEO
        $titulo = new Entity\Titulo;
        $titulo->set_clave('nucleo');
        $titulo->set_nombre('NÚCLEO ADMINISTRATIVO-CONTABLE');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de presupuesto');
        $contenido->set_descripcion('El Módulo de Presupuesto es uno de los módulos claves del Sistema Informático. A través del mismo se realiza la formulación presupuestaria por categorías programáticas, la programación física y financiera de los proyectos, la diagramación de las metas a realizar, la evaluación de las mismas en períodos de tiempo dentro del ejercicio anual y la reflejar los desvíos en caso que los hubiera. Además se destacan funcionalidades como las modificaciones presupuestarias, actualizaciones a los catálogos de los clasificadores presupuestarios, tanto de gastos como de recursos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Contabilidad');
        $contenido->set_descripcion('El Módulo de Contabilidad ejerce el rol central de integrador de la totalidad de los módulos. Se basa en principios y normas de general aceptación y en la registración automática de asientos por partida doble. Entre las funcionalidades destacadas se encuentran la registración de tres de las etapas del gasto: preventivo, compromiso y devengado. Se ingresan las facturas de los proveedores, se tramitan las órdenes de pago y todo tipo de regularizaciones de las etapas antes mencionadas. Además, mantiene actualizada la cuenta corriente de los proveedores.

        Se pueden obtener un gran número de listados y reportes, entre los que se destacan las ejecuciones presupuestarias y extrapresupuestarias de gastos y recursos, estados de situación económico-financiera, la cuenta o esquema de Ahorro-Inversión-Financiamiento, entre otros.

        Adicionalmente, se obtienen los listados y declaraciones juradas de las retenciones realizadas a los proveedores, que son de obligatoria presentación en organismos nacionales y provinciales.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(2);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Tesorería');
        $contenido->set_descripcion('Este módulo se ocupa de gerenciar los flujos de recursos y gastos, en coordinación con las áreas de Presupuesto, Crédito Público y Administración de Ingresos Públicos.

        Entre las operaciones que se pueden realizar se encuentran las de cancelación de órdenes de pago, confección de cheques y su posterior impresión automática, y el registro el pago a los proveedores cumpliendo así con la cuarta etapa de la ejecución presupuestaria establecida en la Reforma.

        Se registran las transferencias de fondos, los depósitos, y se gestionan las retenciones impositivas realizadas a los proveedores. Se registran todos los ingresos al organismo y se mantienen actualizadas las cuentas bancarias, permitiendo realizar periódicamente la conciliación automática.

        Entre los listados que se obtienen, los más relevantes son libro Caja, Bancos, Balance y Saldos de Tesorería.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(3);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Contrataciones');
        $contenido->set_descripcion('Por medio de este módulo se conforma y realiza todo el proceso de compras que puede efectuar un organismo. Se comienza por la operación denominada pedido de suministro, donde las distintas áreas manifiestan las necesidades que tienen en cuanto a adquisición o contratación de bienes o servicios. Luego se generan los pedidos de cotización a los distintos proveedores, pudiendo cargar en el sistema las ofertas para luego realizar la comparativa de precios y determinar quiénes son los aptos para concretar las adquisiciones, conformando así las órdenes de compra. Para poder realizar de manera ordenada los pedidos, el módulo propone un catálogo de bienes y servicios, el cual se puede actualizar según las diferentes necesidades de los municipios.

        Además, se mantiene un registro de proveedores donde se guarda toda la información referente a los mismos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(4);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Crédito Público');
        $contenido->set_descripcion('Con este módulo es posible obtener la actualización permanente de los registros sobre la deuda municipal y conocer, con la debida antelación, el perfil de vencimientos de los servicios que originan dichos pasivos. Facilita la evaluación de la real capacidad de endeudamiento a mediano y largo plazo.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(5);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Administración de Bienes Físicos');
        $contenido->set_descripcion('Con este módulo se registra, valora, revalúa y amortizan los bienes de uso del dominio privado y público de los organismos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(6);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Inversión Pública');
        $contenido->set_descripcion('En él se conforma un Banco de Proyectos, que incluye a los mismos en sus distintas fases de evaluación, de modo tal de disponer de información oportuna orientada a facilitar la toma de decisiones y obtener financiamiento para las iniciativas de inversión.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(7);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Módulo de Configuración, Auditoría y Seguridad (CAS)');
        $contenido->set_descripcion('Este módulo permite realizar la gestión integrada de la seguridad de todo el software RAFAM. Entre las operaciones más importantes que se realizan con la utilización del CAS son la creación, y mantenimiento de los usuarios que acceden a cada uno de los sistemas. Se definen los permisos y se determina que operaciones a ejecutar. Además, se pueden generar menús específicos con restricciones para determinados usuarios.

        A cada una de las operaciones de cada módulo se les puede asignar un tipo de auditoría que determinara el nivel de seguimiento correspondiente. El módulo contiene una sección para parametrizar todo el sistema, es decir, cada uno de los módulos tiene una cierta cantidad de variables que con diferentes valores, según las necesidades, hace que el comportamiento de los mismos sea diferente.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(8);

        $this->em->persist($contenido);


        // DESCRIPCION MODULO
        $titulo = new Entity\Titulo;
        $titulo->set_clave('modulo_ingresos_publicos');
        $titulo->set_nombre('MÓDULO DE ADMINISTRACIÓN DE INGRESOS PÚBLICOS');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Introducción');
        $contenido->set_descripcion('Por medio de esta aplicación se pueden gestionar y administrar los tributos municipales de manera sencilla y eficiente, constituyendo la Cuenta Corriente por Contribuyente un instrumento que posibilita un completo seguimiento de los cargos imputados a las personas físicas y jurídicas y de las cancelaciones que se produzcan en el transcurso de los distintos ejercicios fiscales.

        El sistema divide a los imponibles en cinco tipos: inmuebles, comercios, rodados, cementerio y contribuyentes. Para cada uno de estos se pueden definir una o varias tasas a liquidar. Las liquidaciones pueden realizarse por remesas, permitiendo tener un mejor control sobre los distintos lotes o conjuntos de imponibles.

        Se pueden definir distintos planes de pago, según las necesidades, para atender la problemática de los contribuyentes morosos. En este plano, el sistema permite realizar seguimiento de las notificaciones e intimaciones.

        El módulo posee interfaces de comunicación con las distintas entidades cobradoras que permiten una eficaz actualización de las cuentas corrientes de los contribuyentes.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // DESCRIPCION MODULO
        $titulo = new Entity\Titulo;
        $titulo->set_clave('modulo_personal');
        $titulo->set_nombre('MÓDULO DE ADMINISTRACIÓN DE PERSONAL');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Introducción');
        $contenido->set_descripcion('Por medio del módulo de Administración de Personal se puede compilar, resguardar, y mantener actualizada toda la información referida a los legajos y agentes de planta permanente y temporaria, y contratados. Se realizan operaciones tales como incorporaciones, bajas y promociones de agentes, se mantiene actualizada la antigüedad, las ausencias y los movimientos producidos por el personal entre las distintas áreas. Adicionalmente, se dispone de un módulo completo de licencias, donde los agentes solicitan cada una de las mismas y los funcionarios a cargo admiten y autorizan su cumplimiento.

        Otra operación muy importante es la liquidación de haberes. Se definen cada uno de los conceptos de liquidación, se los asocia a los agentes y mes a mes se realiza dicho proceso. Están incorporadas las interfaces tanto con el Banco, para realizar las transferencias a los agentes, como con otros organismos provinciales y nacionales a los cuales debe remitirse la información del plantel de personal.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);

        // ACTUALIZACIONES
        $titulo = new Entity\Titulo;
        $titulo->set_clave('software_actualizaciones');
        $titulo->set_nombre('Actualizaciones');

        $this->em->persist($titulo);

        // DOCUMENTACION
        $titulo = new Entity\Titulo;
        $titulo->set_clave('software_documentacion');
        $titulo->set_nombre('Documentación');

        $this->em->persist($titulo);

        // NORMATIVA
        $titulo = new Entity\Titulo;
        $titulo->set_clave('normativa');
        $titulo->set_nombre('Normativa');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Normativa');
        $contenido->set_descripcion('Reforma de la Administración Financiera en el Ambito Municipal




        La Plata, 01 de Septiembre de 2000

         

        Visto el expediente 2300-825/00 el cual consta de 2 (dos) cuerpos y la Ley Orgánica de las Municipalidades (Decreto-Ley 6769/58 y sus modificatorias) y el Reglamento de Contabilidad y Disposiciones de Administración para las Municipalidades de la Provincia de Buenos Aires, y,

        CONSIDERANDO:

        Que el Gobierno de la Provincia de Buenos Aires, con la intención de difundir las acciones de transformación encaradas en la propia Administración Provincial, ha decidido impulsar, con el apoyo financiero del Banco Mundial, un proyecto de reforma integral de la administración financiera y de los recursos reales en el ámbito de los municipios bonaerenses;

        Que el nuevo modelo que se propone impulsar recoge los postulados básicos que en la materia han sido desarrollados por el Estado Nacional desde el año 1992, con la sanción de la Ley N° 24.156 como, así también, las principales definiciones incluidas en el diseño conceptual de la reforma que fuera elaborado oportunamente para el sector público de la Provincia de Buenos Aires;

        Que desde un primer momento se visualizó la necesidad de incorporar sistemas que no fueron contemplados en las experiencias anteriormente mencionadas, tales como el Sistema de Inversiones y de Administración de Ingresos, y de adaptar dichos postulados a las particularidades que presenta el Sector Público Municipal;

        Que en el Reglamento de Contabilidad mencionado precedentemente deben ser incorporadas aquellas disposiciones que hagan a una moderna administración financiera y de recursos reales en el ámbito municipal bonaerense, de modo tal que los gobiernos comunales puedan cumplir en forma apropiada las funciones que le competen;

        Que tales incorporaciones tienen la finalidad de alcanzar una eficaz obtención y eficiente utilización de los recursos públicos, una correcta asignación de funciones y responsabilidades y la evaluación sistemática de los resultados alcanzados;

        Que las disposiciones a incluir en el Reglamento de Contabilidad y Disposiciones de Administración para las Municipalidades de la Provincia de Buenos Aires deben ser aplicadas en el ámbito municipal en forma progresiva, comenzando su instrumentación, en una primera etapa, en un conjunto de municipios seleccionados como piloto, para luego replicar todos los aspectos en el resto de los municipios;

        Que ha dictaminado la Asesoría General de Gobierno;

        Que conforme a la facultad otorgada por el Artículo 282° de la Ley Orgánica de las Municipalidades, procede dictar el acto administrativo correspondiente;

         

        Por ello,

         

        EL GOBERNADOR DE LA PROVINCIA DE BUENOS AIRES DECRETA

        Artículo 1°:Declárase iniciado el proceso de reforma de la administración de los recursos financieros y reales en el ámbito municipal de la Provincia de Buenos Aires.

        Artículo 2°: Facúltase al Ministro de Economía a determinar los municipios piloto que, en una primera etapa, instrumentarán dicho proceso de reforma, con el acuerdo de los Intendentes de los Partidos seleccionados como tales.

        Artículo 3°: Apruébanse las Disposiciones de Administración de los Recursos Financieros y Reales para los Municipios Piloto, junto con sus anexos, que forman parte del presente decreto. Dichas disposiciones serán, como un capítulo especial, parte integrante del Reglamento de Contabilidad y Disposiciones de Administración para las Municipalidades de la Provincia de Buenos Aires.

        Artículo 4°: Las normas que emanan de las Disposiciones de Administración de los Recursos Financieros y Reales para los Municipios Piloto, así como los formularios e instructivos que como anexos se adjuntan al mismo, serán de aplicación, en aquellos municipios que sean seleccionados como tales, a partir del primer ejercicio fiscal que se inicie con posterioridad a la sanción del presente Decreto.

        Artículo 5°: Las tareas de contralor externo de las actividades llevadas a cabo por los Municipios que resulten seleccionados como piloto, por parte del Honorable Tribunal de Cuentas, deberán contemplar las normas incluidas en las Disposiciones de Administración de los Recursos Financieros y Reales para Municipios Piloto.

        Artículo 6°: Las Disposiciones de Administración de los Recursos Financieros y Reales para Municipios Piloto deberán aplicarse, en los municipios que no fueran seleccionados como piloto, en un plazo que no deberá superar, como máximo, cinco (5) ejercicios fiscales, contados a partir de la vigencia de tales Disposiciones en los municipios piloto.

        Artículo 7°: El presente DECRETO será refrendado por los Señores Ministros Secretarios en los Departamentos de Economía y de Gobierno.

        Artículo 8°: Regístrese, comuníquese, publíquese, dése al Boletín Oficial, notifíquese al Señor Fiscal de Estado, y pase al Ministerio de Economía a sus efectos.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // CAPACITACION OBJETIVOS
        $titulo = new Entity\Titulo;
        $titulo->set_clave('capacitacion_objetivos');
        $titulo->set_nombre('Objetivos');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Objetivo');
        $contenido->set_descripcion('Esta iniciativa tiene como objetivo fortalecer las capacidades del personal municipal encargado de administrar el sistema RAFAM y generar un espacio de intercambio de experiencias entre todos los participantes. Para ello se han diseñado cursos de capacitación sobre todos los módulos del sistema RAFAM y sobre aspectos relativos al mantenimiento del mismo, los cuales sondictados por el equipo del área a través de una metodología teórico y práctica, según el caso. Los cursos son totalmente gratuitosy se encuentran abiertos a todos los agentes y funcionarios municipales usuarios del sistema con interés en ampliar o profundizar sus conocimientos relativos al uso y administración del sistema. Esperamos que los cursos sean de real utilidad para el Municipio y permitan aprovechar todas las potencialidades del sistema RAFAM.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // CAPACITACION CRONOGRAMA
        $titulo = new Entity\Titulo;
        $titulo->set_clave('capacitacion_cronograma');
        $titulo->set_nombre('Cronograma');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Cronograma');
        $contenido->set_descripcion('A continuación se visualizan los días en que se dictan cada uno de los cursos.

        Todos ellos se cursan en el aula 322 - Área de capacitación en el horario de 9:30 a 16.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // CAPACITACION PLANILLA
        $titulo = new Entity\Titulo;
        $titulo->set_clave('capacitacion_inscripcion');
        $titulo->set_nombre('Planilla de inscripción');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Importante:');
        $contenido->set_descripcion('Le solicitamos que para darse de baja en cualquiera de los cursos, se ponga en contacto con la Direccióncon la mayor anticipación posible.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Contacto');
        $contenido->set_descripcion('Email:cursosrafam@ec.gba.gov.ar

        Teléfono: 0221 429-4484 / 4509');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(2);

        $this->em->persist($contenido);


        // IMPORTANTE
        $titulo = new Entity\Titulo;
        $titulo->set_clave('capacitacion_inscripcion_importante');
        $titulo->set_nombre('Importante:');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Importante:');
        $contenido->set_descripcion('Le solicitamos que para darse de baja en cualquiera de los cursos, se ponga en contacto con la Direccióncon la mayor anticipación posible.');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);


        // CONTACTO
        $titulo = new Entity\Titulo;
        $titulo->set_clave('capacitacion_inscripcion_contacto');
        $titulo->set_nombre('Contacto');

        $this->em->persist($titulo);

        $contenido = new Entity\Contenido;
        $contenido->set_titulo('Contacto');
        $contenido->set_descripcion('Email:cursosrafam@ec.gba.gov.ar

        Teléfono: 0221 429-4484 / 4509');
        $contenido->add_titulo_padre($titulo);
        $contenido->set_orden(1);

        $this->em->persist($contenido);


        // NOTICIAS
        $titulo = new Entity\Titulo;
        $titulo->set_clave('noticias');
        $titulo->set_nombre('Listado de noticias');

        $this->em->persist($titulo);
        
        $this->em->flush();

        echo "se cargaron ".count($this->em->getRepository('Entity\Titulo')->findAll())." titulos. <br>";
        echo "se cargaron ".count($this->em->getRepository('Entity\Contenido')->findAll())." cantenidos. <br>";
    }

    public function load_imagenes()
    {
        $imagen = new Entity\Imagen;
        $imagen->set_nombre('organigrama');
        $imagen->set_imagen('organigrama.png');

        $this->em->persist($imagen);

        $imagen = new Entity\Imagen;
        $imagen->set_nombre('mision_y_funcion');
        $imagen->set_imagen('mision_y_funcion.png');

        $this->em->persist($imagen);

        $imagen = new Entity\Imagen;
        $imagen->set_nombre('prestaciones');
        $imagen->set_imagen('prestaciones.png');

        $this->em->persist($imagen);

        $imagen = new Entity\Imagen;
        $imagen->set_nombre('descripcion');
        $imagen->set_imagen('descripcion.png');

        $this->em->persist($imagen);

        $this->em->flush();
        echo "se cargaron ".count($this->em->getRepository('Entity\Imagen')->findAll())." imagenes. <br>";
    }

    public function load_notificacion_emails()
    {
        // OTROS 

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR6_HTC_II');
        $email->set_email('mgomez@tribctas.gba.gov.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR6_HTC_III');
        $email->set_email('vmbrelm@tribctas.gba.gov.ar');
        $this->em->persist($email);
        
        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_UNMDLP_IV');
        $email->set_email('valdez_irma@ciudad.com.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_UNMDLP');
        $email->set_email('rafam@eco.mdp.edu.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_UNLP_Besler');
        $email->set_email('besler@econo.unlp.edu.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Mfunes');
        $email->set_email('mpfunes@gmail.com');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Grid-IT_II');
        $email->set_email('alejandro.conforti@grid-it.com.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Grid-IT');
        $email->set_email('administracion@grid-it.com.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Dlamela');
        $email->set_email('lamelad@gmail.com');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Grid-IT_III');
        $email->set_email('celeste.rios@grid-it.com.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MClaromeco_Com');
        $email->set_email('delclaromeco@celcla.com.ar');
        $this->em->persist($email);         

        // ARRECIFES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(5);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MArrecifes_Com');
        $email->set_email('juanluistorres56@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // AVELLANEDA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(6);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MAvellaneda_SH');
        $email->set_email('sechacienda@avellaneda-ba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MAvellaneda_Com');
        $email->set_email('jestigarribia@avellaneda-ba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // AYACUCHO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(7);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MAyacucho_SH');
        $email->set_email('intenden@ayacucho.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // AZUL
        $municipio = $this->em->getRepository('Entity\Municipio')->find(8);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MAzul_SH ');
        $email->set_email('secgob@azul.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MAzul_Com');
        $email->set_email('computos@azul.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BAHIA BLANCA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(9);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBahiaBlanca_Com');
        $email->set_email('dsdcompras@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBahiaBlanca_SH');
        $email->set_email('intendencia@bb.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BALCARCE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(10);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBalcarce_Com');
        $email->set_email('vspinelli@balcarce.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBalcarce_SH');
        $email->set_email('privada@balcarce.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BARADERO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(11);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBaradero_SH');
        $email->set_email('ejecutivo@baradero.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BENITO JUAREZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(12);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBenitoJuarez_Com');
        $email->set_email('mvidela@benitojuarez.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBenitoJuarez_SH');
        $email->set_email('subcom@bjuarez.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BERAZATEGUI
        $municipio = $this->em->getRepository('Entity\Municipio')->find(13);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBerazategui_SH');
        $email->set_email('subcom@berazategui.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BERISSO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(14);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBerisso_Com');
        $email->set_email('computos3@berisso.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBerisso_SH');
        $email->set_email('privadaberisso@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BOLIVAR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(15);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBolivar_SH');
        $email->set_email('secretaria@munibolivar.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BRAGADO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(16);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBragado_Com');
        $email->set_email('bra_sistemas@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBragado_SH');
        $email->set_email('intendente@bragado.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // BRANDSEN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(17);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelBrandsen_SH');
        $email->set_email('subcom@brandsen.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CAMPANA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(18);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCampana_Com');
        $email->set_email('eiglina@campana.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCampana_SH');
        $email->set_email('secprivada@campana.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CAÑUELAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(19);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCañuelas_SH');
        $email->set_email('secretaria.privada@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCañuelas_Com');
        $email->set_email('computacioncanuelas@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CAPITAN SARMIENTO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(20);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCapitanSarmiento_SH');
        $email->set_email('subcom@sarmiento.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCapitanSarmiento_SH');
        $email->set_email('jjllaser@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CARLOS CASARES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(21);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarlosCasares_SH');
        $email->set_email('prensa@casares.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarlosCasares_Com');
        $email->set_email('juan@juanbidini.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CARLOS TEJEDOR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(22);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarlosTejedor_Com');
        $email->set_email('presupuesto_tejedor@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarlosTejedor_SH');
        $email->set_email('privada@carlostejedor.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CARMEN DE ARECO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(23);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarmenDeAreco_Com');
        $email->set_email('subcom@areco.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCarmenDeAreco_SH');
        $email->set_email('municareco@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CASTELLI
        $municipio = $this->em->getRepository('Entity\Municipio')->find(24);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCastelli_SH');
        $email->set_email('subcom@castelli.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CHACABUCO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(25);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MChacabuco_SH');
        $email->set_email('privadadechacabuco@yahoo.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CHASCOMUS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(26);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MChascomus_Com');
        $email->set_email('miniaramburu@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MChascomus_SH');
        $email->set_email('secretariachascomus@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CHIVILCOY
        $municipio = $this->em->getRepository('Entity\Municipio')->find(27);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MChivilcoy_Com');
        $email->set_email('computos@chivilcoy.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email); 

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MChivilcoy_SH');
        $email->set_email('hacienda@chivilcoy.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // COLON
        $municipio = $this->em->getRepository('Entity\Municipio')->find(28);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MColon_Com1');
        $email->set_email('marianelasorchille@yahoo.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MColon_Com2');
        $email->set_email('martingimenez24@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MColon_SH');
        $email->set_email('privadacolonb@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CORONEL DORREGO 
        $municipio = $this->em->getRepository('Entity\Municipio')->find(29);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelDorrego_SH');
        $email->set_email('subcom@dorrego.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CORONEL PRINGLES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(30);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelPringles_Com');
        $email->set_email('computos@pringles.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelPringles_SH');
        $email->set_email('subcom@pringles.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CORONEL ROSALES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(31);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelRosales_SH');
        $email->set_email('privadapuntalta@infovia.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // CORONEL SUAREZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(32);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MCoronelSuarez_SH');
        $email->set_email('mocceroricardoa@infovia.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // DAIREAUX
        $municipio = $this->em->getRepository('Entity\Municipio')->find(33);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MDaireaux_SH');
        $email->set_email('contaduriadx@dxred.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // DOLORES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(34);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MDolores_Com');
        $email->set_email('soporte@dolores.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MDolores_SH');
        $email->set_email('intendente@dolores.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ENSENADA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(35);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEnsenada_Com');
        $email->set_email('informatica@ensenada.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEnsenada_SH');
        $email->set_email('hacienda@ensenada.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ESCOBAR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(36);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEscobar_SH');
        $email->set_email('sandroguzman@escobar.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ESTEBAN ECHEVERRIA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(37);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEstebanEcheverria_Com');
        $email->set_email('informatica@estebanecheverria.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEstebanEcheverria_SH');
        $email->set_email('dgonzalez@estebanecheverria.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // EXALTACION DE LA CRUZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(38);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MExaltacionDeLaCruz_SH');
        $email->set_email('horacioerrazu@exaltaciondelacruz.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // EZEIZA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(39);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MuniR5_Espagnac_Ezeiza');
        $email->set_email('raulespagnac@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEzeiza_Com');
        $email->set_email('raulspagnac@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MEzeiza_SH');
        $email->set_email('secprivezeiza@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // FLORENCIO VARELA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(40);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MFlorencioVarela_SH');
        $email->set_email('privadavarela@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // FLORENTINO AMEGHINO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(41);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MFlorentinoAmeghino_Com');
        $email->set_email('franciscoiribarren@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MFlorentinoAmeghino_SH');
        $email->set_email('gobiernoameghino@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL ALVARADO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(42);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralAlvarado_Com');
        $email->set_email('computos@mga.gov.ar');
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralAlvarado_SH');
        $email->set_email('economia@mga.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL ALVEAR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(43);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralAlvear_SH');
        $email->set_email('secpriv@alvear.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL ARENALES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(44);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralArenales_SH');
        $email->set_email('subcom@arenales.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL BELGRANO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(45);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MBelgrano_Com');
        $email->set_email('secretaria1@belgrano.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralBelgrano_SH');
        $email->set_email('carmac655@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL GUIDO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(46);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralGuido_SH');
        $email->set_email('gobyhac@guido.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL LA MADRID
        $municipio = $this->em->getRepository('Entity\Municipio')->find(47);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralLamadrid_SH');
        $email->set_email('subcom@lamadrid.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL LAS HERAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(48);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralLasHeras_Com');
        $email->set_email('contaduriamglh@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL LAVALLE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(49);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLavalle_Com');
        $email->set_email('subcom@lavalle.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralLavalle_SH');
        $email->set_email('guillermomarchi@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL MADARIAGA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(50);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralMadariaga_Com');
        $email->set_email('rafam@telpin.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralMadariaga_SH');
        $email->set_email('intendente@madariaga.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL PAZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(51);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralPaz_SH');
        $email->set_email('diputadoveramendi@yahoo.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL PINTO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(52);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralPinto_Com');
        $email->set_email('computos@pinto.gob.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralPinto_SH');
        $email->set_email('galvanjulioe7@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL PUEYRREDON
        $municipio = $this->em->getRepository('Entity\Municipio')->find(53);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralPueyrredon_Com');
        $email->set_email('informatica@mardelplata.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralPueyrredon_SH');
        $email->set_email('mperezrojas@mardelplata.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL SAN MARTIN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(55);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralSanMartin_SH');
        $email->set_email('privadamgr@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL VIAMONTE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(56);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MViamonte_SH');
        $email->set_email('despacho@viamonte.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GENERAL VILLEGAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(57);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGeneralVillegas_Com');
        $email->set_email('gustavo.gonzalez@villegas.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVillegas_SH');
        $email->set_email('subcom@villegas.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // GUAMINI
        $municipio = $this->em->getRepository('Entity\Municipio')->find(58);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGuamini_Com');
        $email->set_email('gcomputos@net.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MGuamini_SH');
        $email->set_email('secretariadegobierno@guamini.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // HURLINGHAM
        $municipio = $this->em->getRepository('Entity\Municipio')->find(60);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MHurlingham_SH');
        $email->set_email('privada@munhurli.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ITUZAINGO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(61);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MItuzaingo_Com');
        $email->set_email('computos@miituzaingo.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // JOSE C. PAZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(62);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MJoseCPaz_Com');
        $email->set_email('sistemas@josecpaz.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MJosecpaz_SH');
        $email->set_email('privada@josecpaz.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // JUNIN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(63);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MJunin_Com');
        $email->set_email('desarrolloweb@junin.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MJunin_SH');
        $email->set_email('prensa@junin.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LA COSTA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(64);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLaCosta_Com');
        $email->set_email('josistemas@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLacosta_SH');
        $email->set_email('gunales@lacosta.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LA MATANZA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(65);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLaMatanza_SH');
        $email->set_email('fernandoespinoza@lamatanza.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LA PLATA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(66);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLaPlata_Com');
        $email->set_email('ncampano@laplata.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLa Plata_SH');
        $email->set_email('ccerruti@laplata.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LANUS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(67);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLanus_Com');
        $email->set_email('clage@lanus.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLanus_SH');
        $email->set_email('dariodiazperez@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LAPRIDA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(68);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLaprida_Com');
        $email->set_email('sistemas@laprida.net');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLaprida_SH');
        $email->set_email('mlprivada@laprida.net');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LAS FLORES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(69);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLasFlores_Com');
        $email->set_email('computos@futurtel.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLasflores_SH');
        $email->set_email('hacienda1@futurtel.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LINCOLN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(72);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLincoln_Com');
        $email->set_email('contador@lincoln.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLincoln_SH');
        $email->set_email('privint@lincoln.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LOBERIA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(73);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLoberia_Com');
        $email->set_email('mbgoyhenespe@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLoberia_SH');
        $email->set_email('bresciano@loberia.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LOBOS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(74);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLobos_Com');
        $email->set_email('contadora@lobos.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLobos_SH');
        $email->set_email('subcom@lo.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LOMAS DE ZAMORA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(75);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLomasdezamora_SH');
        $email->set_email('ceremonial@lomasdezamora.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // LUJAN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(76);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MLujan_SH');
        $email->set_email('grosso@lujan.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MAGDALENA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(77);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMagdalena_SH');
        $email->set_email('computos@magdalena.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MAIPU
        $municipio = $this->em->getRepository('Entity\Municipio')->find(78);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMaipu_Com');
        $email->set_email('subcom@maipu.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMaipu_SH');
        $email->set_email('lorenaperez2810@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MALVINAS ARGENTINAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(79);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMalvinasArgentinas_SH');
        $email->set_email('intendencia@malvinas.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MAR CHIQUITA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(80);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMarChiquita_Com');
        $email->set_email('computos@marchiquita.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMarchiquita_SH');
        $email->set_email('unidadintendente@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // MARCOS PAZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(81);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMarcospaz_SH');
        $email->set_email('mgonzalez@municipiomarcospaz.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MERCEDES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(82);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMercedes_Com');
        $email->set_email('sistemas_mercedes@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMercedes_SH');
        $email->set_email('subcom@mercedes.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MERLO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(83);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMerlo_SH');
        $email->set_email('ceremonialmerlo@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MONTE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(84);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMonte_Com');
        $email->set_email('subcom@monte.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MONTE HERMOSO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(85);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMonteHermoso_SH');
        $email->set_email('sececonomia@montehermoso.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMonteHermoso_Com');
        $email->set_email('gabriel.bertone@montehermoso.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMhermoso_SH');
        $email->set_email('prensa@montehermoso.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MORENO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(86);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMoreno_SH');
        $email->set_email('privada@moreno.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // MORON
        $municipio = $this->em->getRepository('Entity\Municipio')->find(87);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MMoron_Com');
        $email->set_email('lopez.gustavo@moron.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);       
       

        // NAVARRO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(88);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MNavarro_Com');
        $email->set_email('computos@navarro.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MNavarro_SH');
        $email->set_email('alfredocastellari@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // NECOCHEA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(89);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MNecochea_Com');
        $email->set_email('computosmn@necochea.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MNecochea_SH');
        $email->set_email('subcom@necochea.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // OLAVARRIA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(91);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MOlavarria_Com');
        $email->set_email('sistemasycomunicaciones@olavarria.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MOlavarria_SH');
        $email->set_email('secgobierno-olavarria@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PATAGONES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(92);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPatagones_Com');
        $email->set_email('pat1@patagones.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPatagones_SH');
        $email->set_email('secpintenden@patagones.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PEHUAJO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(93);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPehuajo_Com');
        $email->set_email('sispehuajo@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPehuajo_SH');
        $email->set_email('subcom@pehuajo.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PELLEGRINI
        $municipio = $this->em->getRepository('Entity\Municipio')->find(95);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPellegrini_Com');
        $email->set_email('jcsestac@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPellegrini_SH');
        $email->set_email('municipalidadpellegrini@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // PERGAMINO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(95);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPergamino_Com');
        $email->set_email('elmo_gattone@pergamino.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPergamino_SH');
        $email->set_email('privada@pergamino.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PILA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(96);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPila_Com');
        $email->set_email('ofinformatica@pi.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPila_SH');
        $email->set_email('subcom@pi.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PILAR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(97);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPilar_SH');
        $email->set_email('privada@pilar.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PINAMAR
        $municipio = $this->em->getRepository('Entity\Municipio')->find(98);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPinamar_SH');
        $email->set_email('intendente@pinamar.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PRESIDENTE PERON
        $municipio = $this->em->getRepository('Entity\Municipio')->find(99);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPeron_SH');
        $email->set_email('ferchortega@gmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PUAN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(100);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPuan_SH');
        $email->set_email('subcom@puan.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // PUNTA INDIO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(101);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPuntaindio_Com');
        $email->set_email('subcom@pindio.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MPuntaindio_SH');
        $email->set_email('hectorequiza@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // QUILMES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(102);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MQuilmes_Com');
        $email->set_email('edu@quilmes.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MQuilmes_SH');
        $email->set_email('franciscogutierrez@quilmes.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // RAMALLO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(103);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRamallo_Com');
        $email->set_email('griseldachiappari@ramallo.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRamallo_SH');
        $email->set_email('arielsantalla@arnetbiz.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // RAUCH
        $municipio = $this->em->getRepository('Entity\Municipio')->find(104);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRauch_Com');
        $email->set_email('secplani@rauch.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRauch_SH');
        $email->set_email('contador@rauch.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // RIVADAVIA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(105);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRivadavia_Com');
        $email->set_email('subcom@rivadavia.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRivadavia_SH');
        $email->set_email('munirivadavia@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ROJAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(106);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRojas_Com');
        $email->set_email('departamento.informatica@rojas.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRojas_SH');
        $email->set_email('sec.privada@rojas.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // ROQUE PEREZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(107);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRoquePerez_Com');
        $email->set_email('informaticarl@rpereznet.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MRperez_SH');
        $email->set_email('subcom@rperez.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAAVEDRA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(108);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSaavedra_Com');
        $email->set_email('computos@saavedra.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSaavedra_SH');
        $email->set_email('subcom@saavedra.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SALADILLO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(109);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSaladillo_SH');
        $email->set_email('privada@saladillo.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SALLIQUELO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(110);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSalliquelo_SH');
        $email->set_email('subcom@salliquelo.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSalliquelo_Com');
        $email->set_email('sistemas@saliquelo.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SALTO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(111);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSalto_SH');
        $email->set_email('saltomun@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN ANDRES DE GILES
        $municipio = $this->em->getRepository('Entity\Municipio')->find(112);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSadegiles_SH');
        $email->set_email('subcom@sadegiles.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN CAYETANO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(114);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSancayetano_Com');
        $email->set_email('tita@copesa.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN FERNANDO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(115);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanfernando_SH');
        $email->set_email('privada@sanfernando.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // SAN ISIDRO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(116);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanisidro_SH');
        $email->set_email('gposse@sanisidro.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN MIGUEL
        $municipio = $this->em->getRepository('Entity\Municipio')->find(117);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanmiguel_SH');
        $email->set_email('privadasanmiguel@hotmail.com');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSmdelmonte_SH');
        $email->set_email('intenden@monte.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN NICOLAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(118);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('OTRO');
        $email->set_email('computos@sannicolas.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('OTRO');
        $email->set_email('jorgebalcof@sannicolas.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSannicolas_Com');
        $email->set_email('computos@sannicolas.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSannicolas_SH');
        $email->set_email('ceremonial@sannicolas.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN PEDRO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(119);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanpedro_Com');
        $email->set_email('municipalidadsp@arnet.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanpedro_SH');
        $email->set_email('sprivada@sanpedro.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SAN VICENTE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(120);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanVicente_Com');
        $email->set_email('sistemas@msv.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSanvicente_SH');
        $email->set_email('subcom@svicente.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // SUIPACHA
        $municipio = $this->em->getRepository('Entity\Municipio')->find(121);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MSuipacha_SH');
        $email->set_email('atencionrapida@coesanet.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TANDIL
        $municipio = $this->em->getRepository('Entity\Municipio')->find(122);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTandil_Com');
        $email->set_email('computos@tandil.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTandil_SH');
        $email->set_email('secprivada@tandil.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TAPALQUE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(123);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTapalque_Com');
        $email->set_email('contad1@tapalque.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTapalque_SH');
        $email->set_email('intenden@tapalque.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // TIGRE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(124);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTigre_SH');
        $email->set_email('sergiomassa@tigre.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email); 

        // TORDILLO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(125);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTordillo_SH');
        $email->set_email('municipalidadtordillo@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TORNQUIST
        $municipio = $this->em->getRepository('Entity\Municipio')->find(126);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTornquist_Com');
        $email->set_email('contadormunicipal.tornquist@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTornquist_SH');
        $email->set_email('casacampo@tornquist.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TRENQUE LAUQUEN
        $municipio = $this->em->getRepository('Entity\Municipio')->find(127);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTrenqueLauquen_Com');
        $email->set_email('marcelo.german@trenquelauquen.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTlauquen_SH');
        $email->set_email('intenden@tlauquen.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // TRES ARROYOS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(128);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTresArroyos_Com');
        $email->set_email('computos3a@speedy.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTresarroyos_SH');
        $email->set_email('privada@tresa.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TRES DE FEBRERO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(129);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTresDeFebrero_Com');
        $email->set_email('m3fsis@yahoo.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // TRES LOMAS
        $municipio = $this->em->getRepository('Entity\Municipio')->find(130);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTresLomas_Com');
        $email->set_email('jcomputos@treslomas.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MTreslomas_SH');
        $email->set_email('intenden@treslomas.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // VEINTICINCO DE MAYO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(131);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVenticincoDeMayo_Com');
        $email->set_email('sistemas@vm.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVenticincoDeMayo_SH');
        $email->set_email('subcom@vm.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // VICENTE LOPEZ
        $municipio = $this->em->getRepository('Entity\Municipio')->find(132);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVicenteLopez_Com');
        $email->set_email('gaston.cambre@vicentelopez.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVicenteLopez_SH');
        $email->set_email('enrique.garcia@vicentelopez.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // VILLA GESELL
        $municipio = $this->em->getRepository('Entity\Municipio')->find(133);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVillaGesell_Com');
        $email->set_email('computosvg@gesell.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVillaGesell_SH');
        $email->set_email('s.privada@hotmail.com.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        // VILLARINO
        $municipio = $this->em->getRepository('Entity\Municipio')->find(134);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVillarino_SH');
        $email->set_email('intendente@villarino.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);       

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MYrigoyen_SH');
        $email->set_email('subcom@yrigoyen.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MVillarino_Com');
        $email->set_email('informatica@villarino.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        // ZARATE
        $municipio = $this->em->getRepository('Entity\Municipio')->find(135);
        
        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MZarate_SH');
        $email->set_email('secretariaprivada@zarate.mun.gba.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);

        $email = new Entity\NotificacionEmail;
        $email->set_nombre('MZarate_Com');
        $email->set_email('ibaro@zarate.gov.ar');
        $email->set_municipio($municipio);
        $this->em->persist($email);


        $this->em->flush();
        echo "se cargaron ".count($this->em->getRepository('Entity\NotificacionEmail')->findAll())." emails. <br>"; 
    }

}
