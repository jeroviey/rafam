<?php if (!defined('BASEPATH'))
    exit("No direct script access allowed");

class Load extends MX_Controller 
{
	function __construct()
    {
        parent::__construct();
        include('migrate.php');
    	$this->controller = new Migrate;
    }

	public function index()
    {
        $this->controller->load_usuarios();
        $this->controller->load_municipios();
        $this->controller->load_modulos();
        $this->controller->load_profesores();
        $this->controller->load_cursos();
        $this->controller->load_actualizaciones();
        $this->controller->load_temas();
        $this->controller->load_faqs();
        $this->controller->load_tipos_documentacion();
        $this->controller->load_documentacion();
        $this->controller->load_titulos_contenidos();
        $this->controller->load_imagenes();
        // $this->controller->load_notificacion_emails();
    }
    
}