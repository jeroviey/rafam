<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Software extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index()
    {
        redirect('inicio');
    }

    public function descripcion()
    {
        $data['imagen'] = $this->em->getRepository('Entity\Imagen')->findOneBy(array('nombre' => 'descripcion'));
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'software_descripcion'));
        $data['titulo_nucleo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'nucleo'));
        $data['titulo_ingresos_publicos'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'modulo_ingresos_publicos'));
        $data['titulo_personal'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'modulo_personal'));
        $this->load->template_frontend('software/descripcion', $data);           
    }

    public function requerimientos()
    {
        $this->load->template_frontend('software/requerimientos');           
    }

    public function documentacion()
    {   
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'software_documentacion'));
        $tipos = $this->em->getRepository('Entity\TipoDocumentacion')->findAll();
        $tipos_documentaciones = array();
        foreach ($tipos as $tipo) 
        {
            $tipos_documentaciones[$tipo->get_nombre()] =  $this->em->getRepository('Entity\Documentacion')->documentacionDeUnTipo($tipo->get_id());
        }

        $data['tipos_documentaciones'] = $tipos_documentaciones;
        $this->load->template_frontend('software/documentaciones', $data);   
    }

    public function ver_pdf($id_documentacion)
    {
        $documento = $this->em->getRepository('Entity\Documentacion')->find($id_documentacion);
        ver($documento->get_path(), base_url().'software/documentacion');
    }

    public function descargar_pdf($id_documentacion)
    {
        $documento = $this->em->getRepository('Entity\Documentacion')->find($id_documentacion);
        descargar($documento->get_path(), base_url().'software/documentacion');
    }
}