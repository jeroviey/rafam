<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Capacitacion extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index()
    {
        redirect('inicio');
    }

    public function objetivos()
    {
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'capacitacion_objetivos'));
        $this->load->template_frontend('capacitacion/objetivos', $data);
    }

    public function cronograma()
    {
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'capacitacion_cronograma'));
        $data['cursos'] = $this->em->getRepository('Entity\Curso')->findAll();
        $data['cursoclases'] = array();
        foreach($data['cursos'] as $curso)
        {
            $meses = array();
            foreach($curso->get_clases()->toArray() as $clase)
            {
                $meses[$clase->get_mes()] = $clase->get_dias();
            }
            $data['cursoclases'][$curso->get_id()] = $meses;
        }

        $this->load->template_frontend('capacitacion/cronograma', $data);
    }

    public function inscripcion()
    {
        $data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'capacitacion_inscripcion'));
        $data['titulo_contacto'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'capacitacion_inscripcion_contacto'));
        $data['titulo_importante'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'capacitacion_inscripcion_importante'));
    	$data['cursos'] = $this->em->getRepository('Entity\Curso')->findAll();
        $data['municipios'] = $this->em->getRepository('Entity\Municipio')->findAll();
        $data['inscripcion'] = new Entity\Inscripcion;
        $this->cargar_datos_insc($data['inscripcion']);
        $form = $this->input->post('inscripcion', true);
        $data['tipo_trabajo_selected'] =  ($form && array_key_exists('tipo_trabajo', $form)) ? $form['tipo_trabajo'] : 0;
        $data['municipio_selected'] = $form ? $form['municipio'] : '0';
        $data['curso_selected'] = $form ? $form['curso'] : '0';
        $data['clase_selected'] = $form ? $form['clase'] : '0';
        $data['entidad_curso'] = false;
        if($data['curso_selected'] != '0')
        {
            $data['entidad_curso'] = $this->em->getRepository('Entity\Curso')->find($data['curso_selected']);
        }

    	$this->load->template_frontend('capacitacion/inscripcion', $data);
    }

    public function cargar_datos_insc($insc)
    {
        $formulario = $this->input->post('inscripcion', true);
        if($formulario)
        {
            $curso = $this->em->getRepository('Entity\Curso')->find($formulario['curso']);
            if($curso)
            {
                $insc->set_curso($curso);
            }
            $clase = $this->em->getRepository('Entity\Clase')->find($formulario['clase']);
            if($clase)
            {
                $insc->set_clase($clase);
            }
            $insc->set_nombre($formulario['nombre']);
            $insc->set_apellido($formulario['apellido']);
            $insc->set_dni($formulario['dni']);
            $insc->set_email($formulario['email']);
            $insc->set_telefono($formulario['telefono']);
            $municipio = $this->em->getRepository('Entity\Municipio')->find($formulario['municipio']);
            if($municipio)
            {
                $insc->set_municipio($municipio);
            }
            $insc->set_lugar_trabajo($formulario['lugar_trabajo']);
            $insc->set_cargo($formulario['cargo']);
            $insc->set_oficina($formulario['oficina']);
        }
    }

    public function procesar_inscripcion()
    {
        if ($this->form_validation->run($this, 'inscripcion'))
        {
            $inscripcion = new Entity\Inscripcion;
            $this->cargar_datos_insc($inscripcion);
            $this->em->persist($inscripcion);
            $this->em->flush();

            $email = $inscripcion->get_email();
            $subject = 'Capacitación RAFAM – Inscripción a curso';
            $message = $this->load->view('template/capacitacion/registro_inscripcion', array('inscripcion' => $inscripcion), true);
            enviar_email($email,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
            
    		$this->load->template_frontend('capacitacion/inscripcion_confirmacion');
        }
        else
        {
            $this->inscripcion();
        }
    }


    //CALLBACKS
    public function municipio_check($municipio, $form)
    {
    	return municipio_check($municipio, $form, $this);
    }

    public function lugar_trabajo_check($lugar_trabajo, $form)
    {
        return lugar_trabajo_check($lugar_trabajo, $form, $this);
    }
    
    public function dni_check($lugar_trabajo, $form)
    {
        return dni_check($lugar_trabajo, $form, $this);
    }
}