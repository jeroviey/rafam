<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Inicio extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index()
    {
        $datos['noticias'] = array_slice($this->em->getRepository('Entity\Noticia')->findBy(array(),array( 'created_at' => 'DESC' )),0,5);
        $this->load->template_frontend('index',$datos);           
    }

    public function login()
    {
        $is_logged_in = $this->session->userdata('session_usuario');
        if(!isset($is_logged_in) || $is_logged_in['online'] != true)
        {
            $this->load->view('login');
        }
        else
        {
            redirect('backend/inicio');
        }
    }

    public function preguntas_frecuentes()
    {
        $data['temas'] = $this->em->getRepository('Entity\Tema')->findBy(array(), array('orden' => 'ASC'));
        $this->load->template_frontend('preguntas_frecuentes/preguntas_frecuentes', $data);           
    }

}