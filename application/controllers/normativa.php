<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Normativa extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
    
    public function index()
    {   
    	$data['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'normativa'));
        $this->load->template_frontend('normativa/normativa', $data);  
    }
    
}