<?php

$lang['migration_none_found']			= "No se encontraron migraciones.";
$lang['migration_not_found']			= "This migration could not be found.";
$lang['migration_multiple_version']		= "Hay varias migraciones con el mismo número: %d.";
$lang['migration_class_doesnt_exist']	= "La clase migración \"%s\" no pudo ser encontrada.";
$lang['migration_missing_up_method']	= "La clase migración \"%s\" le falta el método 'up'.";
$lang['migration_missing_down_method']	= "La clase migración \"%s\" le falta el método 'down'.";
$lang['migration_invalid_filename']		= "Migración \"%s\" tiene un nombre de archivo inválido.";


/* End of file migration_lang.php */
/* Location: ./system/language/english/migration_lang.php */