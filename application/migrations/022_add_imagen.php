<?php

class Migration_add_imagen extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("nombre varchar(100) NOT NULL");
        $this->dbforge->add_field("imagen varchar(100) NOT NULL");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('imagen');
	
		echo "la tabla IMAGEN fue creada exitosamente<br>";	
    }
 
    public function down()
    {
        
    }
}