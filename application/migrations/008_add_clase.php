<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_clase extends CI_Migration {

    public function up() {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("mes int(11) default NULL");
        $this->dbforge->add_field("dias varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("lugar varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("curso_id int(11) unsigned");
        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('clase');
        echo "La tabla CLASE fue creada exitosamente</br>";
    }

    public function down() {
        
    }

}