<?php

class Migration_add_notificacion_email extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("nombre varchar(100) NOT NULL");
        $this->dbforge->add_field("email varchar(100) NOT NULL");
        $this->dbforge->add_field("municipio_id int(11) unsigned");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('notificacion_email');
	
		echo "la tabla NOTIFICACION EMAIL fue creada exitosamente<br>";	
    }
 
    public function down()
    {
        
    }
}