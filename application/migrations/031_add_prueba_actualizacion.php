<?php

class Migration_add_prueba_actualizacion extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("nombre varchar(100) NOT NULL");
        $this->dbforge->add_field("archivo varchar(300) NOT NULL");
        $this->dbforge->add_field("tipo varchar(100) NOT NULL");
        $this->dbforge->add_field("version varchar(100) NOT NULL");
        $this->dbforge->add_field("modulo_id int(11)");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('prueba_actualizacion');
	
		echo "la tabla PRUEBA_ACTUALIZACION fue creada exitosamente<br>";	
    }
 
    public function down()
    {
        
    }
}