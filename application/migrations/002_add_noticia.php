<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_noticia extends CI_Migration 
{
    public function up() 
    {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("titulo varchar(100) NOT NULL");
        $this->dbforge->add_field("texto text NOT NULL");
        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('noticia');

        echo "La tabla NOTICIA fue creada exitosamente</br>";
    }

    public function down() 
    {
        
    }

}