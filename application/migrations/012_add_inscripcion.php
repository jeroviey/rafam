<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_inscripcion extends CI_Migration {

    public function up() 
    {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("nombre varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("apellido varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("dni int(11) default NULL");
        $this->dbforge->add_field("email varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("cargo varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("oficina varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("lugar_trabajo varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("asistencia int(1) NOT NULL DEFAULT 0");

        $this->dbforge->add_field("curso_id int(11) unsigned");
        $this->dbforge->add_field("clase_id int(11) unsigned");
        $this->dbforge->add_field("municipio_id int(11) unsigned");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('inscripcion');

        echo "La tabla INSCRIPCION fue creada exitosamente</br>";
    }

    public function down() {
        
    }

}