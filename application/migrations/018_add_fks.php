<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_fks extends CI_Migration {

	public function up() {
        
        $this->db->query("ALTER TABLE curso_profesor ADD FOREIGN KEY (curso_id) REFERENCES curso(id)");
        $this->db->query("ALTER TABLE curso_profesor ADD FOREIGN KEY (profesor_id) REFERENCES profesor(id)");

        $this->db->query("ALTER TABLE inscripcion ADD FOREIGN KEY (curso_id) REFERENCES curso(id)");
        $this->db->query("ALTER TABLE inscripcion ADD FOREIGN KEY (clase_id) REFERENCES clase(id)");
        $this->db->query("ALTER TABLE inscripcion ADD FOREIGN KEY (municipio_id) REFERENCES municipio(id)");

		$this->db->query("ALTER TABLE clase ADD FOREIGN KEY (curso_id) REFERENCES curso(id)");
		
		$this->db->query("ALTER TABLE pregunta ADD FOREIGN KEY (tema_id) REFERENCES tema(id)");

        $this->db->query("ALTER TABLE contacto ADD FOREIGN KEY (municipio_id) REFERENCES municipio(id)");

        $this->db->query("ALTER TABLE actualizacion_documento ADD FOREIGN KEY (actualizacion_id) REFERENCES actualizacion(id)");
		$this->db->query("ALTER TABLE actualizacion_documento ADD FOREIGN KEY (documento_id) REFERENCES documento(id)");
		
		$this->db->query("ALTER TABLE documentacion ADD FOREIGN KEY (tipo_documentacion_id) REFERENCES tipo_documentacion(id)");

	echo "Las CLAVES FORANEAS fueron agregadas exitosame </br>";
	
	}
	public function down() {
	}
}