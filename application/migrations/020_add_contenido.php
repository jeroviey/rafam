<?php

class Migration_add_contenido extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("orden int(11)");
        $this->dbforge->add_field("titulo varchar(100) NOT NULL");
        $this->dbforge->add_field("descripcion text NOT NULL");
        $this->dbforge->add_field("titulo_id int(11) unsigned");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('contenido');
	
		echo "la tabla CONTENIDO fue creada exitosamente<br>";	
    }
 
    public function down()
    {
        
    }
}