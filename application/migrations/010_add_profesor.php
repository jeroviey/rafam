<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_profesor extends CI_Migration {

    public function up() {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("nombre varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("apellido varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('profesor');
        echo "La tabla PROFESOR fue creada exitosamente</br>";
    }

    public function down() {
        
    }

}