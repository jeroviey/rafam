<?php

class Migration_add_documentacion extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("nombre varchar(100)");
        $this->dbforge->add_field("path varchar(100)");
        $this->dbforge->add_field("fecha_subida date");
		$this->dbforge->add_field("tipo_documentacion_id int(11)");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('documentacion');
	
		echo "la tabla DOCUMENTACION fue creada exitosamente<br>";	
    }
 
    public function down(){
    }
}