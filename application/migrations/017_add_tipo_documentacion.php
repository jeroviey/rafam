<?php

class Migration_add_tipo_documentacion extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) AUTO_INCREMENT NOT NULL");
        $this->dbforge->add_field("nombre varchar(100)");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tipo_documentacion');
	
		echo "la tabla TIPO_DOCUMENTACION fue creada exitosamente<br>";	
    }
 
    public function down(){
    }
}