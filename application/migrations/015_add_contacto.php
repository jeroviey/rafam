<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_contacto extends CI_Migration {

    public function up() {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("nombre varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("apellido varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("email varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("cargo varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("oficina varchar(50) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("lugar_trabajo varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("asunto varchar(100) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("mensaje varchar(1000) NOT NULL DEFAULT ''");
        $this->dbforge->add_field("solucionado int(1) NOT NULL DEFAULT 0");
        $this->dbforge->add_field("leido int(1) NOT NULL DEFAULT 0");
        $this->dbforge->add_field("fecha_solucionado date");

        $this->dbforge->add_field("municipio_id int(11) unsigned");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('contacto');
        echo "La tabla CONTACTO fue creada exitosamente</br>";
    }

    public function down() {
        
    }

}