<?php

class Migration_add_actualizacion_documento extends CI_Migration 
{
    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
		$this->dbforge->add_field("documento_id int(11) unsigned");
		$this->dbforge->add_field("actualizacion_id int(11) unsigned");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('actualizacion_documento');
	
		echo "la tabla ACTUALIZACION_DOCUMENTO fue creada exitosamente<br>";
    }
 
    public function down()
    {
    }
}