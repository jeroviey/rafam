<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_user extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field("id int(11) unsigned NOT NULL AUTO_INCREMENT");
        $this->dbforge->add_field("username varchar(300) NOT NULL");
        $this->dbforge->add_field("password varchar(300) NOT NULL");
        $this->dbforge->add_field("estado int(1) NOT NULL DEFAULT '0'");
        $this->dbforge->add_field("nombre varchar(100) NOT NULL");
        $this->dbforge->add_field("apellido varchar(100) NOT NULL");
        $this->dbforge->add_field("email varchar(100) NOT NULL");
        $this->dbforge->add_field("password_generada int(1) NOT NULL DEFAULT '0'");

        $this->dbforge->add_field("created_at datetime NOT NULL");
        $this->dbforge->add_field("updated_at datetime NOT NULL");
        
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('usuario');

        echo "la tabla USUARIO fue creada exitosamente <br>";
    }
 
    public function down()
    {

    }
}