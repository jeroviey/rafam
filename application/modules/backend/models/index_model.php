<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Index_model extends CI_Model
{
    
    public function __construct()
    {
        
        parent::__construct();
        
    }
    
    public function get_users()
    {
        
        $query = $this->db->get('users');
        
        return $query->result();
        
    }
    
}
/*
*end modules/login/models/index_model.php
*/