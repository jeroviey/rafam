<script src="<?=base_url();?>assets/js/clases.js"></script>
<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open($accion);?>

		<div class="form-group">
			<label>Nombre</label>
			<input 
				type="text" 
				name="profesor[nombre]" 
				class="form-control" 
				id="nombre"
				placeholder="Ingrese el nombre del profesor"
				value="<?php echo set_value('nombre', isset($profesor) ? $profesor->get_nombre() : '' ); ?>"
			>
			<?php echo form_error('profesor[nombre]',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<div class="form-group">
			<label>Apellido</label>
			<input 
				type="text" 
				name="profesor[apellido]" 
				class="form-control" 
				id="apellido"
				placeholder="Ingrese el apellido del profesor"
				value="<?php echo set_value('apellido', isset($profesor) ? $profesor->get_apellido() : '' ); ?>"
			>
			<?php echo form_error('profesor[apellido]',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<div class="form-group">
			<label>Email</label>
			<input 
				type="text" 
				name="profesor[email]" 
				class="form-control" 
				id="email"
				placeholder="Ingrese el email del profesor"
				value="<?php echo set_value('email', isset($profesor) ? $profesor->get_email() : '' ); ?>"
			>
			<?php echo form_error('profesor[email]',"<p class='bg-danger'>","</p>"); ?>
		</div>

	    <div class="pull-right">
		    <a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>

	<?php echo form_close();?>	       	
</div>