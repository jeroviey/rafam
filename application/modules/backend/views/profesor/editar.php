<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/profesor/editar_guardar/'.$profesor->get_id(),
			                   'titulo' => 'Editar profesor',
			                   'submit'=> 'Actualizar profesor',
			                   'volver' => base_url('/backend/profesor'),
			                   'profesor' => $profesor);

			    $this->load->view('profesor/form', $datos); 
			?>
		</div>
    </div>
</section>
