<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/profesor/nuevo_guardar',
                               'titulo' => 'Nuevo profesor',
                               'submit'=> 'Guardar profesor',
                               'volver' => base_url('/backend/profesor'),
                               'profesor' => $profesor);
                $this->load->view('profesor/form', $datos); 
            ?>
        </div>
    </div>
</section>
