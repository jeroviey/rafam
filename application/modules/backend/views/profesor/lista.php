<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de profesores</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Profesor </th>
                        <th class="campos" > Email </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($profesores)) { ?>
                    
                    	<?php foreach ($profesores as $profesor){?>
                            <tr>
                                <td> <?= $profesor ?> </td>
                                <td><h5><?= $profesor->get_email() ?></h5> </td>
                                <td class="centrado"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/profesor/editar/<?= $profesor->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/profesor/borrar/<?= $profesor->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea eliminar al profesor '<?= $profesor ?>'?"><span data-toggle="tooltip" data-placement="top" title="Eliminar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/profesor/nuevo/"><h5 class="derecha">NUEVO PROFESOR</h5></a>

            <?php if (empty($profesores)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ningun profesor en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>