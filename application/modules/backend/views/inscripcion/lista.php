<section>
    <div class="container">
        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de inscriptos</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <?php $this->load->view('backend/inscripcion/filtros') ?>

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="tabla table table-hover table-condensed">

                        <thead>
                            <tr>
                                <th class="campos" > Nombre y apellido</th>
                                <th class="campos" > Lugar de trabajo</th>
                                <th class="campos" > Curso </th>
                                <th class="campos" > Clase </th>
                                <th class="campos centrado" > Estado </th>
                                <th class="campos centrado" > Asistió </th>
                                <th class="campos centrado" > Acciones </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if (!empty($inscripciones)) { ?>
                            
                            	<?php foreach ($inscripciones as $inscripcion){?>
                                    <tr data-toggle="tooltip" data-placement="top" title="<?= date_format($inscripcion->get_created_at(), 'd/m/Y-H:i:s')?>">
                                        <td> <?= $inscripcion->get_nombre().' '.$inscripcion->get_apellido() ?> </td>
                                        <td> <?php echo $inscripcion->get_municipio() ? 'Municipio de '.$inscripcion->get_municipio()->get_nombre() : $inscripcion->get_lugar_trabajo(); ?> </td>
                                        <td> <?= $inscripcion->get_curso()->get_nombre() ?> </td>
                                        <td> <?= $inscripcion->get_clase() ?> </td>
                                        
                                        <?php switch ($inscripcion->get_aceptada()) {
                                            case '0':
                                        ?>
                                            <td class="centrado">
                                                <span data-toggle="tooltip" data-placement="top" title="Pendiente" class="glyphicon glyphicon-alert"></span>
                                            </td>
                                            <td class="centrado">
                                                -
                                            </td>
                                        <?php
                                                break;
                                            case '1':
                                        ?>
                                            <td class="centrado">
                                                <span data-toggle="tooltip" data-placement="top" title="Aceptado" class="glyphicon glyphicon-ok"></span>
                                            </td>
                                            <td class="centrado">
                                                <?php if ($inscripcion->get_asistencia()){ ?>
                                                    <a data-href="<?=base_url()?>backend/inscripcion/asistencia/<?= $inscripcion->get_id() ?>/ausente" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea confirmar la ausencia de <?= $inscripcion->get_nombre().' '.$inscripcion->get_apellido() ?> al curso <?=$inscripcion->get_curso()->get_nombre()?>?"><span data-toggle="tooltip" data-placement="top" title="Registrar ausencia" class="glyphicon glyphicon-check gris_BA"></a>
                                                <?php }else{?>
                                                    <a data-href="<?=base_url()?>backend/inscripcion/asistencia/<?= $inscripcion->get_id() ?>/presente" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea confirmar la presencia de <?= $inscripcion->get_nombre().' '.$inscripcion->get_apellido() ?> al curso <?=$inscripcion->get_curso()->get_nombre()?>?"><span data-toggle="tooltip" data-placement="top" title="Registrar presente" class="glyphicon glyphicon-unchecked gris_BA"></a>
                                                <?php } ?>
                                            </td>
                                        <?php
                                                break;
                                            case '2':
                                        ?>
                                            <td class="centrado">
                                                <span data-toggle="tooltip" data-placement="top" title="Rechazado" class="glyphicon glyphicon-remove"></span>
                                            </td>
                                            <td class="centrado">
                                                <span data-toggle="tooltip" data-placement="top" title="Rechazado" class="glyphicon glyphicon-remove"></span>
                                            </td>
                                        <?php
                                                break;
                                        }?>
                                        <td class="centrado"> 
                                            <a data-toggle="tooltip" data-placement="top" title="Enviar notificación" href="<?=base_url()?>backend/inscripcion/notificacion/<?= $inscripcion->get_id() ?>"><span class="glyphicon glyphicon-envelope gris_BA"></a>
                                            <a data-toggle="tooltip" data-placement="top" title="Mas datos" href="<?=base_url()?>backend/inscripcion/mostrar/<?= $inscripcion->get_id() ?>"><span class="glyphicon glyphicon-plus gris_BA"></a>
                                            <a data-toggle="tooltip" data-placement="top" title="Editar datos inscripción" href="<?=base_url()?>backend/inscripcion/editar/<?= $inscripcion->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                        </td>
                                    </tr> 

                                <?php } ?>
                            <?php }else{ ?>
                                <tr>
                                    <td rowspan="8"> </td>
                                </tr>
                            <?php } ?>
                        </tbody>

                    </table>
                </div>
            </div>

            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <?php if (empty($inscripciones)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ninguna inscripcion en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>
    </div>
</section>