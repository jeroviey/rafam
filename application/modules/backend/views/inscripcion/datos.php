<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Informacion de inscripción</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<p><strong>Nombre y Apellido:</strong> <?= $inscripcion ?></p>
				<p><strong>DNI:</strong> <?= $inscripcion->get_dni() ?></p>
				<p><strong>Email:</strong> <?= $inscripcion->get_email() ?></p>
				<p><strong>Teléfono:</strong> <?= $inscripcion->get_telefono() ?></p>
				<p><strong>Lugar de trabajo:</strong> <?php echo $inscripcion->get_municipio() ? 'Municipio de '.$inscripcion->get_municipio()->get_nombre() : $inscripcion->get_lugar_trabajo(); ?></p>
				<p><strong>Oficina:</strong> <?= $inscripcion->get_oficina() ?></p>
				<p><strong>Cargo:</strong> <?= $inscripcion->get_cargo() ?></p>
				<p><strong>Curso y clase:</strong> <?php echo $inscripcion->get_curso()->get_nombre().'  -  '.$inscripcion->get_clase(); ?></p>
				<p><strong>¿Aceptado?</strong> <?php echo ($inscripcion->get_aceptada() == 1) ? 'Si' : 'No'; ?></p>
				<p><strong>¿Asistió?</strong> <?php echo $inscripcion->get_asistencia() ? 'Si' : 'No'; ?></p>
			</div>

			<a class="boton pull-right" href="<?=base_url()?>backend/inscripcion/generar_pdf/<?=$inscripcion->get_id()?>"><h5 class="derecha">GENERAR CERTIFICADO DE ASISTENCIA</h5></a>

		</div>
    </div>
</section>