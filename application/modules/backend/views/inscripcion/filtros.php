  <p>Filtrar por</p>
	<?=form_open('backend/inscripcion/inscripciones/1', array('method' => 'get','class' => "form", "role"=>"form"))?>
		
		<div class="form-group col-md-2">
        <input type="text" class="form-control" name="inscripcion" id="inscripcion" placeholder="Nombre ó Apellido" value="<?php if(isset($busqueda['inscripcion'])){ echo $busqueda['inscripcion']; } ?>">
    </div>

		<div class="form-group col-md-2">
			<?= form_dropdown('id_municipio', $municipios, isset($busqueda['id_municipio']) ? $busqueda['id_municipio'] : '',"class=form-control") ?>
		</div>

		<div class="form-group col-md-2">
			<select name="curso_id" id="curso" class="form-control">
					<option value="0" data-infoclases="0=Seleccione primero un curso" >Curso</option>
				<?php foreach($cursos as $curso): ?>
					<option value="<?= $curso->get_id() ?>" data-infoclases="<?= $curso->get_info_clases(); ?>" <?php echo isset($busqueda['curso']) ? (($busqueda['curso'] == $curso->get_id()) ? 'selected' : '') : '' ?> > <?= $curso->get_nombre() ?> </option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group col-md-2">

          	<?php $clases= array('0' => "Seleccione primero un curso",);?>
          	<?php echo form_dropdown('clase', 
                                  $clases, 
                                  null,
                                  "class=form-control id=clase data-selected=".(isset($busqueda['clase']) ? $busqueda['clase'] : "0")) 
          	?>
	    </div>

		<div class="form-group col-md-2">
          	<?php $estados= array('' => "Estado (todos)",
                                '0' => "Pendientes",
                                '1' => "Aceptados",
                                '2' => "Rechazados");?>
          	<?php echo form_dropdown('estado', 
                                  $estados, 
                                  (isset($busqueda['estado']) && array_key_exists($busqueda['estado'],$estados)) ? $busqueda['estado'] : '',
                                  "class=form-control") 
          	?>
	    </div>

	    <div class="form-group col-md-2">
          	<?php $asistencias= array('' => "Asistencia (todos)",
                                '0' => "No asistió",
                                '1' => "Asistió");?>
          	<?php echo form_dropdown('asistencia', 
                                  $asistencias, 
                                  (isset($busqueda['asistencia']) && array_key_exists($busqueda['asistencia'],$asistencias)) ? $busqueda['asistencia'] : '',
                                  "class=form-control") 
          	?>
	    </div>

		<div class="form-group col-md-12">
        <button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-search"></span> Filtrar</button>  
    </div>
    <?php if(isset($busqueda)): ?>
    <div class="form-group col-md-12">
      <button class="btn btn-default btn-block" type="submit" name="clear"><span class="glyphicon glyphicon-remove"></span> Limpiar filtros</button>  
    </div>
    <?php endif ?>

	<?=form_close()?>

<script src="<?=base_url();?>assets/js/inscripcion.js"></script>