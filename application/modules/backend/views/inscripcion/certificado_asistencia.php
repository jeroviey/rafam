<!DOCTYPE html>
<html lang="ES">
<head profile="http://www.w3.org/2005/10/profile">
    <?php
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="<?=base_url('assets/imagenes/loguito_rafam_azul_25px.png')?>" />
    <title>RAFAM - Reforma de la Administración Financiera</title>
    <!-- BOOTSTRAP -->
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">

    <!-- JS -->
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.js"></script>


    <!-- E S T I L O S (CSS) -->
    <link href="<?=base_url();?>assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets/css/fuentes.css" rel="stylesheet" type="text/css">

</head>

<style text="text/css">
    p.taman_medio{
        font-size:25px;
    }

    p.taman_chico{
        font-size:15px;
    }

    p.sangria{
        text-indent: 40px;
        margin-top: 30px;
    }

    .justificado{
    text-align : justify;
    }
</style>

<body>

    <div class="container">

        <div class="container">

            <div class="col-xs-6 " id="logo_rafam_menu">
                <img class="img-thumbnail" src="<?=base_url()?>assets/imagenes/cerfitifado_rafam.png">
            </div>

            <div class="col-sm-6 col-xs-6 pull-right">
                <img class="img-thumbnail" src="<?=base_url()?>assets/imagenes/cerfitifado_BA.png">
            </div>
        </div>

        <div class="row col-xs-10 col-xs-offset-1">
            <br>
            <br>
            <p class="pull-right taman_medio"> La Plata, <?= $dia ?> de <?=$inscripcion->get_clase()->get_mes_texto() ?> de <?=$inscripcion->get_clase()->get_ano()?> </p>

            <br>
            <br>
            <br>
            <br>
            <p class="col-xs-12 justificado sangria taman_medio">
                Certifico que <?=$inscripcion?> DNI <?=$inscripcion->get_dni()?>  <?php echo $inscripcion->get_municipio() ? 'de la municipalidad de '.$inscripcion->get_municipio()->get_nombre() : 'en '.$inscripcion->get_lugar_trabajo(); ?> ha realizado los días <?=$inscripcion->get_clase()->get_dias().' de '.$inscripcion->get_clase()->get_mes_texto().' de '.$inscripcion->get_clase()->get_ano() ;?> la capacitación de “<?=$inscripcion->get_curso()->get_nombre()?>” correspondiente al Sistema Integrado de Reforma de la Administración Financiera en el Ámbito Municipal (RAFAM), dictado por personal de este Ministerio.
            </p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <p class="col-xs-12 justificado sangria taman_medio">
                Se expide el presente en la ciudad de La Plata, a los <?= $dia ?> días de <?=$inscripcion->get_clase()->get_mes_texto() ?> del <?=$inscripcion->get_clase()->get_ano()?>
            </p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <p class="text-center taman_chico">Ministerio de Economía | Subsecretaría de Coordinación Económica</p>
                    <p class="text-center taman_chico">Dirección Provincial de Coordinación Municipal | Dirección de Sistemas de Información Financiera Municipal</p>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>