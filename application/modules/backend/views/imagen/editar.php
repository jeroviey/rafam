<section class="section_azul">
	<div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3> Edición imagen "<?=$imagen?>" </h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
			<?php echo form_open_multipart(base_url('backend/imagen/actualizar/'.$imagen->get_id())); ?>
				<div class="form-group">
					<label form="imagen">Imagen actual:</label>
		    		<div class="col-md-4">
						<img src="<?= $imagen->get_ubicacion() ?>" class="img-thumbnail">
					</div>
				</div>

				<div class="form-group">
					<label form="imagen">Nueva imagen:</label>
					<input type="file" id="imagen" name="imagen" size="1" />
				</div>

				<div class="pull-right">
				    <a href="<?= base_url() ?>" class="btn btn-default">Cancelar</a>
				    <button type="submit" class="btn btn-primary"> Guardar </button>
				</div>
			<?php echo form_close();?>
			</div>
		</div>
	</div>
</section>