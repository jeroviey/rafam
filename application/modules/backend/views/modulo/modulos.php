<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">


            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de modulos</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th class="campos" > Nombre </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($modulos)) { ?>
                    
                        <?php foreach ($modulos as $modulo){?>
                            <tr>
                                <td> <?= $modulo->get_nombre();?> </td>
                                <td class="centrado"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/modulo/editar/<?= $modulo->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/modulo/borrar/<?= $modulo->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el módulo seleccionado?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>
            
            <a class="boton pull-right" href="<?=base_url()?>backend/modulo/nuevo/"><h5 class="derecha">NUEVO</h5></a>
            
            <?php if (empty($modulos)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ningun modulo en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>