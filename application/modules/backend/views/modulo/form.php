<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open($accion);?>

		<div class="form-group">
			<label>Nombre</label>
			<input 
				type="text" 
				name="nombre" 
				class="form-control" 
				id="nombre"
				placeholder="Ingrese el nombre del nuevo módulo"
				value="<?php echo set_value('nombre', isset($modulo) ? $modulo->get_nombre() : '' ); ?>"
			>
			<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>
		</div>
			
	    <div class="pull-right">
		    <a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>

	<?php echo form_close();?>	       	
</div>