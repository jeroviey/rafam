<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/modulo/nuevo_guardar',
                               'titulo' => 'Nuevo modulo',
                               'submit'=> 'Guardar modulo',
                               'volver' => base_url('/backend/modulo'));
                $this->load->view('modulo/form', $datos); 
            ?>
        </div>
    </div>
</section>
