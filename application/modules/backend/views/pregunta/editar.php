<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/pregunta/editar_guardar/'.$pregunta->get_id(),
			                   'titulo' => 'Editar pregunta',
			                   'submit'=> 'Actualizar pregunta',
			                   'volver' => base_url('/backend/pregunta'),
			                   'pregunta' => $pregunta,
			                   'temas' => $temas);

			    $this->load->view('pregunta/form', $datos); 
			?>
		</div>
    </div>
</section>
