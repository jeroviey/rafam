<script src="<?=base_url();?>assets/js/clases.js"></script>
<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open($accion);?>

		<div class="form-group">
			<label>Tema</label>
			<select name="pregunta[tema]" id="tema" class="form-control">
				<?php foreach($temas as $tema): ?>
					<?php
						if($pregunta->get_tema()){
							$selected = ($tema->get_id() == $pregunta->get_tema()->get_id()) ? 'selected' : '';
						}
						else{
							$selected = '';
						}
					?>
					<option value="<?= $tema->get_id() ?>" <?php echo $selected ?> > <?= $tema->get_nombre() ?> </option>
				<?php endforeach ?>
			</select>
			<?php echo form_error('pregunta[tema]',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Pregunta</label>
			<textarea placeholder="Ingrese la pregunta" name="pregunta[pregunta]" id="pregunta"><?php echo $pregunta->get_pregunta() ?></textarea>
			<?php echo form_error('pregunta[pregunta]',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Respuesta</label>
			<textarea placeholder="Ingrese la respuesta" name="pregunta[respuesta]" id="respuesta"><?php echo $pregunta->get_respuesta() ?></textarea>
			<?php echo form_error('pregunta[respuesta]',"<p class='bg-danger'>","</p>"); ?>
		</div>

	    <div class="pull-right">
		    <a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>

	<?php echo form_close();?>	       	
</div>