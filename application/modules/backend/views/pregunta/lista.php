<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de preguntas frecuentes</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Pregunta </th>
                        <th class="campos" > Tema </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($preguntas)) { ?>
                    
                    	<?php foreach ($preguntas as $pregunta){?>
                            <tr>
                                <td> <?= $pregunta->get_pregunta() ?> </td>
                                <td><h5><?= $pregunta->get_tema() ?></h5> </td>
                                <td class="centrado"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/pregunta/editar/<?= $pregunta->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/pregunta/borrar/<?= $pregunta->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la pregunta '<?= $pregunta->get_pregunta();?>'?"><span data-toggle="tooltip" data-placement="top" title="Eliminar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/pregunta/nuevo/"><h5 class="derecha">NUEVA PREGUNTA</h5></a>
            <a class="boton pull-right" href="<?=base_url()?>backend/tema/"><h5 class="derecha">EDITAR TEMAS</h5></a>

            <?php if (empty($preguntas)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ninguna pregunta en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>