<section>
	<div class="container">
		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<table class="tabla table-responsive table table-hover table-condensed">
				<thead>
			        <tr>
						<th>Nombre y apellido</th>
						<th>Email</th>
						<th>Nombre de usuario</th>
						<th class="text-center">¿Habilidado?</th>
						<th class="text-center">Acciones</th>
			        </tr>
			    </thead>
			    <tbody>
			    <?php foreach($usuarios as $usuario): ?>
			        <tr>
						<td><?php echo $usuario ?></td>
						<td><?php echo $usuario->get_email() ?></td>
						<td><?php echo $usuario->get_username() ?></td>
						<td class="text-center">
						<?php if ($usuario->get_estado() == true) { ?>
							<a 	title="Desactivar" 
								data-href="<?= base_url('backend/usuario/desactivar/'.$usuario->get_id()) ?>"
								data-toggle="modal"
								data-boton="Aceptar"
								data-target="#confirm-modal"
								href="#"
								data-confirm="¿Está seguro que desea DESACTIVAR del usuario '<?=$usuario->get_username() ?>'' ?"
							> 
								<?php echo render_boolean($usuario->get_estado()) ?> 
							</a>
						<?php }else{ ?>
							<a 	title="Activar"
								data-href="<?= base_url('backend/usuario/activar/'.$usuario->get_id()) ?>"
								data-toggle="modal"
								data-boton="Aceptar"
								data-target="#confirm-modal"
								href="#"
								data-confirm="¿Está seguro que desea ACTIVAR del usuario '<?=$usuario->get_username() ?>'' ?"
							> 
								<?php echo render_boolean($usuario->get_estado()) ?> 
							</a>
						<?php } ?>
						</td>
						<td class="text-center">
							<a 	data-href="<?=base_url('backend/usuario/restablecer/'.$usuario->get_id())?>" 
								data-toggle="modal"
								data-boton="Aceptar"
								data-target="#confirm-modal" 
								href="#"
								data-confirm="¿Está seguro que desea restablecer la contraseña del usuario '<?=$usuario->get_username() ?>'' ?"
							> 
								<span data-toggle="tooltip" data-placement="top" title="Restablecer contraseña" class="glyphicon glyphicon-refresh gris_BA"> 
							</a>

							<a href="<?=base_url('backend/usuario/editar/'.$usuario->get_id())?>"><span data-toggle="tooltip" data-placement="top" title="Editar" class="glyphicon glyphicon-pencil gris_BA"></span></a>

							<a href="<?=base_url('backend/usuario/ver/'.$usuario->get_id())?>"><span data-toggle="tooltip" data-placement="top" title="Ver perfil" class="glyphicon glyphicon-user gris_BA"></span></a>
						</td>
			        </tr>
			    <?php endforeach ?>    
			    </tbody>
			</table>
		</div>
	</div>
</section>