<section>
	<div class="container">
		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/usuario/actualizar/'.$usuario->get_id(),
			                   'submit'=> 'Actualizar usuario');
			    $this->load->view('usuario/form', $datos); 
			?>
		</div>
	</div>
</section>