<?php echo form_open($accion);?>

    <div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" class="form-control" value="<?php echo set_value('nombre', isset($usuario) ? $usuario->get_nombre() : '' ); ?>">
        <?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>
    </div>

    <div class="form-group">
        <label for="apellido">Apellido</label>
        <input type="text" id="apellido" name="apellido" class="form-control" value="<?php echo set_value('apellido', isset($usuario) ? $usuario->get_apellido() : '' ); ?>">
        <?php echo form_error('apellido',"<p class='bg-danger'>","</p>"); ?>
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" id="email" name="email" class="form-control" value="<?php echo set_value('email', isset($usuario) ? $usuario->get_email() : '' ); ?>">
        <?php echo form_error('email',"<p class='bg-danger'>","</p>"); ?>
    </div>

    <?php if($usuario->newRecord()): ?>
    <div class="form-group">
        <label for="username">Nombre de usuario</label>
        <input type="text" id="username" name="username" class="form-control">
        <?php echo form_error('username',"<p class='bg-danger'>","</p>"); ?>
    </div>
    <?php endif ?>

    <div class="pull-right">
        <a href="<?php echo base_url('/backend/usuario') ?>" class="btn btn-default">Cancelar</a>
        <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
    </div>

<?php echo form_close();?>