<section>
	<div class="container">
		<div class="hoja_blanca col-md-4 col-md-offset-4">
			<h2 class="text-center">Perfil del usuario</h2>
			<br>
			<p> <strong>Nombre de usuario:</strong> <?= $usuario->get_username() ?> </p>
			<p> <strong>Nombre:</strong> <?= $usuario->get_nombre() ?> </p>
			<p> <strong>Apellido:</strong> <?= $usuario->get_apellido() ?> </p>
			<p> <strong>Correo electrónico:</strong> <?= $usuario->get_email() ?> </p>
			<p> <strong>Fecha de creación:</strong> <?= date_format($usuario->get_created_at(), 'd-m-Y') ?> </p>
			<p> <strong>Fecha ultima modificación:</strong> <?= date_format($usuario->get_updated_at(), 'd-m-Y') ?> </p>
		</div>
	</div>
</section>