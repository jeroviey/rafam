<!-- BOOTSTRAP FILE INPUT -->
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>

<section class="section_azul">
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Nueva documentación</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<?php echo form_open_multipart('backend/documentacion/nuevo_guardar/');?>

					<div class="form-group">
						<label for="tipo_documentacion_id">Tipo</label>
						<?= form_dropdown('tipo_documentacion_id', $tipos_documentaciones, '','id="tipo_documentacion_id" class="form-control"');?>
						<?php echo form_error('tipo_documentacion_id',"<p class='bg-danger'>","</p>"); ?>
					</div>

					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input 
							type="text" 
							name="nombre" 
							class="form-control" 
							id="nombre"
							placeholder="Ingrese el nombre de la nueva documentación"
							value="<?php echo set_value('nombre', isset($documentacion) ? $documentacion->get_nombre() : '' ); ?>"
						>
						<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>

					</div>

					<div class="form-group">
						<label>Archivo PDF</label>
			            <input name="archivo" id="file-1" class="file" type="file" data-show-upload="false" data-show-remove="false">
					</div>
				
				    <div class="pull-right">
					    <a href="<?= base_url('/backend/documentacion') ?>" class="btn btn-default">Cancelar</a>
					    <button type="submit" class="btn btn-primary"> Guardar </button>
					</div>

				<?php echo form_close();?>	       	
			</div>

        </div>
    </div>
</section>

<script src="<?=base_url();?>assets/js/fileinput_pdf.js" type="text/javascript"></script>