<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de documentaciones</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Tipo </th>
                        <th class="campos" > Nombre </th>
                        <th class="campos" > Archivo </th>
                        <th class="campos" > Fecha Subida </th>
                        <th class="campos" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($documentaciones)) { ?>
                    
                    	<?php foreach ($documentaciones as $documentacion){?>
                            <tr>


                                <td> <?=$documentacion->get_tipo()->get_nombre() ?> </td>
                                <td> <?=$documentacion->get_nombre() ?> </td>
                                <td class="centrado"> <a href="<?= base_url()?>backend/documentacion/descargar_pdf/<?=$documentacion->get_id()?>" data-toggle="tooltip" data-placement="top" title="<?= $documentacion->get_nombre();?>"> <span class="glyphicon glyphicon-file azul_rafam"></span> </a> </td>
                                <td> <?= date_format($documentacion->get_fecha_subida(), 'd/m/Y') ?> </td>

                                <td class="centrado"> 
                                    <a data-href="<?=base_url()?>backend/documentacion/borrar/<?= $documentacion->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la documentación seleccionada?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/documentacion/nuevo/"><h5 class="derecha">NUEVO</h5></a>
            <a class="boton pull-right" href="<?=base_url()?>backend/tipo_documentacion"><h5 class="derecha">TIPOS DOCUMENTACIÓN</h5></a>

            

            <?php if (empty($documentaciones)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ninguna documentación en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>