<!-- BOOTSTRAP FILE INPUT -->
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>

<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?= $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open_multipart($accion);?>

		<div class="form-group">
			<label>Nombre</label>
			<input 
				type="text" 
				name="nombre" 
				class="form-control" 
				id="nombre"
				placeholder="Ingrese el nombre del nuevo archivo"
				value="<?php echo set_value('nombre', isset($archivo) ? $archivo->get_nombre() : '' ); ?>"
			>
			<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Archivo ZIP / RAR</label>
            <input name="archivo" id="file-1" class="file" type="file" data-show-upload="false" data-show-remove="false" data-show-preview="false">
		</div>
	
	    <div class="pull-right">
		    <a href="<?= $cancelar ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" id="mostrarBarra" data-titulo="Subiendo archivo" class="btn btn-primary"> <?=$submit?> </button>
		</div>

	<?php echo form_close();?>	       	
</div>

<?= $this->load->view('plantillas/modal_barra_progreso'); ?>

<script src="<?=base_url();?>assets/js/fileinput_zip.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/barra_progreso.js" type="text/javascript"></script>