<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/archivo_sistema/nuevo_guardar/',
                               'titulo' => 'Nueva archivo del sistema',
                               'submit'=> 'Guardar',
                               'volver' => $volver,
                               'cancelar' => base_url('backend/archivo_sistema/'),
                               'archivo' => $archivo
                );

                $this->load->view('backend/archivo_sistema/form', $datos); 
            ?>
        </div>
    </div>
</section>
