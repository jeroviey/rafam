<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de archivos del sistema</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Nombre </th>
                        <th class="campos centrado" > Archivo </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($archivos)) { ?>
                    
                    	<?php foreach ($archivos as $archivo){?>
                            <tr>
                                <td> <?= $archivo->get_nombre();?> </td>
                                <td class="centrado"> <a data-link="<?=base_url()?>archivo_sistema/descargar/<?=$archivo->get_id()?>" data-toggle="modal" data-target="#copy-modal" href="#"> <span class="glyphicon glyphicon-download azul_rafam" data-toggle="tooltip" data-placement="top" title="Copiar Link a portapapeles"></span> </a> </td>
                                <td class="centrado"> <a data-href="<?=base_url()?>backend/archivo_sistema/borrar/<?= $archivo->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el archivo del sistema '<?= $archivo->get_nombre() ?>' ?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a></td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/archivo_sistema/nuevo/"><h5 class="derecha">NUEVO</h5></a>

            <?php if (empty($archivos)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ninguna archivo en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>