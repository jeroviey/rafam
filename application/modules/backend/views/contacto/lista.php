<script src="<?=base_url();?>assets/js/mensajes.js"></script>

<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <?php $this->load->view('backend/contacto/filtros') ?>

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Mensajes mesa de ayuda</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

                <div id="scroller" class="noticia col-md-4">
                    <ul class="nav nav-pills nav-stacked">
                        <?php if (!empty($contactos)) { ?>
                            <?php $i = 1?>
                            <?php foreach ($contactos as $contacto){?>
                                <li class="<?=$i?> " role="presentation">
                                    <a class="mostrar_mensaje gris_BA" data-identificador="<?=$i?>" data-url="<?=base_url().'backend/contacto/mensaje_json/'.$contacto->get_id()?>">
                                        <p class="asunto"><?= $contacto->get_asunto() ?></p>
                                        <p class="apellido"><?=$contacto->get_apellido().' '.$contacto->get_nombre()?></p>
                                        <p class="email"><?= $contacto->get_email() ?></p>
                                    </a>
                                </li>
                                <?php $i++?>
                            <?php } ?>

                        <?php }else{ ?>
                            <li>NO EXISTEN MENSAJES</li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-md-7 col-md-offset-1">
                    <div style="display:none" id="dvloader">
                        <img src="<?=base_url('assets/images/loading.gif')?>" />
                    </div>

                    <div id="scroller" class="panel_json noticia">

                    </div>
                </div>
        </div>

    </div>

</section>

<script type="text/html" id="message-view">
    <div>
        <div>
            <div id="de"><strong> De: </strong></div>
            <div id="lugar_trabajo"><strong> Lugar de trabajo: </strong></div>
            <div id="oficina" class="col-xs-6"><strong> Oficina:</strong></div>
            <div id="cargo" class="col-xs-6"><strong>Cargo:</strong></div>
        </div>

        <div id="asunto" class="asunto separar_top5"></div>
        <div id="cuerpo" class="separar_top5"></div>

        <hr>

        <div id="no_solucionado">
            <a data-url="<?=base_url()?>backend/contacto/solucionado/" data-toggle="modal" data-target="#confirm-modal" data-boton="Solucionar" href="#" data-confirm="¿Está seguro que desea marcar como solucionado este mensaje?" id="link_solucionado"><span data-toggle="tooltip" data-placement="top" title="Solucionar" class="boton pull-right"><h5 class="derecha">Solucionar</h5></a>
        </div>
        <div id="solucionado" class="pull-right" >
            <span class="glyphicon glyphicon-ok"></span> Solucionado
        </div>
    </div>
</script>