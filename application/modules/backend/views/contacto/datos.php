<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Informacion del mensaje</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
					<div class="form-group">
						<label>Nombre y apellido</label>
						<input 
							type="text" 
							name="nombre" 
							class="form-control" 
							id="nombre"
							value="<?php echo $contacto->get_nombre().' '.$contacto->get_apellido(); ?>"
							disabled="true"
						>
					</div>
					<div class="form-group">
						<label>e-mail</label>
						<input 
							type="text" 
							name="email" 
							class="form-control" 
							id="email"
							value="<?php echo $contacto->get_email(); ?>"
							disabled="true"
						>
					</div>
					<div class="form-group">
						<label>Lugar de trabajo</label>
						<input 
							type="text" 
							name="email" 
							class="form-control" 
							id="email"
							value="<?php echo $contacto->get_municipio() ? 'Municipio de '.$contacto->get_municipio()->get_nombre() : $contacto->get_lugar_trabajo(); ?>"
							disabled="true"
						>
					</div>
					<div class="form-group">
						<label>Oficina</label>
						<input 
							type="text" 
							name="oficina" 
							class="form-control" 
							id="oficina"
							value="<?php echo $contacto->get_oficina(); ?>"
							disabled="true"
						>
					</div>
					<div class="form-group">
						<label>Cargo</label>
						<input 
							type="text" 
							name="cargo" 
							class="form-control" 
							id="cargo"
							value="<?php echo $contacto->get_cargo(); ?>"
							disabled="true"
						>
					</div>

					<div class="form-group">
						<label>Asunto</label>
						<input 
							type="text" 
							name="asunto" 
							class="form-control" 
							id="asunto"
							value="<?php echo $contacto->get_asunto(); ?>"
							disabled="true"
						>
					</div>

					<div class="form-group">
						<label>Mensaje</label>
						<textarea name="mensaje" id="mensaje" disabled="true"><?php echo $contacto->get_mensaje() ?></textarea>
					</div>

					<div class="form-group">
						<label>¿Solucionado?</label>
						<input 
							type="text" 
							name="asistencia" 
							class="form-control" 
							id="asistencia"
							value="<?php echo $contacto->get_solucionado() ? 'Si' : 'No'; ?>"
							disabled="true"
						>
					</div>

				    <div class="pull-right">
					    <a href="<?php echo base_url('/backend/contacto') ?>" class="btn btn-primary">Volver</a>
					</div>

			</div>
		</div>
    </div>
</section>