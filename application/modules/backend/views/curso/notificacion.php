<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Enviar notificación</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-envelope HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<p><strong>Curso:</strong> <?= $curso->get_nombre() ?> </p>
				
				<p><strong>Modulo:</strong> <?= $curso->get_modulo() ?> </p>

				<p><strong>Profesores:</strong></p>
				<?php foreach ($curso->get_profesores() as $profesor) { ?>
					<p><?=  $profesor.' ('.$profesor->get_email().')' ?></p>
				<?php } ?>
				
				<p><strong>Clases:</strong></p>
				<?php foreach ($clases as $clase) { ?>
				<div class="col-md-12">
					<div class="col-md-12">
						<?=  $clase['clase'] ?> <strong>(Cantidad de inscriptos: <?= $clase['inscriptos'] ?>)</strong>
					</div>
					<br>
					<br>
					<div class="col-md-12">
						<a data-href="<?=base_url('backend/curso/enviar_notificacion_reprogramacion/'.$curso->get_id().'/'.$clase['clase']->get_id())?>" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea enviar la notificación, a los profesores e inscriptos, de que el curso '<?= $curso->get_nombre();?>' fue actualizado?" class="btn btn-default">Reprogramación curso</a>
						<a data-href="<?=base_url('backend/curso/enviar_notificacion_cambio_aula/'.$curso->get_id().'/'.$clase['clase']->get_id())?>" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea enviar la notificación, a los profesores e inscriptos, de que el curso '<?= $curso->get_nombre();?>' fue actualizado?" class="btn btn-default">Cambio de aula curso</a>
						<?php if($clase['clase']->get_estado() != 1){ ?>
							<a data-href="<?=base_url('backend/curso/enviar_notificacion/'.$curso->get_id().'/'.$clase['clase']->get_id().'/1')?>" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea enviar la notificación, a los profesores e inscriptos, de que el curso '<?= $curso->get_nombre();?>' se dictará con práctica?" class="btn btn-default">Se dicta con practica</a>
						<?php } ?>
						<?php if($clase['clase']->get_estado() != 2){ ?>
							<a data-href="<?=base_url('backend/curso/enviar_notificacion/'.$curso->get_id().'/'.$clase['clase']->get_id().'/2')?>" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea enviar la notificación, a los profesores e inscriptos, de que el curso '<?= $curso->get_nombre();?>' se dictará sin práctica?" class="btn btn-default">Se dicta sin practica</a>
						<?php } ?>
						<?php if($clase['clase']->get_estado() != 3){ ?>
							<a data-href="<?=base_url('backend/curso/enviar_notificacion/'.$curso->get_id().'/'.$clase['clase']->get_id().'/3')?>" data-toggle="modal" data-target="#confirm-modal" data-boton="Confirmar" href="#" data-confirm="¿Está seguro que desea enviar la notificación, a los profesores e inscriptos, de que el curso '<?= $curso->get_nombre();?>' no se dictará?" class="btn btn-default">No se dicta</a>
						<?php } ?>
					</div>
				<br>
				<br>
				<br>
				<br>
				<?php } ?>
			</div>

		</div>
    </div>
</section>