<script src="<?=base_url();?>assets/js/clases.js"></script>
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>
<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php $meses =  array(4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre');?>
	<?php echo form_open_multipart($accion);?>
		<div class="form-group">
			<label>Nombre</label>
			<input 
				type="text" 
				name="nombre" 
				class="form-control" 
				id="nombre"
				placeholder="Ingrese el nombre del nuevo curso"
				value="<?php echo set_value('nombre', isset($curso) ? $curso->get_nombre() : '' ); ?>"/>
			<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<div class="form-group">
			<label>Modulo</label>
			<select name="modulo" id="modulo" class="form-control">
				<?php foreach($modulos as $modulo): ?>
					<?php
						if($curso->get_modulo()){
							$selected = ($modulo->get_id() == $curso->get_modulo()->get_id()) ? 'selected' : '';
						}
						else{
							$selected = '';
						}
					?>
					<option value="<?= $modulo->get_id() ?>" <?php echo $selected ?> > <?= $modulo->get_nombre() ?> </option>
				<?php endforeach ?>
			</select>
			<?php echo form_error('modulo',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<?php $this->load->view('curso/form_archivo', ['curso' => $curso]); ?>

		<?php $this->load->view('curso/form_clases', ['meses' => $meses, 'clases' => $clases]); ?>

		<?php $this->load->view('curso/form_profesores', ['profesores' => $profesores, 'profesores_actuales' => $profesores_actuales]); ?>

		<br>
		<br>
		<br>
	    <div class="form-group col-xs-12">
	    	<div class="pull-right">
		    	<a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    	<button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
			</div>
		</div>

	<?php echo form_close();?>	       	
</div>