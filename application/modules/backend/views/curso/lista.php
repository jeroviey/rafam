<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de cursos</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Nombre </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($cursos)) { ?>
                    
                    	<?php foreach ($cursos as $curso){?>
                            <tr>
                                <td class="col-xs-10"> <?= $curso->get_nombre();?> </td>
                                <td class="centrado col-xs-2"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Enviar notificación" href="<?=base_url()?>backend/curso/notificacion/<?= $curso->get_id() ?>"><span class="glyphicon glyphicon-envelope gris_BA"></a>
                                    <a data-toggle="tooltip" data-placement="top" title="Mas datos" href="<?=base_url()?>backend/curso/mostrar/<?= $curso->get_id() ?>"><span class="glyphicon glyphicon-plus gris_BA"></a>
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/curso/editar/<?= $curso->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/curso/borrar/<?= $curso->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el curso '<?= $curso->get_nombre();?>'?"><span data-toggle="tooltip" data-placement="top" title="Eliminar curso" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>
            
            <a class="boton pull-right" href="<?=base_url()?>backend/curso/nuevo/"><h5 class="derecha">NUEVO</h5></a>

            <?php if (empty($cursos)) { ?>
                <div class="text-center">
                    <h3><p style="color: black">No existe ningun curso en el sistema</p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>