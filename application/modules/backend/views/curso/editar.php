<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/curso/editar_guardar/'.$curso->get_id(),
			                   'titulo' => 'Editar curso',
			                   'submit'=> 'Actualizar curso',
			                   'volver' => base_url('/backend/curso'),
			                   'curso' => $curso,
			                   'profesores' => $profesores,
			                   'modulos' => $modulos,
			                   'profesores_actuales' => $profesores_actuales,
			                   'clases' => $clases);

			    $this->load->view('curso/form', $datos); 
			?>
		</div>
    </div>
</section>
