<!DOCTYPE html>
<html lang="ES">
<head profile="http://www.w3.org/2005/10/profile">
    <?php
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="<?=base_url('assets/imagenes/loguito_rafam_azul_25px.png')?>" />
    <title>RAFAM - Reforma de la Administración Financiera</title>
    <!-- BOOTSTRAP -->
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">

    <!-- JS -->
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.js"></script>


    <!-- E S T I L O S (CSS) -->
    <link href="<?=base_url();?>assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets/css/fuentes.css" rel="stylesheet" type="text/css">
</head>

<style type="text/css">

    thead {display: table-row-group}
    tr { page-break-inside: avoid }
    
</style>

<body>

    <div class="container">

        <div class="row">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3> ASISTENCIA PARTICIPANTES </h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <h4>Actividad:  CURSO <strong><?= $curso->get_nombre()?></strong> DE LA REFORMA ADMINISTRATIVA FINANCIERA EN EL AMBITO MUNICIPAL</h4>

            <table style="page-break-after: always;" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="campos" > Nombre y Apellido </th>
                        <th class="campos" > DNI </th>
                        <th class="campos" > Email </th>
                        <th class="campos" > Teléfono </th>
                        <th class="campos" > Lugar de trabajo </th>
                        <?php foreach ($clases as $dia){?>
                        <th class="campos" > <?= $dia.'/'.$clase->get_mes().'/'.$clase->get_ano() ?> </th>
                        <?php } ?>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($inscriptos)) { ?>
                    
                        <?php foreach ($inscriptos as $inscripto){?>
                            <tr>
                                <td> <?= $inscripto;?> </td>
                                <td> <?= $inscripto->get_dni();?> </td>
                                <td> <?= $inscripto->get_email();?> </td>
                                <?php if($inscripto->get_telefono() != 0){ ?>
                                    <td> <?= $inscripto->get_telefono();?> </td>
                                <?php }else{ ?>
                                    <td>  </td>
                                <?php } ?>
                                <td> <?= $inscripto->get_municipio() ? 'Municipio de '.$inscripto->get_municipio()->get_nombre() : $inscripto->get_lugar_trabajo(); ?> </td>
                                <?php foreach ($clases as $dia){?>
                                    <td>  </td>
                                <?php } ?>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            <?php if (empty($inscriptos)) { ?>
                <div class="text-center">
                    <h3><p style="color: black">No existe ningun inscripto al curso en el sistema</p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</body>
</html>