<label>Profesor/es</label>
<?php echo form_error('profesor',"<p class='bg-danger'>","</p>"); ?>

		<div class="form-group">
			 <table class="table" >
                <tbody id="profesores">
                	<tr id="template_profesor" style="display:none">
                		<td class="campos" >
                			<select name="profesor[prof]" id="profesor_prof_" class="form-control">
									<option value="0" > Seleccione un profesor </option>
								<?php foreach($profesores as $profesor): ?>
									<option value="<?= $profesor->get_id() ?>" > <?= $profesor ?> </option>
								<?php endforeach ?>
							</select>
                		</td>
                        <td class="campos" >
                        	<a class="delete_profesor pull-right" data-callback="curso_borrar_profesor" data-callbackparam="" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar al profesor seleccionado?"><span class="glyphicon glyphicon-remove azul_rafam"></a>
                        </td>
                    </tr>
                    <?php
		                $index = 1;
					    foreach($profesores_actuales as $profesor_actual){
					    	?>
		                    <tr id="tr-profesor-<?= $index ?>" >
		                		<td class="campos" >
		                			<select name="profesor[prof][<?= $index ?>]" id="profesor_prof_<?= $index ?>" class="form-control">
										<?php foreach($profesores as $profesor): ?>
											<option value="<?= $profesor->get_id() ?>" <?php echo ($profesor_actual->get_id() == $profesor->get_id()) ? 'selected' : '' ?> > <?= $profesor ?> </option>
										<?php endforeach ?>
									</select>
		                		</td>
		                        <td class="campos" >
		                        	<a class="delete_profesor pull-right" data-callback="curso_borrar_profesor" data-callbackparam="<?= $index ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar al profesor seleccionado?"><span class="glyphicon glyphicon-remove azul_rafam"></a>
		                        </td>
		                    </tr>
					    	<?php
					    	$index++;
					   	}
					?>
                </tbody>
              </table>
              <button id="agregar_profesor" class="btn-censo-add btn btn-primary col-md-4 pull-right" type="button">Agregar Profesor</button>
              <input 
			    	type="hidden" 
			    	id="profesor_index" 
			    	name="profesor_index" 
			    	value="<?= $index ?>" 
		    	/>
		</div>