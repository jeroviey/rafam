<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Informacion del curso</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<p><strong>Curso:</strong> <?= $curso->get_nombre() ?> </p>
				
				<p><strong>Modulo:</strong> <?= $curso->get_modulo() ?> </p>

				<p><strong>Profesores:</strong></p>
				<?php foreach ($curso->get_profesores() as $profesor) { ?>
					<p><?=  $profesor.' ('.$profesor->get_email().')' ?></p>
				<?php } ?>
				
				<p><strong>Clases:</strong></p>
				<div class="col-md-10 col-md-offset-1">
					<table class="tabla table-responsive table table-hover table-condensed">
		                <thead>
		                    <tr>
		                        <th class="campos" > Fecha y Lugar </th>
		                        <th class="campos centrado" > Cantidad de Inscriptos </th>
		                        <th class="campos centrado" > Estado </th>
		                        <th class="campos centrado" > Acciones </th>
		                    </tr>
		                </thead>

		                <tbody>
							<?php foreach ($clases as $clase) { ?>
                            <tr>
                                <td> <?=  $clase['clase'] ?>  </td>
                                <td class="centrado"> <?= $clase['inscriptos'] ?> </td>
                                <td class="centrado"> <?= $clase['clase']->get_estado_string() ?> </td>
                                <td class="centrado"> 
                                	<a data-toggle="tooltip" data-placement="top" title="Generar planilla de asistencia" href="<?=base_url()?>backend/curso/generar_plantilla/<?= $curso->get_id() ?>/<?= $clase['clase']->get_id() ?>"><span class="glyphicon glyphicon-paste gris_BA"></span></a>
                                </td>
                            </tr> 
							<?php } ?>
		                </tbody>
		            </table>
				</div>
			</div>
		</div>
    </div>
</section>