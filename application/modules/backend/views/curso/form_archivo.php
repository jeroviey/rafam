<div class="form-group">
    <label>Contenido</label>

    <div class="form-group">
    <?php if(!$curso->get_archivo_path()){ ?>
        <h4>NO EXISTE ARCHIVO RELACIONADO AL CURSO</h4>
    <?php } ?>
    </div>

    <?php echo form_error('archivo_id',"<p class='bg-danger'>","</p>"); ?>
    <div class="form-group">
        <input 
            class="col-xs-11"
            type="text" 
            id="archivo_nombre" 
            name="archivo_nombre"
            placeholder="Título" 
            value="<?= $curso->get_archivo_nombre() ?>" 
        >
        <?php if($curso->get_archivo_path()){ ?>
            <div class="col-xs-1">
                <center>
                <a href="<?= base_url()?>backend/curso/descargar_pdf/<?=$curso->get_archivo_id()?>" data-toggle="tooltip" data-placement="top" title="Descargar Archivo"> <span class="glyphicon glyphicon-download-alt azul_rafam"></span> </a>
                </center>
            </div>
        <?php } ?>
        <input
            type="hidden"
            id="archivo_id"
            name="archivo_id"
            value="<?= $curso->get_archivo_id() ?>"
        >
    </div>

    <label></label>
    <div class="form-group" id="show_archivo" <?= $curso->get_archivo_path() ? 'style="display:block"' : 'style="display:none"' ?>>
        <div class="col-xs-12" >
            <input class="col-xs-11" placeholder="<?= $curso->get_archivo_filename() ?>" readonly="readonly">
            <div class="col-xs-1">
                <center>
                <span id="actualizar_archivo" class="glyphicon glyphicon-refresh azul_rafam" data-toggle="tooltip" data-placement="top" title="Actualizar Archivo">
                </center>
            </div>
        </div>
    </div>
    <div class="form-group" id="carga_archivo" <?= $curso->get_archivo_path() ? 'style="display:none"' : 'style="display:block"' ?>>
        <div class="col-xs-12" >
            <div class="col-xs-11">
                <input class="col-xs-12 file" name="archivo_curso" id="archivo_curso" type="file" data-show-upload="false" data-show-remove="false" data-show-preview="false" >
            </div>
            <?php if($curso->get_archivo_id() != 0): ?>
                <div class="col-xs-1">
                    <center>
                    <span id="cancelar_archivo" class="glyphicon glyphicon-trash azul_rafam" data-toggle="tooltip" data-placement="top" title="Cancelar">
                    </center>
                </div>
            <?php endif ?>
        </div>
    </div>
    
    <div class="clearfix"></div>
</div>    