	<div class="form-group">
		<label>Clases</label>
		<?php echo form_error('clase',"<p class='bg-danger'>","</p>"); ?>
	</div>
	<div class="form-group">
		 <table class="table" >
	        <thead>
	            <tr>
	                <th class="campos" > Año </th>
	                <th class="campos" > Mes </th>
	                <th class="campos" > Dias </th>
	                <th class="campos" > Lugar </th>
	                <th class="campos" > &nbsp; </th>
	            </tr>
	        </thead>
	        <tbody id="clases">
	        	<input id="validar_borrado_clase" type="hidden" value="<?= base_url('backend/curso/validar_clase/')?>" >
	        	<tr id="template_clase" style="display:none">
	        		<td class="campos" >
	        			<select name="clase[anio]" id="clase_anio_" class="form-control">
							<?php foreach(lista_anios_clases() as $k => $v): ?>
								<option value="<?= $k ?>" > <?= $v ?> </option>
							<?php endforeach ?>
						</select>
	        		</td>
	        		<td class="campos" >
	        			<select name="clase[mes]" id="clase_mes_" class="form-control">
							<?php foreach($meses as $k => $v): ?>
								<option value="<?= $k ?>" > <?= $v ?> </option>
							<?php endforeach ?>
						</select>
						<input 
					    	type="hidden" 
					    	id="clase_id_" 
					    	name="clase[id]" 
					    	value="0" />
	        		</td>
	                <td class="campos" >
	                	<input 
							type="text" 
							name="clase[dias]" 
							id="clase_dias_"
							class="form-control" 
							placeholder="Ingrese los dias del curso"
							value=""
						>
	                </td>
	                <td class="campos" >
	                	<input 
							type="text" 
							name="clase[lugar]" 
							id="clase_lugar_"
							class="form-control" 
							placeholder="Ingrese el lugar del curso"
							value=""
						>
	                </td>
	                <td class="campos" >
	                	<!--<a class="delete_clase pull-right" data-callback="curso_borrar_clase" data-callbackparam="" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la clase seleccionada?"><span class="glyphicon glyphicon-remove azul_rafam"></a>-->
	                	<a class="delete_clase pull-right" data-claseid="0" href="#" data-confirm="¿Está seguro que desea borrar la clase seleccionada?"><span class="glyphicon glyphicon-remove azul_rafam"></a>
	                </td>
	            </tr>
	            <?php
	                $index = 1;
				    foreach($clases as $clase){
				    	?>
	                    <tr id="tr-clase-<?= $index ?>">
	                		<td class="campos" >
	                			<select name="clase[anio][<?= $index ?>]" id="clase_anio_<?= $index ?>" class="form-control">
									<?php foreach(lista_anios_clases() as $k => $v): ?>
										<option value="<?= $k ?>" <?= ($k == $clase->get_ano()) ? 'selected' : '' ?> > <?= $v ?> </option>
									<?php endforeach ?>
								</select>
	                		</td>
	                		<td class="campos" >
	                			<select name="clase[mes][<?= $index ?>]" id="clase_mes_<?= $index ?>" class="form-control">
									<?php foreach($meses as $k => $v): ?>
										<option value="<?= $k ?>" <?php echo ($k == $clase->get_mes()) ? 'selected' : '' ?>> <?= $v ?> </option>
									<?php endforeach ?>
								</select>
								<input 
							    	type="hidden" 
							    	id="clase_id_<?= $index ?>" 
							    	name="clase[id][<?= $index ?>]" 
							    	value="<?= $clase->get_id() ?>" />
	                		</td>
	                        <td class="campos" >
	                        	<input 
									type="text" 
									name="clase[dias][<?= $index ?>]" 
									id="clase_dias_<?= $index ?>"
									class="form-control" 
									placeholder="Ingrese los dias del curso"
									value="<?= $clase->get_dias() ?>"
								>
	                        </td>
	                        <td class="campos" >
	                        	<input 
									type="text" 
									name="clase[lugar][<?= $index ?>]" 
									id="clase_lugar_<?= $index ?>"
									class="form-control" 
									placeholder="Ingrese el lugar del curso"
									value="<?= $clase->get_lugar() ?>"
								>
	                        </td>
	                        <td class="campos" >
						    	<!--<a class="delete_clase pull-right" data-callback="curso_borrar_clase" data-callbackparam="<?= $index ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la clase seleccionada?"><span class="glyphicon glyphicon-remove azul_rafam"></a>-->
						    	<a class="delete_clase pull-right" data-claseid="<?= $clase->get_id() ?>" data-claseindex="<?= $index ?>" href="#" data-confirm="¿Está seguro que desea borrar la clase seleccionada?"><span class="glyphicon glyphicon-remove azul_rafam"></a>
	                        </td>
	                    </tr>
				    	<?php
				    	$index++;
				   	}
				?>
	        </tbody>
	      </table>
	      <button id="agregar_clase" class="btn-censo-add btn btn-primary col-md-4 pull-right" type="button">Agregar Clase</button>
	      <input 
	    	type="hidden" 
	    	id="clase_index" 
	    	name="clase_index" 
	    	value="<?= $index ?>" />
	</div>