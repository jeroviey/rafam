<!-- BOOTSTRAP FILE INPUT -->
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>

<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Editando actualización</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<?php echo form_open_multipart('backend/actualizacion/editar_guardar/'.$actualizacion->get_id() )?>

					<div class="form-group">
						<labelfor="modulo_id">Módulo</label>
						<input 
							type="text" 
							name="modulo_id" 
							class="form-control" 
							id="modulo_id"
							readonly="readonly"
							value="<?= $actualizacion->get_modulo()->get_nombre() ?>"
						>
					</div>

					<div class="form-group">
						<label for="tipo">Tipo</label>
						<input 
							type="text" 
							name="tipo" 
							class="form-control" 
							id="tipo"
							readonly="readonly"
							value="<?= $actualizacion->get_tipo() ?>"
						>
					</div>

					<div class="form-group">
						<label>Versión</label>
						<input 
							type="text" 
							name="version" 
							class="form-control" 
							id="version"
							readonly="readonly"
							value="<?= $actualizacion->get_version()?>"
						>
					</div>

					<div class="form-group">
						<label>Archivo ZIP</label>
						<input 
							type="text" 
							class="form-control"
							readonly="readonly"
							value="<?= $actualizacion->get_nombre_archivo()?>"
						>
					</div>

					<div class="form-group">
						<label class="panel-title">Documentos</label>
						<?php foreach ($actualizacion->get_documentos() as $documento) { ?>
						<input type="text" class="form-control" readonly="readonly" value="<?= $documento->get_nombre()?>">
						<?php } ?>
					</div>

					<div>
						<?php if(isset($error)){ ?>
							<p class='bg-danger'><?php echo $error ?></p>
						<?php } ?>
					</div>

					<div class="form-group">
						<label>Nuevos documentos</label>
			            <input name="documentos[]" id="file-1" class="file" type="file" multiple data-show-upload="false" data-show-remove="false">
					</div>

					<div class="form-group">
						<label class="panel-title">Documentos ya cargados en el sistema:</label>
						<div class="checkbox">
						    <label class="checkbox-inline">
						    	<?php $index = 1;?>
						    	<?php foreach ($documentos as $documento) { ?>
						      		<input id="tipo_trabajo_1" name="doc_existentes[<?=$index?>]" value="<?=$documento->get_id()?>" type="checkbox"> <?= $documento->get_nombre() ?> <br>
						    		<?php $index++;?>

						    	<?php }?>
						    </label>
						  </div>
					</div>

						
				    <div class="pull-right">
					    <a href="<?=base_url('backend/actualizacion')?>" class="btn btn-default">Cancelar</a>
					    <button type="submit" class="btn btn-primary">Guardar actualización</button>
					</div>

				<?php echo form_close();?>	       	
			</div>
		</div>
    </div>
</section>

<script src="<?=base_url();?>assets/js/fileinput_pdf.js" type="text/javascript"></script>