<!-- BOOTSTRAP FILE INPUT -->
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>

<section class="section_azul">
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Nueva actualización - Paso 1 de 2</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<?php echo form_open_multipart('backend/actualizacion/nuevo_paso_1/');?>

					<div class="form-group">
						<label for="modulo_id">Módulo</label>
						<?= form_dropdown('modulo_id', $modulos, '','id="modulo_id" class="form-control" data-ajaxurl="'.base_url().'backend/actualizacion/get_documentos"');	?>
						<?php echo form_error('modulo_id',"<p class='bg-danger'>","</p>"); ?>
					</div>

					<div class="form-group">
						<label for="tipo">Tipo</label>
						<div>
						    <select name="tipo" class="tipo form-control">
						    	<option value="<?= isset($actualizacion) ? $actualizacion->get_tipo() : ''?>"> <?= isset($actualizacion) ? $actualizacion->get_tipo() : 'Seleccione una opción'?> </option>
						    	<option value="VERSION"> VERSION </option>
						    	<option value="RELEASE"> RELEASE </option>
						    </select>
						    <?php echo form_error('tipo',"<p class='bg-danger'>","</p>"); ?>
						</div>
					</div>

					<div class="form-group">
						<label>Versión</label>
						<input 
							type="text" 
							name="version" 
							class="form-control" 
							id="version"
							placeholder="Ingrese la versión de la nueva actualización"
							value="<?php echo set_value('version', isset($actualizacion) ? $actualizacion->get_version() : '' ); ?>"
						>
						<?php echo form_error('version',"<p class='bg-danger'>","</p>"); ?>
					</div>

					<div class="form-group">
						<label>Archivo ZIP / RAR</label>
			            <input name="archivo" id="file-1" class="file" type="file" data-show-upload="false" data-show-remove="false" data-show-preview="false">
					</div>
				
				    <div class="pull-right">
					    <a href="<?= base_url('/backend/actualizacion') ?>" class="btn btn-default">Cancelar</a>
					    <button type="submit" class="btn btn-primary"> Siguiente </button>
					</div>

				<?php echo form_close();?>	       	
			</div>

        </div>
    </div>
</section>

<script src="<?=base_url();?>assets/js/fileinput_zip.js" type="text/javascript"></script>