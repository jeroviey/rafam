<!-- BOOTSTRAP FILE INPUT -->
<link href="<?=base_url();?>assets/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>assets/fileinput/js/fileinput.js" type="text/javascript"></script>

<section class="section_azul">
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Nueva actualización - Paso 2 de 2</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">

				<div>
					<?php if(isset($error)){ ?>
						<p class='bg-danger'><?php echo $error ?></p>
					<?php } ?>
				</div>
				<div class="well">
				<p> A la nueva actualización "<?= $actualizacion->get_modulo()->get_nombre().'_'.$actualizacion->get_tipo().'_'.$actualizacion->get_version()?>" se le debe asignar su correspondiente documentación. </p>
				<p>La misma puede ser: </p>
				<p>1. Cargada a traves del sistema. </p>
				<p>2. Seleccionada de los documentos ya existentes en el sistema. </p>
				<p>3. Ambas, cargada y seleccionada del sistema. </p>
				<p>Una actualización puede tener asignado más de un documento. </p>
				</div>
				<?php echo form_open_multipart('backend/actualizacion/nuevo_finalizar/'.$actualizacion->get_id());?>

					<div class="form-group">
						<label>Nuevos documentos</label>
			            <input name="documentos[]" id="file-1" class="file" type="file" multiple data-show-upload="false" data-show-remove="false">
					</div>

					<div class="form-group">
						<label class="panel-title">Documentos ya cargados en el sistema:</label>
						<div class="checkbox">
						    <label class="checkbox-inline">
						    	<?php $index = 1;?>
						    	<?php foreach ($documentos as $documento) { ?>
						      		<input id="tipo_trabajo_1" name="doc_existentes[<?=$index?>]" value="<?=$documento->get_id()?>" type="checkbox"> <?= $documento->get_nombre() ?> <br>
						    		<?php $index++;?>

						    	<?php }?>
						    </label>
						  </div>
					</div>	

				    <div class="pull-right">
					    <a href="<?=base_url()?>backend/actualizacion/borrar_cancelar/<?=$actualizacion->get_id()?>" class="btn btn-default">Cancelar</a>
					    <button type="submit" class="btn btn-primary"> Guardar actualización </button>
					</div>
				<?php echo form_close();?>	       	
			</div>

        </div>
    </div>
</section>

<script src="<?=base_url();?>assets/js/fileinput_pdf.js" type="text/javascript"></script>