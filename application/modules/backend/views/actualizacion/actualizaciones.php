<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de actualizaciones</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Modulo </th>
                        <th class="campos" > Tipo </th>
                        <th class="campos" > Versión </th>
                        <th class="campos" > Fecha Subida </th>
                        <th class="campos" > Archivo </th>
                        <th class="campos" > Documentación </th>
                        <th class="campos" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($actualizaciones)) { ?>
                    
                    	<?php foreach ($actualizaciones as $actualizacion){?>
                            <tr>
                                <td> <?= $actualizacion->get_modulo()->get_nombre();?> </td>
                                <td> <?= $actualizacion->get_tipo();?> </td>
                                <td> <?= $actualizacion->get_version();?> </td>
                                <td> <?= date_format($actualizacion->get_fecha_subida(), 'd/m/Y') ?> </td>
                                <td class="centrado"> <a href="<?=base_url()?>backend/actualizacion/descargar_zip/<?=$actualizacion->get_id()?>" data-toggle="tooltip" data-placement="top" title="Instalador <?=$actualizacion->get_modulo()->get_nombre()."_".$actualizacion->get_tipo()."_".$actualizacion->get_version() ?>"> <span class="glyphicon glyphicon-download azul_rafam"></span> </a> </td>
                                <td class="centrado">
                                    <?php foreach ($actualizacion->get_documentos() as $documento) { ?>
                                        <a href="<?= base_url()?>backend/actualizacion/ver_pdf/<?=$documento->get_id()?>" target="_blank" data-toggle="tooltip" data-placement="top" title="<?= $documento->get_nombre();?>"> <span class="glyphicon glyphicon-file azul_rafam"></span> </a> 
                                    <?php } ?> 
                                </td>
                                <td class="centrado"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/actualizacion/editar/<?= $actualizacion->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/actualizacion/borrar/<?= $actualizacion->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la actualización seleccionada?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>

            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/actualizacion/nuevo/"><h5 class="derecha">NUEVO</h5></a>
            <a class="boton pull-right" href="<?=base_url()?>backend/email_notificacion"><h5 class="derecha">NOTIFICADOS</h5></a>

            <?php if (empty($actualizacion)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ninguna actualización en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>