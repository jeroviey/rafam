<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/contenido/actualizar/'.$contenido->get_id(),
			                   'titulo_form' => 'Nuevo contenido para el titulo '.$titulo->get_nombre(),
			                   'submit'=> 'Actualizar',
			                   'volver' => base_url('/backend/titulo/listar/'.$titulo->get_id()),
			                   'contenido' => $contenido);

			    $this->load->view('contenido/form', $datos);
			?>
		</div>
    </div>
</section>