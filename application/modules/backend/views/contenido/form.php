<script src="<?=base_url();?>assets/js/tinymce/tinymce.min.js"></script>
<script src="<?=base_url();?>assets/js/edicion_texto.js"></script>

<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo_form ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open($accion);?>

		<div class="form-group">
			<label>Título</label>
			<input 
				type="text" 
				name="titulo" 
				class="form-control" 
				id="titulo"
				placeholder="Ingrese el titulo del contenido"
				value="<?php echo $contenido->get_titulo() ; ?>"
			>
			<?php echo form_error('titulo',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Descripción</label>
			<textarea name="descripcion">
				<?php echo set_value('descripcion', isset($contenido) ? $contenido->get_descripcion() : ''); ?>
			</textarea>
			<?php echo form_error('descripcion',"<p class='bg-danger'>","</p>"); ?>
		</div>

	    <div class="pull-right">
		    <a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>

	<?php echo form_close();?>	       	
</div>