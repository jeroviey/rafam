<div class="modal fade" id="copy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h3>Link de descarga - Archivo del sistema</h3>
            </div>

            <div class="modal-body">
                <div class="row">
                    <input type="text" value="" id="copy_input" class="col-xs-12">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>