<?php $userdata = $this->session->userdata('session_usuario') ?>
<div class="menu_azul navbar-fixed-top">
	<div class="container ">

		<div class="col-xs-4">
			<div class="row">
				<span>
				<a data-toggle="tooltip" data-placement="bottom" title="MENU" href="<?=base_url()?>backend/inicio"><img id="rafam_25px" src="<?=base_url()?>assets/imagenes/loguito_rafam_25px.png"></a>
				<a data-toggle="tooltip" data-placement="bottom" title="PORTADA" href="<?=base_url()?>"><img id="rafam_25px" src="<?=base_url()?>assets/imagenes/loguito_frontend_25px.png"></a>
				</span>
				<!-- VOLVER -->
				<?php if ( isset($volver) ){ ?>
					<a title="volver" data-toggle="tooltip" data-placement="bottom" href="<?=base_url($volver)?>"><span class="glyphicon glyphicon-circle-arrow-left" id="volver_backend"></span></a>
				<?php } ?>
			</div>
		</div>

		<div class="col-xs-8">
			<div class="row">
				<div class="pull-right">
					<a id="touch-menu" class="mobile-menu" href="#"><span class="glyphicon glyphicon-tasks"></span></a>
					<nav>
						<ul class="menu pull-right">
							
							<!-- MENSAJES -->
							<li>
								<?php if ( $this->session->userdata('cant_mensajes') > 0 ){ ?>
									<a href="<?=base_url('/backend/contacto/contactos/no_leidos')?>">
										<span class="glyphicon glyphicon-envelope naranja"></span>
										<span class="badge"><?=$this->session->userdata('cant_mensajes')?></span>
								<?php }else{ ?>
									<a href="<?=base_url('/backend/contacto')?>">
										<span class="glyphicon glyphicon-envelope"></span>
								<?php } ?>
								</a>
							</li>

							<li><a href="javascript:void(0)"><span class="glyphicon glyphicon-cog"></span> Usuarios</a>
							   <ul class="sub-menu">
								   <li><a href="<?php echo base_url('/backend/usuario/nuevo') ?>">Nuevo usuario</a></li>
								   <li><a href="<?php echo base_url('/backend/usuario') ?>">Listar usuarios</a></li>
						  		</ul>
							</li>
							<li><a href="javascript:void(0)"><span class="glyphicon glyphicon-user"></span> <?= $userdata['username'] ?></a>
							   <ul class="sub-menu">
								   <li><a href="<?php echo base_url('/backend/perfil') ?>">Mi perfil</a></li>
								   <li><a href="<?php echo base_url('/backend/login/cerrar_sesion/') ?>">Cerrar sesión</a></li>
						  		</ul>
							</li>
						</ul>
						<div class="col-xs-2 hidden-xs hidden-sm hidden-md hidden-lg" id="logo_rafam_menu">
							<a title="Inicio" href="<?=base_url()?>"><img src="<?=base_url()?>assets/imagenes/loguito_rafam.png"></a>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('plantillas/flashes'); ?>
