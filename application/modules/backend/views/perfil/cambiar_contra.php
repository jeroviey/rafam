<section>
	<div class="container">
		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php echo form_open('backend/perfil/actualizar_contra');?>

				<div class="form-group">
			    	<label for="oldpassword"> Contraseña actual </label>
				    	<input 
				    		type="password"
				    		class="form-control"
				    		id="oldpassword" 
				    		name="oldpassword" 
				    		placeholder="Escriba su contraseña actual"
				    	/>
			    		<?php echo form_error('oldpassword',"<p class='bg-danger'>","</p>"); ?>
			    </div>

				<div class="form-group">
			    	<label for="password"> Contraseña nueva </label>
				    	<input 
				    		type="password"
				    		class="form-control"
				    		id="password" 
				    		name="password" 
				    		placeholder="Escriba su nueva contraseña"
				    	/>
			    		<?php echo form_error('password',"<p class='bg-danger'>","</p>"); ?>
			    </div>

			    <div class="form-group">
			    	<label for="passconf"> Confirmar contraseña </label>
				    	<input 
				    		type="password"
				    		class="form-control"
				    		id="passconf" 
				    		name="passconf" 
				    		placeholder="Vuelva a escribir su nueva contraseña"
				    	/>
			    		<?php echo form_error('passconf',"<p class='bg-danger'>","</p>"); ?>
			    </div>


			    <div class="pull-right">
			        <a href="<?php echo base_url('/backend/perfil') ?>" class="btn btn-default">Cancelar</a>
			        <button type="submit" class="btn btn-primary">Guardar</button>
			    </div>

			<?php echo form_close();?>
		</div>
	</div>
</section>
