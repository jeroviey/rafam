<section>
	<div class="container">
		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="pull-right"><a href="<?=base_url()?>backend/perfil/cambiar_contra" class="btn btn-danger">Cambiar clave</a></div>            
			<?php 
			    $datos = array('accion'=> 'backend/perfil/actualizar',
			                   'submit'=> 'Guardar cambios');
			    $this->load->view('usuario/form', $datos); 
			?>
		</div>
	</div>
</section>
