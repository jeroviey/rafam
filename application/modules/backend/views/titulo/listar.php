<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3><?=$titulo->get_nombre()?> <a class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/editar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a></h3>
                </div>
            </div>

            <div>
        		 <table class="tabla table-responsive table table-hover table-condensed">
	                <thead>
	                    <tr>
	                        <th class="campos" > Título </th>
	                        <th class="campos" > Acciones </th>
	                    </tr>
	                </thead>

	                <tbody>

            				<?php foreach ($titulo->get_contenidos() as $contenido) { ?>
			                	<tr>
	                                <td> <?= $contenido->get_titulo() ?> </td>
	                                <td> 
	                                    <a title="Editar" data-toggle="tooltip" data-placement="top" href="<?=base_url()?>backend/contenido/editar/<?= $contenido->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
	                                    <a data-href="<?=base_url()?>backend/contenido/borrar/<?= $contenido->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el contenido <?= $contenido->get_titulo() ?> ?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a>
	                                </td>
                            	</tr> 
            				<?php } ?>

	                </tbody>

	            </table>

	            <a class="boton pull-right" href="<?=base_url('backend/contenido/nuevo/'.$titulo->get_id())?>"><h5 class="derecha">NUEVO CONTENIDO</h5></a>

            </div>         

        </div>

    </div>

</section>