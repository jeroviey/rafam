<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/titulo/actualizar/'.$titulo->get_id(),
			                   'titulo_form' => 'Editar título',
			                   'submit'=> 'Actualizar',
			                   'volver' => base_url('/backend/titulo/listar/'.$titulo->get_id()),
			                   'titulo' => $titulo);

			    $this->load->view('titulo/form', $datos); 
			?>
		</div>
    </div>
</section>