<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/email_notificacion/nuevo_guardar',
                               'titulo' => 'Nuevo email',
                               'submit'=> 'Guardar',
                               'volver' => $volver,
                               );
                $this->load->view('backend/email_notificacion/form', $datos); 
            ?>
        </div>
    </div>
</section>
