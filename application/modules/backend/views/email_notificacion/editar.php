<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/email_notificacion/editar_guardar/'.$notificacion_email->get_id(),
			                   'titulo' => 'Edición email',
			                   'submit'=> 'Actualizar',
			                   'notificacion_email'=> $notificacion_email,
			                   'volver' => $volver,
			                   );

			    $this->load->view('backend/email_notificacion/form', $datos); 
			?>
		</div>
    </div>
</section>