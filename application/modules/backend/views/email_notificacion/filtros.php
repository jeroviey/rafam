<p>Filtrar por</p>
	<?=form_open('backend/email_notificacion/listar/1', array('method' => 'get','class' => "form", "role"=>"form"))?>
		
    	<div class="form-group col-md-4">
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="<?php if(isset($busqueda['nombre'])){ echo $busqueda['nombre']; } ?>">
        </div>

        <div class="form-group col-md-4">
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php if(isset($busqueda['email'])){ echo $busqueda['email']; } ?>">
        </div>

		<div class="form-group col-md-4">
			<?= form_dropdown('id_municipio', $municipios, isset($busqueda['id_municipio']) ? $busqueda['id_municipio'] : '',"class=form-control") ?>
		</div>

    	<div class="form-group col-md-12">
            <button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-search"></span> Filtrar</button>  
        </div>

        <?php if(isset($busqueda)): ?>
            <div class="form-group col-md-12">
              <button class="btn btn-default btn-block" type="submit" name="clear"><span class="glyphicon glyphicon-remove"></span> Limpiar filtros</button>  
            </div>
        <?php endif ?>

	<?=form_close()?>