<div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>

<div class="HB_contenido">
	<?php echo form_open($accion);?>
		
		<div class="form-group">
			<label>Municipio</label>
			<?= form_dropdown('id_municipio', $municipios, isset($notificacion_email) ? (($notificacion_email->tiene_municipio()) ? $notificacion_email->get_municipio()->get_id() : '') : '',"class=form-control") ?>
			<?php echo form_error('id_municipio',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Nombre</label>
			<input 
				type="text" 
				name="nombre" 
				class="form-control" 
				id="nombre"
				placeholder="Ingrese el nombre"
				value="<?php echo set_value('nombre', isset($notificacion_email) ? $notificacion_email->get_nombre() : '' ); ?>"
			>
			<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<div class="form-group">
			<label>Email</label>
			<input 
				type="text" 
				name="email" 
				class="form-control" 
				id="email"
				placeholder="Ingrese el email"
				value="<?php echo set_value('email', isset($notificacion_email) ? $notificacion_email->get_email() : '' ); ?>"
			>
			<?php echo form_error('email',"<p class='bg-danger'>","</p>"); ?>
		</div>

			
	    <div class="pull-right">
		    <a href="<?php echo base_url($volver) ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>

	<?php echo form_close();?>	       	
</div>