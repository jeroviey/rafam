<section class="section_azul">
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>Nuevo tipo documentación</h3>
			    </div>
			    <div class="col-xs-2">
				    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
			    </div>
			</div>

			<div class="HB_contenido">
				<?php echo form_open_multipart('backend/tipo_documentacion/nuevo_guardar/');?>

					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input 
							type="text" 
							name="nombre" 
							class="form-control" 
							id="nombre"
							placeholder="Ingrese el nombre del nuevo tipo de documentación"
							value="<?php echo set_value('nombre', isset($tipo_documentacion) ? $tipo_documentacion->get_nombre() : '' ); ?>"
						>
						<?php echo form_error('nombre',"<p class='bg-danger'>","</p>"); ?>

					</div>
				
				    <div class="pull-right">
					    <a href="<?= base_url('/backend/tipo_documentacion') ?>" class="btn btn-default">Cancelar</a>
					    <button type="submit" class="btn btn-primary"> Guardar </button>
					</div>

				<?php echo form_close();?>	       	
			</div>

        </div>
    </div>
</section>