<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de los tipos de documentaciones</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Tipo </th>
                        <th class="campos" > Documentación relacionada </th>
                        <th class="campos" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($tipos_documentaciones)) { ?>
                    
                    	<?php foreach ($tipos_documentaciones as $tipo){?>
                            <tr>
                                <td> <?=$tipo->get_nombre() ?> </td>
                                <td> <?=count($tipo->get_documentaciones()) ?> </td>
                                <td class="centrado"> 
                                    <a data-href="<?=base_url()?>backend/tipo_documentacion/borrar/<?= $tipo->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el tipo de documentación seleccionada?"><span data-toggle="tooltip" data-placement="top" title="Borrar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>


            <a class="boton pull-right" href="<?=base_url()?>backend/tipo_documentacion/nuevo/"><h5 class="derecha">NUEVO</h5></a>

            
            <div class="text-center">
                <span><?php //echo $p_links; ?></span>
            </div>

            <?php if (empty($tipos_documentaciones)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ningun tipo de documentación en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>