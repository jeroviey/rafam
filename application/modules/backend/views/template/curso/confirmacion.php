<div>
	<div><a href="frontend/inicio/index" data-toggle="tooltip" data-placement="bottom" title="Inicio"><img src="logo_rafam_azul_sinfondo.png"></a></div>
	<br>
	<br>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;">Web de RAFAM.</h3>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong> Le recordamos que el curso "<?= $curso->get_nombre()?>" al cual se ha inscripto se dictará los días <?= $clase->get_dias().' de '.$meses[$clase->get_mes()]?>.</strong></p>
	<?php if($practica){ ?>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong> El mismo incluye actividades prácticas..</strong></p>
	<?php } ?>
	
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;"><strong> Los profesores a cargo del mismo son: </strong></p>
	<?php foreach ($curso->get_profesores() as $profesor) { ?>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;"> <?= $profesor.' ('.$profesor->get_email().')' ?></p>
	<?php } ?>
	<br>
	<div id="linea" style="border-top: 1px solid #adadad; padding-top: 20px;">
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;"><strong>Para más información comunicarse a:</strong></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Email: <a href="" id="email" style="color: #999; text-align: center; font-size: 1em;">cursosrafam@ec.gba.gov.ar</a></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Teléfono:(0221) 429-4484 / 4509</p> 
	</div>
</div>