<div>
	<div><a href="frontend/inicio/index" data-toggle="tooltip" data-placement="bottom" title="Inicio"><img src="logo_rafam_azul_sinfondo.png"></a></div>
	<br>
	<br>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;">Web de RAFAM.</h3>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong><?= $inscripcion ?></strong></p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong>Le informamos que su inscripción al curso de <?= $inscripcion->get_curso()->get_nombre() ?> ha sido ADMITIDA.</strong></p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong>El mismo se dictará <?= $inscripcion->get_clase();?></strong></p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 2em;">EN CASO DE NO ASISTIR, POR FAVOR NOTIFICAR.</p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;">Muchas gracias.</p>
	<br>
	<div id="linea" style="border-top: 1px solid #adadad; padding-top: 20px;">
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;"><strong>Para más información comunicarse a:</strong></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Email: <a href="" id="email" style="color: #999; text-align: center; font-size: 1em;">cursosrafam@ec.gba.gov.ar</a></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Teléfono:(0221) 429-4484 / 4509</p> 
	</div>
</div>