<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/tema/nuevo_guardar',
                               'titulo' => 'Nuevo tema',
                               'submit'=> 'Guardar tema',
                               'volver' => base_url('/backend/tema'),
                               'tema' => $tema);
                $this->load->view('tema/form', $datos); 
            ?>
        </div>
    </div>
</section>
