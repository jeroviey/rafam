<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<?php 
			    $datos = array('accion'=> 'backend/tema/editar_guardar/'.$tema->get_id(),
			                   'titulo' => 'Editar tema',
			                   'submit'=> 'Actualizar tema',
			                   'volver' => base_url('/backend/tema'),
			                   'tema' => $tema);

			    $this->load->view('tema/form', $datos); 
			?>
		</div>
    </div>
</section>
