<script src="<?=base_url();?>assets/js/jquery-sortable.js"></script>
<script src="<?=base_url();?>assets/js/sorteable.js"></script>

<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de temas</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="table">
                <tbody>
                    <tr>
                        <td> 
                            <ul id="sortable" class="simple_with_animation vertical">
                            <?php foreach ($temas as $tema): ?>
                                <li id="<?= $tema->get_id() ?>">
                                    <strong><?php echo $tema->get_nombre(); ?></strong>
                                </li>
                            <?php endforeach ?>
                            </ul>
                        </td>
                    </tr> 
                </tbody>
            </table>
            
            <form name="edit_order" action="<?php echo base_url('backend/tema/save_order'); ?>" method="post">
                <input type="hidden" class="form-control" id="ids_cargos" name="ids_cargos">
                <div class="pull-right">
                    <a href="<?php echo base_url('backend/tema') ?>" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Ordenar</button>
                </div>

            </form>

        </div>

    </div>

</section>