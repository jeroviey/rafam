<section>

    <div class="container">

        <div class="row hoja_blanca col-xs-10 col-xs-offset-1">

            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3>Listado de temas</h3>
                </div>

                <div class="col-xs-2">
                    <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
                </div>
            </div>

            <table class="tabla table-responsive table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="campos" > Tema </th>
                        <th class="campos centrado" > Acciones </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (!empty($temas)) { ?>
                    
                    	<?php foreach ($temas as $tema){?>
                            <tr>
                                <td> <?= $tema->get_nombre() ?> </td>
                                <td class="centrado"> 
                                    <a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/tema/editar/<?= $tema->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                                    <a data-toggle="tooltip" data-placement="top" title="Ordenar preguntas" href="<?=base_url()?>backend/tema/ordenar_preguntas/<?= $tema->get_id() ?>"><span class="glyphicon glyphicon-sort gris_BA"></a>
                                    <a data-href="<?=base_url()?>backend/tema/borrar/<?= $tema->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar el tema '<?= $tema->get_nombre();?>'?"><span data-toggle="tooltip" data-placement="top" title="Eliminar" class="glyphicon glyphicon-remove gris_BA"></a>
                                </td>
                            </tr> 

                        <?php } ?>

                    <?php }else{ ?>
                        <tr>
                            <td rowspan="8"> </td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            <div class="text-center">
                <span><?php echo $p_links; ?></span>
            </div>

            <a class="boton pull-right" href="<?=base_url()?>backend/tema/nuevo/"><h5 class="derecha">NUEVO TEMA</h5></a>
            <a class="boton pull-right" href="<?=base_url()?>backend/tema/ordenar"><h5 class="derecha">ORDENAR TEMAS</h5></a>
            <a class="boton pull-right" href="<?=base_url()?>backend/pregunta/"><h5 class="derecha">EDITAR PREGUNTAS</h5></a>

            <?php if (empty($temas)) { ?>
                <div class="text-center">
                    <h3><p style="color: black"> No existe ningun tema en el sistema </p></h3>                              
                </div>
            <?php } ?> 

        </div>

    </div>

</section>