<section>
    <div class="container">

         <div class="col-md-4 col-sm-6 col-xs-12 centrado">
            <a href="<?=base_url()?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_frontend_SF.png"></a>
                <h3 class="blanco">PORTADA</h3>
        </div>

    	<div class="col-md-4 col-sm-6 col-xs-12 centrado">
        <a href="<?php echo base_url('/backend/noticia/') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_noticia_SF.png"></a>
        	<h3 class="blanco">Noticias</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
        <a href="<?php echo base_url('/backend/curso') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_cronograma_SF.png"></a>
        	<h3 class="blanco">Cronograma cursos</h3>
        </div>


        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
            <a href="<?=base_url('backend/actualizacion')?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_actualizaciones_SF.png"></a>
                <h3 class="blanco">Actualizaciones</h3>
        </div>


        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
            <a href="<?=base_url('backend/documentacion')?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_documentacion_SF.png"></a>
            	<h3 class="blanco">Documentación</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
                <a href="<?php echo base_url('/backend/inscripcion') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_capacitacion_SF.png"></a>
                <h3 class="blanco">Inscripción cursos</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
                <a href="<?php echo base_url('/backend/pregunta') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_mesadeayuda_SF.png"></a>
            	<h3 class="blanco">Preguntas frecuentes</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
                <a href="<?php echo base_url('/backend/modulo') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_modulos_SF.png"></a>
                <h3 class="blanco">Módulos</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
                <a href="<?php echo base_url('/backend/profesor') ?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_docentes_SF.png"></a>
                <h3 class="blanco">Profesores</h3>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 centrado">
            <a href="<?=base_url('backend/archivo_sistema')?>"><img id="iconos_menu_backend" class="" src="<?=base_url()?>assets/imagenes/icon_descarga_archivos.png"></a>
                <h3 class="blanco">Descarga Archivos</h3>
        </div>

    </div>
</section>
