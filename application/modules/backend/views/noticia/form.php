<script type="text/javascript" src="<?=base_url();?>assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    language : 'es',
    height : 500,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    // ===========================================
  	// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
  	// ===========================================
	
  	relative_urls: false
});
</script>
 <div class="HB_encabezado">
    <div class="col-xs-10">
        <h3><?php echo $titulo ?></h3>
    </div>
    <div class="col-xs-2">
	    <span class="glyphicon glyphicon-circle-arrow-up HB_circulo"></span>
    </div>
</div>
<div class="HB_contenido">
	<?php echo form_open($accion);?>
		<div class="form-group">
			<label>Título</label>
			<input type="text" name="titulo" class="form-control" id="titulo" value="<?php echo set_value('titulo', $noticia->get_titulo() ); ?>">
			<?php echo form_error('titulo',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<div class="form-group">
			<label>Texto</label>
	    	<textarea name="texto" style="width:100%"><?php echo set_value('texto', $noticia->get_texto() ); ?></textarea>
	    	<?php echo form_error('texto',"<p class='bg-danger'>","</p>"); ?>
	    </div>
	    <div class="pull-right">
		    <a href="<?php echo $volver ?>" class="btn btn-default">Cancelar</a>
		    <button type="submit" class="btn btn-primary"><?php echo $submit ?></button>
		</div>
	<?php echo form_close();?>	       	
</div>