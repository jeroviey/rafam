<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
           <?php 
                $datos = array('accion'=> 'backend/noticia/actualizar/'.$noticia->get_id(),
                               'titulo' => 'Editar noticia',
                               'submit'=> 'Actualizar noticia',
                               'volver' => base_url('/backend/inicio'));
                $this->load->view('noticia/form', $datos); 
            ?>
        </div>
    </div>
</section>
