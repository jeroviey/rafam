<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Tema extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->temas();
    }

    public function temas($page = 1)
    {
        $data['volver'] = 'backend/pregunta';
        $rows = $this->em->getRepository('Entity\Tema')->findBy(array(), array('orden' => 'ASC'));
        $config = init_pagination('backend/tema/temas/',count($rows));
        $this->pagination->initialize($config);
        $data["p_links"] = $this->pagination->create_links();
    	$data['temas'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('tema/lista', $data);
    }

    public function ordenar()
    {
        $data['volver'] = 'backend/tema';
        $data['temas'] = $this->em->getRepository('Entity\Tema')->findBy(array(), array('orden' => 'ASC'));
        $this->load->template_backend('tema/ordenar', $data);
    }

    public function save_order()
    {
        $ids = $this->input->post('ids_cargos', true);
        if($ids == ''){
            $this->session->set_flashdata('advertencia','No se actualizo el orden de los temas');
            redirect('backend/tema');
        }
        else{
            $arreglo = explode( ',' ,$this->input->post('ids_cargos', true));
            foreach ($arreglo as $k => $id) {
                $tema = $this->em->getRepository('Entity\Tema')->find($id);
                $tema->set_orden($k);
            }
            $this->em->flush();
            $this->session->set_flashdata('noticia','El orden de los temas se guardo correctamente');
            redirect('backend/tema');
        }
    }

    public function ordenar_preguntas($id_tema)
    {
        $data['volver'] = 'backend/tema';
        $tema = $this->em->getRepository('Entity\Tema')->find($id_tema);
        if($tema){
            $data['preguntas'] = $tema->get_preguntas_ordenadas();
            $data['tema'] = $tema;
            $this->load->template_backend('tema/ordenar_preguntas', $data);
        }
    }

    public function save_order_faqs()
    {
        $ids = $this->input->post('ids_cargos', true);
        if($ids == ''){
            $this->session->set_flashdata('advertencia','No se actualizo el orden de los preguntas');
            redirect('backend/tema');
        }
        else{
            $arreglo = explode( ',' ,$this->input->post('ids_cargos', true));
            foreach ($arreglo as $k => $id) {
                $faq = $this->em->getRepository('Entity\Pregunta')->find($id);
                $faq->set_orden($k);
            }
            $this->em->flush();
            $this->session->set_flashdata('noticia','El orden de las preguntas se guardo correctamente');
            redirect('backend/tema');
        }
    }

    public function nuevo()
    {
        $data['volver'] = 'backend/tema';
        $data['tema'] = new Entity\Tema;
    	$this->load->template_backend('tema/nuevo', $data);
    }

    public function nuevo_guardar()
    {
        if ($this->form_validation->run($this, 'tema'))
    	{
            $tema= new Entity\Tema;
    		$tema->set_nombre($this->input->post('nombre', true));
            $this->em->persist($tema);
            $this->em->flush();

    		$this->session->set_flashdata('noticia', 'El tema ha sido creado correctamente');
            
            redirect('backend/tema');
    	}
    	else
    	{
    		$this->nuevo();
    	}
    }

    public function editar($id_tema)
    {
        $data['volver'] = 'backend/tema';
    	$data['tema'] = $this->em->getRepository('Entity\Tema')->find($id_tema);
    	$this->load->template_backend('tema/editar', $data);
    }

    public function editar_guardar($id_tema)
    {
        if ($this->form_validation->run($this, 'tema'))
    	{
    		$tema = $this->em->getRepository('Entity\Tema')->find($id_tema);
    		$tema->set_nombre($this->input->post('nombre', true));
            $this->em->persist($tema);
            $this->em->flush();

    		$this->session->set_flashdata('noticia', 'El tema ha sido editado correctamente');
            
            redirect('backend/tema');
    	}
    	else
    	{
    		$this->editar($id_tema);
    	}
    }

    public function borrar($id_tema)
    {
    	$tema = $this->em->getRepository('Entity\Tema')->find($id_tema);

        /**/
        // dar alerta si se quiere borrar un tema con preguntas
        /**/

        $this->em->remove($tema);
        $this->em->flush();            

        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente el tema");
        redirect('backend/tema');
    }    
}