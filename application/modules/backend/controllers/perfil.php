<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Perfil extends MY_Controller
{
    
    function __construct()
	{
	    parent::__construct();
	}

	public function index()
	{
		$userdata = $this->session->userdata('session_usuario');
		$datos['usuario'] = $this->em->getRepository('Entity\Usuario')->findOneById($userdata['id']);
		$this->load->template_backend('perfil/detalles', $datos);
	}

	public function actualizar()
	{
		$userdata = $this->session->userdata('session_usuario');
		$usuario = $this->em->getRepository('Entity\Usuario')->findOneById($userdata['id']);
		$usuario->set_nombre($this->input->post('nombre',true));
		$usuario->set_apellido($this->input->post('apellido',true));
		$usuario->set_email($this->input->post('email',true));
		$this->em->flush();
		$this->session->set_flashdata('noticia','El perfil sido actualizado correctamente');
		redirect('backend/inicio/');
	}

	public function cambiar_contra()
    {
        $this->load->template_backend('perfil/cambiar_contra');
    }

    public function actualizar_contra()
    {
    	if ($this->form_validation->run($this, 'cambiar_contra'))
    	{
        	$userdata = $this->session->userdata('session_usuario');
        	$usuario = $this->em->getRepository('Entity\Usuario')->find($userdata['id']);
        	$usuario->set_password(encriptar_contra(($this->input->post('password', true))));
	        $usuario->set_password_generada(0);
	        $this->em->flush();

	        $this->session->set_flashdata('noticia','Contraseña actualizada correctamente');
			redirect('backend/perfil/');
    	}
    	else
    	{
    		$this->cambiar_contra();
    	}
    }


    //CALLBACK
    public function match_pass($value)
    {
        $userdata = $this->session->userdata('session_usuario');
        $usuario = $this->em->getRepository('Entity\Usuario')->find($userdata['id']);
        $this->form_validation->set_message('match_pass', 'La contraseña actual ingresada no coincide con su contraseña');
        if($usuario->get_password() == encriptar_contra($value))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}