<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Documentacion extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;          
    }
        
    public function index()
    {
        $this->lista_documentaciones();
    }

    public function lista_documentaciones($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Documentacion')->findAll();
        $config = init_pagination('backend/documentacion/lista_documentaciones/',count($rows));
        $this->pagination->initialize($config); 

        $data["p_links"] = $this->pagination->create_links();
        $data['documentaciones'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);

        $this->load->template_backend('documentacion/documentaciones', $data);
    }

    public function nuevo()
    {
        $data['volver'] = 'backend/documentacion';
        $data['tipos_documentaciones'] = colleccion_listado($this->em->getRepository('Entity\TipoDocumentacion')->findAll());
        $this->load->template_backend('documentacion/nuevo',$data);
    }

    public function nuevo_guardar()
    {
        if($this->form_validation->run($this, 'documentacion'))
        {
            if (is_uploaded_file($_FILES['archivo']['tmp_name'])) 
            {
                $documentacion = new Entity\Documentacion;
                $documentacion = $this->cargar_pdf($documentacion);
                $documentacion->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
                $documentacion->set_nombre($this->input->post('nombre', true));

                $tipo_documentacion = $this->em->getRepository('Entity\TipoDocumentacion')->find($this->input->post('tipo_documentacion_id', true));
                $tipo_documentacion->add_documentacion($documentacion);

                $this->em->flush();
                
                $this->session->set_flashdata('noticia','Documentación creada exitosamente.');
                redirect('backend/documentacion');
            }
            else
            {
                $this->session->set_flashdata('error','No ha ingresado ningun Archivo PDF, no se pudo crear la nueva documentación.');
                redirect('backend/documentacion/nuevo');
            }
        }
        else
        {
            $this->nuevo();
        }
    }

    public function cargar_pdf($documentacion)
    {
        $config['upload_path'] = $this->config->item('path_documentaciones');
        $config['allowed_types'] = 'pdf';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('archivo'))
        {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('backend/documentacion/nuevo');
        }
        else
        {
            $documentacion->set_path($this->config->item('path_documentaciones').($this->upload->data()['orig_name']));
            return $documentacion;
        }
    }

    public function descargar_pdf($id_documentacion)
    {
        $documentacion = $this->em->getRepository('Entity\Documentacion')->find($id_documentacion);

        descargar($documentacion->get_path());
    }

    public function borrar($id_documentacion)
    {
        $documentacion = $this->em->getRepository('Entity\Documentacion')->find($id_documentacion);

        eliminar_archivo($documentacion->get_path());        
        
        $this->em->remove($documentacion);
        $this->em->flush();         
        
        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente la documentación ".$documentacion->get_nombre().".");
        redirect('backend/documentacion');
    }   

}