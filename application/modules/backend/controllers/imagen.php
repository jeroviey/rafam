<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Imagen extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;          
    }

    public function editar($id)
    {
        $data['imagen'] = $this->em->getRepository('Entity\Imagen')->find($id);
        $this->load->template_backend('imagen/editar', $data); 
    }

    public function actualizar($id)
    {
        if(is_uploaded_file($_FILES['imagen']['tmp_name']))
        {
            $imagen = $this->em->getRepository('Entity\Imagen')->find($id);
            $nombre_imagen = $this->cargar_imagen("imagen");
            
            $this->eliminar_imagen($imagen->get_imagen());

            $imagen->set_imagen($nombre_imagen);
            $this->em->flush();

            $this->session->set_flashdata('noticia','La imagen '.$nombre_imagen.' fue cargada exitosamente');
            redirect('backend/imagen/editar/'.$id);
        }
        else
        {
            $this->session->set_flashdata('error','No ha ingresado ninguna nueva imagen.');
            redirect('backend/imagen/editar/'.$id);
        }

    }

    public function cargar_imagen($input_file = 'userfile')
    {
        $config['upload_path'] = $this->config->item('path_imagenes');
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '2024';
        $config['max_width']  = '2024';
        $config['max_height']  = '2008';
        $this->load->library('upload', $config);
        //verificamos si existe errores
        $_FILES['imagen']["name"] = quitar_tildes($_FILES['imagen']["name"]);
        if (!$this->upload->do_upload($input_file))
        {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('error', $error);
            return false;
        }   
        else
        {
            $file_info = $this->upload->data();
            return $file_info['file_name'];            
        }
    }

    public function eliminar_imagen($imagen)
    {
        if (file_exists($imagen))
        {
            unlink($imagen);
        }
    }
}