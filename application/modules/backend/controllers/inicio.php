<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Inicio extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();        
    }
        
    public function index()
    {
        $this->load->template_backend('index');     
    }
    
    
}
/*
*end modules/login/controllers/index.php
*/