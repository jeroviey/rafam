<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Tipo_documentacion extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;          
    }
        
    public function index()
    {
        $this->tipos_documentaciones();
    }

    public function tipos_documentaciones($page=1)
    {
        $data['volver'] = 'backend/documentacion';
        $rows = $this->em->getRepository('Entity\TipoDocumentacion')->findAll();
        $config = init_pagination('backend/tipo_documentacion/tipos_documentaciones/',count($rows));
        $this->pagination->initialize($config);
        $data["p_links"] = $this->pagination->create_links();
    	$data['tipos_documentaciones'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('tipo_documentacion/tipos_documentaciones', $data);
    }
    
    public function nuevo()
    {
        $data['volver'] = 'backend/tipo_documentacion';
    	$this->load->template_backend('tipo_documentacion/nuevo', $data);
    }

    public function nuevo_guardar()
    {
        if($this->form_validation->run($this, 'tipo_documentacion'))
        {
            $tipo_documentacion = new Entity\TipoDocumentacion;
            $tipo_documentacion->set_nombre($this->input->post('nombre', true));

            $this->em->persist($tipo_documentacion);

            $this->em->flush();
            
            $this->session->set_flashdata('noticia','Tipo de documentación creada exitosamente.');
            redirect('backend/tipo_documentacion');
        }
        else
        {
            $this->nuevo();
        }
    }

    public function borrar($id_tipo)
    {
        $tipo_documentacion = $this->em->getRepository('Entity\TipoDocumentacion')->find($id_tipo);

        if ( count($tipo_documentacion->get_documentaciones()) == 0 ) 
        {
            $this->em->remove($tipo_documentacion);
            $this->em->flush();

            $this->session->set_flashdata('noticia','El tipo de documentación "'.$tipo_documentacion->get_nombre().'" fue eliminado exitosamente.');
            redirect('backend/tipo_documentacion');
        }
        else
        {
            $this->session->set_flashdata('error','Para eliminar un tipo de documentación, el mismo no puede contener documentación asignados.');
            redirect('backend/tipo_documentacion');
        }
    }
}