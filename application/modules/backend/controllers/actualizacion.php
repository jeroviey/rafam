<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Actualizacion extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;          
    }

    public function index()   
    {
        $this->actualizaciones();
    }

    public function actualizaciones($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Actualizacion')->findAll();
        $config = init_pagination('backend/actualizacion/actualizaciones/',count($rows));
        $this->pagination->initialize($config); 

        $data["p_links"] = $this->pagination->create_links();
    	$data['actualizaciones'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('actualizacion/actualizaciones', $data);
    }
    
    public function nuevo()
    {
        $data['volver'] = 'backend/actualizacion';
    	$data['modulos'] = colleccion_listado($this->em->getRepository('Entity\Modulo')->findAll());
    	$this->load->template_backend('actualizacion/nuevo_paso_1',$data);
    }

    public function nuevo_paso_1()
    {
        if($this->form_validation->run($this, 'actualizacion'))
        {
            if (is_uploaded_file($_FILES['archivo']['tmp_name'])) 
            {
                $actualizacion = new Entity\Actualizacion;
                $actualizacion = $this->cargar_zip($actualizacion);
                $actualizacion->set_fecha_subida(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
                $actualizacion->set_tipo($this->input->post('tipo', true));
                $actualizacion->set_version($this->input->post('version', true));
                $modulo = $this->em->getRepository('Entity\Modulo')->find($this->input->post('modulo_id', true));
                $modulo->add_actualizacion($actualizacion);

                $this->em->flush();
                
                $act_data = array('actualizacion_id' => $actualizacion->get_id());
                $this->session->set_userdata('nueva_actualizacion', $act_data);
                redirect('backend/actualizacion/nuevo_paso_2');
            }
            else
            {
                $this->session->set_flashdata('error','No ha ingresado ningun Archivo ZIP, no se pudo crear la nueva actualización.');
                redirect('backend/actualizacion/nuevo');
            }
        }
        else
        {
            $this->nuevo();
        }
    }

    public function cargar_zip($actualizacion)
    {
        $config['upload_path'] = $this->config->item('path_actualizaciones');
        $config['allowed_types'] = 'rar|zip';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('archivo'))
        {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('backend/actualizacion/nuevo');
        }
        else
        {
            $actualizacion->set_archivo($this->config->item('path_actualizaciones').($this->upload->data()['orig_name']));
            return $actualizacion;
        }
    }

    public function error($url,$actualizacion, $error)
    {
        $data['volver'] = 'backend/actualizacion';

        $data['actualizacion'] = $actualizacion;
        $data['documentos'] = $this->em->getRepository('Entity\Documento')->findAll();
        $data['error'] = $error;
        $this->load->template_backend($url, $data);
    }

    public function nuevo_paso_2()
    {
        $id_actualizacion = $this->session->userdata('nueva_actualizacion')["actualizacion_id"];
        $data['volver'] = 'backend/actualizacion/borrar_cancelar/'.$id_actualizacion;
        $data['actualizacion'] = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);
        $data['documentos'] = $this->em->getRepository('Entity\Documento')->findAll();
        $this->load->template_backend('actualizacion/nuevo_paso_2', $data);
    }

    public function nuevo_finalizar($id_actualizacion)
    {
        $actualizacion = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);
        if ( ($this->input->post('doc_existentes') != NULL ) || ( $_FILES['documentos']['tmp_name'][0] != NULL ) ) 
        {
            if ($_FILES['documentos']['tmp_name'][0] != NULL ) 
            {
                $actualizacion = $this->carga_multiple($actualizacion);
            }

            if ($this->input->post('doc_existentes') != NULL )
            {
                foreach ( $this->input->post('doc_existentes') as $doc) 
                {
                    $documento = $this->em->getRepository('Entity\Documento')->find($doc);
                    $actualizacion->add_documento($documento);
                }
            }

            $this->em->flush();

            $this->notificar_emails($actualizacion);

            $this->session->set_flashdata('noticia','Actualización creada correctamente.');
            redirect('backend/actualizacion'); 
        }
        else
        {
            $documento_error = 'No ha ingresado o seleccionado ningun DOCUMENTO.';
            $url = 'actualizacion/nuevo_paso_2';
            $this->error($url,$actualizacion, $documento_error);
        }

    }

    public function notificar_emails($actualizacion)
    {
        $emails = $this->em->getRepository('Entity\NotificacionEmail')->findAll();
        $subject = "RAFAM - Actualización de módulo";
        $message = $this->load->view('backend/template/modulo/actualizacion_actualizada', array('actualizacion' => $actualizacion), true);
        foreach ($emails as $email) 
        {
            $to = $email->get_email();
            enviar_email($to,$subject,$message);
            
        }
    }

    public function editar($id_actualizacion)
    {
        $data['volver'] = 'backend/actualizacion';
    	$data['actualizacion'] = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);
        $data['documentos'] = $this->em->getRepository('Entity\Documento')->findAll();
    	$this->load->template_backend('actualizacion/editar',$data);
    }

    public function editar_guardar($id_actualizacion)
    {
        $actualizacion = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);

        if ( ($this->input->post('doc_existentes') != NULL ) || ( $_FILES['documentos']['tmp_name'][0] != NULL ) )
        {
            if ($_FILES['documentos']['tmp_name'][0] != NULL ) 
            {
                $actualizacion = $this->carga_multiple($actualizacion);
            }

            if ($this->input->post('doc_existentes') != NULL )
            {
                foreach ( $this->input->post('doc_existentes') as $doc) 
                {
                    $documento = $this->em->getRepository('Entity\Documento')->find($doc);
                    $actualizacion->add_documento($documento);
                }
            }

            $this->em->flush();

            $this->session->set_flashdata('noticia','Actualización editada correctamente.');
            redirect('backend/actualizacion');
    	}
        else
    	{
    		$documento_error = 'No ha ingresado o seleccionado ningun DOCUMENTO.';
            $url = 'actualizacion/editar';
            $this->error($url,$actualizacion, $documento_error);
    	}
    }

    public function carga_multiple($actualizacion)
    {
        $config['upload_path'] = $this->config->item('path_documentos');
        $config['allowed_types'] = 'pdf';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_multi_upload('documentos'))
        {
            $error = $this->upload->display_errors();
            $this->error($actualizacion, $error);
        }
        else
        {
            $documentos = $this->upload->get_multi_upload_data();
            foreach ($documentos as $doc) 
            {
                $documento = new Entity\Documento;
                $documento->set_nombre($doc['file_name']);
                $documento->set_path($doc['full_path']);
                $actualizacion->add_documento($documento);
            }
            return $actualizacion;
        }
    }

    public function borrar($id_actualizacion, $msj = TRUE)
    {
        $actualizacion = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);

        //elimino los documentos?
        foreach ($actualizacion->get_documentos() as $documento)
        {
            if ($documento->puedo_eliminar($actualizacion)) 
            {
                eliminar_archivo($documento->get_path());
                //borra al objeto y a la tabla intermedia
                $this->em->remove($documento);
            }
        }

        eliminar_archivo($actualizacion->get_archivo());

        $this->em->flush();         
        
        $this->em->remove($actualizacion);
        $this->em->flush();         

        if ($msj) 
        {
            $this->session->set_flashdata('noticia', "Se ha eliminado correctamente la actualizacion.");
        }
        redirect('backend/actualizacion');
    }

    public function borrar_cancelar($id_actualizacion)
    {
        $this->borrar($id_actualizacion, FALSE);
    }


    public function ver_pdf($id_documento)
    {
        $documento = $this->em->getRepository('Entity\Documento')->find($id_documento);

        ver($documento->get_path(), 'backend/actualizacion');
    }

    public function descargar_zip($id_actualizacion)
    {
        $actualizacion = $this->em->getRepository('Entity\Actualizacion')->find($id_actualizacion);

        descargar($actualizacion->get_archivo(), 'backend/actualizacion');
    }

}