<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Profesor extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->profesores();     
    }

    public function profesores($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Profesor')->findAll();
        $config = init_pagination('backend/profesor/profesores/',count($rows));
        $this->pagination->initialize($config);
        $data["p_links"] = $this->pagination->create_links();
        $data['profesores'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('profesor/lista', $data);     
    }    

    public function nuevo()
    {
        $data['volver'] = 'backend/profesor';
        $data['profesor'] = new Entity\Profesor;
    	$this->load->template_backend('profesor/nuevo', $data);
    }

    public function nuevo_guardar()
    {
        if ($this->form_validation->run($this, 'profesor'))
    	{
            $profesor = new Entity\Profesor;
    		$this->cargar($profesor);

    		$this->session->set_flashdata('noticia', 'El profesor ha sido creado correctamente');
            
            redirect('backend/profesor');
    	}
        else
        {
            $this->nuevo();
        }
    }

    public function editar($id_profesor)
    {
        $data['volver'] = 'backend/profesor';
        $data['profesor'] = $this->em->getRepository('Entity\Profesor')->find($id_profesor);
        $this->load->template_backend('profesor/editar', $data);

    }

    public function editar_guardar($id_profesor)
    {
        if ($this->form_validation->run($this, 'profesor'))
        {
            $profesor = $this->em->getRepository('Entity\Profesor')->find($id_profesor);
            $this->cargar($profesor);

            $this->session->set_flashdata('noticia', 'El profesor ha sido editado correctamente');
            
            redirect('backend/profesor');
        }
        else
        {
            $this->editar($id_profesor);
        }
    }

    public function cargar($profesor)
    {
        $profe = $this->input->post('profesor', true);
        $profesor->set_nombre($profe['nombre']);
        $profesor->set_apellido($profe['apellido']);
        $profesor->set_email($profe['email']);

        $this->em->persist($profesor);
        $this->em->flush();
    }

    public function borrar($id_profesor)
    {
    	$profe = $this->em->getRepository('Entity\Profesor')->find($id_profesor);

        $this->em->remove($profe);
        $this->em->flush();            

        $this->session->set_flashdata('noticia', "El profesor se ha eliminado correctamente");
        redirect('backend/profesor');
    }    
}