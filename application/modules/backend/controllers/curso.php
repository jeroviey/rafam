<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Curso extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->cursos();
    }

    public function cursos($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Curso')->findAll();
        $config = init_pagination('backend/curso/cursos',count($rows));
        $this->pagination->initialize($config); 
        $data["p_links"] = $this->pagination->create_links();
    	$data['cursos'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);

        $this->load->template_backend('curso/lista', $data);     
    }

    public function nuevo()
    {
        $data['volver'] = 'backend/curso';
        $data['curso'] = new Entity\Curso;
        if($this->input->post())
        {
    		$this->cargar_datos($data['curso']);
        }
        $data['profesores'] = $this->em->getRepository('Entity\Profesor')->findAll();
        $data['modulos'] = $this->em->getRepository('Entity\Modulo')->findAll();
        $data['profesores_actuales'] = $data['curso']->get_profesores();
        $data['clases'] = $data['curso']->get_clases();
        $this->load->template_backend('curso/nuevo', $data);
    }

    public function nuevo_guardar()
    {
        if ($this->form_validation->run($this, 'curso'))
        {
            $curso = new Entity\Curso;
            $this->cargar_datos($curso);
            $this->em->persist($curso);
            $this->em->flush();
            $this->enviar_email_nuevo_curso($curso);

    		$this->session->set_flashdata('noticia', 'El curso ha sido creado correctamente');
            redirect('backend/curso');
    	}
    	else
    	{
    		$this->nuevo();
    	}
    }

    public function enviar_notificacion_reprogramacion($id_curso, $id_clase)
    {
        $subject = "RAFAM - Reprogramación de curso";
        $url = 'backend/template/curso/reprogramacion';
        $this->enviar_email_notificacion($id_curso, $id_clase, $subject, $url);
    }

    public function enviar_notificacion_cambio_aula($id_curso, $id_clase)
    {
        $subject = "RAFAM - Cambio de aula";
        $url = 'backend/template/curso/cambio_aula';
        $this->enviar_email_notificacion($id_curso, $id_clase, $subject, $url);
    }

    public function enviar_email_notificacion($id_curso, $id_clase, $subject, $url)
    {
        $curso = $this->em->getRepository('Entity\Curso')->find($id_curso);
        $clase = $this->em->getRepository('Entity\Clase')->find($id_clase);
        $meses = array('4' => 'Abril', '5' => 'Mayo', '6' => 'Junio', '7' => 'Julio', '8' => 'Agosto', '9' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre');
        $message = $this->load->view($url, array('curso' => $curso, 'clase' => $clase, 'meses' => $meses), true);
        // a los profesores
        foreach ($curso->get_profesores() as $profesor)
        {
            $to = $profesor->get_email();
            enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
        }
        // a los alumnos
        $inscriptos = $this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($id_curso, $id_clase);
        foreach ($inscriptos as $inscripto)
        {
            $to = $inscripto->get_email();
            enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
        }

        $this->session->set_flashdata('noticia', 'Se envio exitosamente la notificación de actualizacion del curso '.$curso->get_nombre());
            
        redirect('backend/curso');
    }

    public function cargar_datos(&$curso)
    {
        $curso->set_nombre($this->input->post('nombre', true));
        $modulo = $this->em->getRepository('Entity\Modulo')->find($this->input->post('modulo', true));
        if($modulo)
        {
            $curso->set_modulo($modulo);
        }

        //seguir mañana
        $archivo_id = $this->input->post('archivo_id', true);
        $archivo_nombre = $this->input->post('archivo_nombre', true);
        if($archivo_nombre != '')
        {
            if($archivo_id == 0){
                $file = new Entity\ArchivoCurso;
                $file->set_curso($curso);
            }
            else{
                $file = $this->em->getRepository('Entity\ArchivoCurso')->find($archivo_id);
            }
            $file->set_nombre($archivo_nombre);
            if(is_uploaded_file($_FILES['archivo_curso']['tmp_name']))
            {
                $this->cargar_archivo($file);
            }
        }

        $clases = $this->input->post('clase', true);
        $id_clases_actuales = $curso->get_id_clases();
        $ids = $clases['id'];
        $meses = $clases['mes'];
        $anios = $clases['anio'];
        $dias = $clases['dias'];
        $lugares = $clases['lugar'];
        $profesores = $this->input->post('profesor', true);

        foreach($meses as $i => $mes)
        {
            if($ids[$i] == 0){
                $clase = new Entity\Clase;
            }
            else{
                $clase = $this->em->getRepository('Entity\Clase')->find($ids[$i]);
                if (($key = array_search($ids[$i], $id_clases_actuales)) !== false) {
                    unset($id_clases_actuales[$key]);
                }
            }
            $clase->set_mes($mes);
            $clase->set_ano($anios[$i]);
            $clase->set_dias($dias[$i]);
            $clase->set_lugar($lugares[$i]);
            $clase->set_curso($curso);
        }
        foreach($id_clases_actuales as $clase)
        {
            $c = $this->em->getRepository('Entity\Clase')->find($clase);
            $curso->remove_clase($c);
            $this->em->remove($c);
        }
        $curso->remove_all_profesores();
        $nuevos = array();
        foreach($profesores['prof'] as $profe)
        {
            if(!in_array($profe, $nuevos))
            {
                $teacher = $this->em->getRepository('Entity\Profesor')->find($profe);
                if($teacher)
                {
                    $curso->add_profesor($teacher);
                }
                $nuevos[] = $profe;
            }
        }
    }

    public function cargar_archivo(&$archivoCurso)
    {
        $config['upload_path'] = $this->config->item('path_archivos');
        $config['allowed_types'] = 'pdf|doc|docx';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('archivo_curso'))
        {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('backend/curso/nuevo');
        }
        else
        {
            if($archivoCurso->get_path())
            {
                eliminar_archivo($archivoCurso->get_path()); 
            }
            $archivoCurso->set_path($this->config->item('path_archivos').($this->upload->data()['orig_name']));
        }
    }

    public function enviar_email_nuevo_curso($curso)
    {
        $meses = meses_cursos();
        $subject = "RAFAM - Nuevo curso disponible";
        $message = $this->load->view('backend/template/curso/nuevo', array('curso' => $curso, 'meses' => $meses), true);
        foreach ($curso->get_profesores() as $profesor)
        {
            $to = $profesor->get_email();
            enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
        }
    }

    public function editar($id_curso)
    {
        $data['volver'] = 'backend/curso';
        $data['curso'] = $this->em->getRepository('Entity\Curso')->find($id_curso);
        if($this->input->post())
        {
            $this->cargar_datos($data['curso']);
        }
        $data['profesores'] = $this->em->getRepository('Entity\Profesor')->findAll();
        $data['modulos'] = $this->em->getRepository('Entity\Modulo')->findAll();
        $data['profesores_actuales'] = $data['curso']->get_profesores();
        $data['clases'] = $data['curso']->get_clases();
        $this->load->template_backend('curso/editar', $data);

    }

    public function editar_guardar($id_curso)
    {
        if ($this->form_validation->run($this, 'curso'))
        {
            $curso = $this->em->getRepository('Entity\Curso')->find($id_curso);
            $this->cargar_datos($curso);
            $this->em->persist($curso);
            $this->em->flush();

    		$this->session->set_flashdata('noticia', 'El curso ha sido editado correctamente');
            
            redirect('backend/curso');
    	}
    	else
    	{
    		$this->editar($id_curso);
    	}
    }

    public function notificacion($id_curso)
    {
        $data['volver'] = 'backend/curso';
        $data['curso'] = $this->em->getRepository('Entity\Curso')->find($id_curso);
        $data['meses'] = meses_cursos();
        $data['clases'] = array();
        foreach ($data['curso']->get_clases() as $clase) 
        {
            $actual = array();
            $actual['clase'] = $clase;
            $actual['inscriptos'] = count($this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($data['curso']->get_id(), $clase->get_id()));
            $data['clases'][] = $actual;
        }
        $this->load->template_backend('curso/notificacion', $data);
    }

    public function enviar_notificacion($id_curso, $id_clase, $tipo_mensaje)
    {
        $curso = $this->em->getRepository('Entity\Curso')->find($id_curso);
        $clase = $this->em->getRepository('Entity\Clase')->find($id_clase);
        $meses = meses_cursos();
        if ($tipo_mensaje == 3) 
        {
            $subject = "RAFAM - Cancelación de curso";
            $message = $this->load->view('backend/template/curso/cancelacion', array('curso' => $curso, 'meses' => $meses, 'clase' => $clase), true);
        }
        else
        {
            $subject = "RAFAM - Confirmación de curso";
            $practica = ($tipo_mensaje == 1) ? true : false;
            $message = $this->load->view('backend/template/curso/confirmacion', array('curso' => $curso, 'meses' => $meses, 'clase' => $clase, 'practica' => $practica), true);
        }

        $inscriptos = $this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($id_curso, $id_clase);
        foreach ($inscriptos as $inscripto)
        {
            $to = $inscripto->get_email();
            enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
        }
        foreach ($curso->get_profesores() as $profesor)
        {
            $to = $profesor->get_email();
            enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');
        }

        $clase->set_estado($tipo_mensaje);
        $this->em->flush();

        $this->session->set_flashdata('noticia', "Se ha enviado correctamete la notificación del curso: ".$curso->get_nombre());
        redirect('backend/curso');
    }

    public function borrar($id_curso)
    {
        $curso = $this->em->getRepository('Entity\Curso')->find($id_curso);

        foreach($curso->get_clases() as $clase)
        {
            $resultado = $this->validar_clase($clase->get_id());
            if($resultado['estado'] == 0)
            {
                $this->session->set_flashdata('error', "El curso '".$curso->get_nombre()."' no puede ser eliminado ya que cuenta con al menos una clase en estado pendiente. Confirme si se dictará o no antes de eliminarlo.");
                redirect('backend/curso');
            }
        }

        $this->em->remove($curso);
        $this->em->flush();            

        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente el curso: ".$curso->get_nombre());
        redirect('backend/curso');
    }

    public function mostrar($id_curso)
    {
        $data['volver'] = 'backend/curso';
        $data['curso'] = $this->em->getRepository('Entity\Curso')->find($id_curso);
        $data['clases'] = array();
        foreach ($data['curso']->get_clases() as $clase) 
        {
            $actual = array();
            $actual['clase'] = $clase;
            $actual['inscriptos'] = count($this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($data['curso']->get_id(), $clase->get_id()));
            $data['clases'][] = $actual;
        }
        
        $this->load->template_backend('curso/mostrar', $data);
    }


    public function validar_clase($id_clase)
    {
        $clase = $this->em->getRepository('Entity\Clase')->find($id_clase);
        $estado = $clase->get_estado();
        $inscriptos = count($this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($clase->get_curso()->get_id(), $clase->get_id()));
        $resultado = array('estado' => $estado, 'inscriptos' => $inscriptos);
        if($this->input->is_ajax_request())
        {
            echo json_encode($resultado);
        }
        else
        {
            return $resultado;
        }
    }


    public function descargar_pdf($id_archivo)
    {
        $archivo = $this->em->getRepository('Entity\ArchivoCurso')->find($id_archivo);

        descargar($archivo->get_path());
    }

    public function generar_plantilla($id_curso, $id_clase)
    {
        $data['curso'] = $this->em->getRepository('Entity\Curso')->find($id_curso);
        $data['inscriptos'] = $this->em->getRepository('Entity\Inscripcion')->inscripcionesDeUnCursoYUnaClase($id_curso, $id_clase);        
        $data['clase'] = $this->em->getRepository('Entity\Clase')->find($id_clase);
        $data['clases'] = obtener_dias($data['clase']->get_dias());

        $this->load->view('curso/planilla_asistencia', $data);
        $html = $this->output->get_output();
        crearPdf($html, 'planilla de asistencia curso '.$data['curso']->get_nombre());
    }

    //CALLBACK
    public function clases_check($clases)
    {
        $meses = $clases['mes'];
        $dias = $clases['dias'];
        $lugar = $clases['lugar'];
        if(is_array($meses))
        {
            foreach($meses as $i => $mes)
            {
                if(($dias[$i] == '') || ($lugar[$i] == ''))
                {
                    $this->form_validation->set_message('clases_check', '<span class="glyphicon glyphicon-remove-sign"></span> Los campos Dias y Lugar son obligatorios');
                    return false;
                    break;
                }
            }
        }
        else
        {
            $this->form_validation->set_message('clases_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe indicar al menos una clase para el curso');
            return false;
        }
        return true;
    }

    public function profesor_check($profesores)
    {
        $profes = $profesores['prof'];
        if(is_array($profes))
        {
            foreach($profes as $prof_id)
            {
                if($prof_id == 0)
                {
                    $this->form_validation->set_message('profesor_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe seleccionar un profesor para cada campo profesor');
                    return false;
                    break;
                }
            }
        }
        else
        {
            $this->form_validation->set_message('profesor_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe indicar al menos un profesor para el curso');
            return false;
        }
        return true;
    }
    public function archivo_check($archivo_id)
    {
        $nombre = $this->input->post('archivo_nombre', true);

        $subio_archivo = is_uploaded_file($_FILES['archivo_curso']['tmp_name']);
        $ingreso_nombre = $nombre != '';

        if(($subio_archivo && !$ingreso_nombre) || (($archivo_id != 0) && !$ingreso_nombre))
        {
            $this->form_validation->set_message('archivo_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe ingresar un nombre para el archivo cargado');
            return false;
        }
        if(!$subio_archivo && $ingreso_nombre && ($archivo_id == 0))
        {
            $this->form_validation->set_message('archivo_check', '<span class="glyphicon glyphicon-remove-sign"></span> Debe subir un archivo');
            return false;
        }
        return true;
    }
}