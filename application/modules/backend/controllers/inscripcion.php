<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Inscripcion extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->inscripciones();
    }

    public function inscripciones($page = 1)
    {
        list($data,$busqueda) = $this->filtrar_inscripciones($page,"backend/inscripcion/inscripciones");
        
        // die(var_dump($busqueda));
        if(count($busqueda) > 1 || $busqueda['inscripcion'] != '') { $data['busqueda'] = $busqueda; }

        $this->load->template_backend('inscripcion/lista', $data);     
    }

    private function filtrar_inscripciones($page,$url,$unpaged=false)
    {
        $busqueda['inscripcion']= ''; 
        if(!isset($_GET['clear']))
        {
            if(count($this->input->get()) > 0)
            {
                if($this->input->get('curso_id', true) != '0')
                { 
                    $busqueda['curso'] = $this->input->get('curso_id', true); 
                }
                if($this->input->get('clase', true) != '0')
                { 
                    $busqueda['clase'] = $this->input->get('clase', true); 
                }
                if($inscripcion = $this->input->get('inscripcion', true))
                { 
                    $busqueda['inscripcion'] = $inscripcion; 
                }
                if($this->input->get('id_municipio', true) != '')
                { 
                    $busqueda['id_municipio'] = $this->input->get('id_municipio', true); 
                }
                if($this->input->get('estado', true) != '')
                { 
                    $busqueda['estado'] = $this->input->get('estado', true); 
                }
                if($this->input->get('asistencia', true) != '')
                { 
                    $busqueda['asistencia'] = $this->input->get('asistencia', true); 
                }

                $filtros = '?'.implode('&', array_map(function ($v, $k) { return sprintf("%s=%s", $k, $v); }, $busqueda, array_keys($busqueda)));

                if($busqueda['inscripcion'] != '' || sizeof($busqueda) > 1 )
                {
                    $this->session->set_userdata('inscripcion_filtro', array('filtro' => $filtros));
                    $this->session->set_userdata('inscripcion_busqueda', array('busqueda' => $busqueda));
                }
            } 
        }
        else
        {
            $this->session->unset_userdata('inscripcion_filtro');
            $this->session->unset_userdata('inscripcion_busqueda');
            redirect(current_url());
            break;
        }

        $filtros = $this->session->userdata('inscripcion_filtro')['filtro']; 
        $busqueda = $this->session->userdata('inscripcion_busqueda')['busqueda'];
        $rows = $this->em->getRepository('Entity\Inscripcion')->inscripcionGetBusqueda($busqueda);

        if(!$unpaged)
        {
            $config = init_pagination($url,count($rows), 20, 4,$filtros);
            $data['inscripciones'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
            $data["segmento"] = $page;
            $data["p_links"] = $this->pagination->create_links();

            $data['municipios'] = colleccion_listado($this->em->getRepository('Entity\Municipio')->findAll());
            $data['municipios'][''] = 'Lugar de trabajo';
            array_unshift ($data['municipios'] , 'OTROS');
            $data['cursos'] = $this->em->getRepository('Entity\Curso')->findAll();
        }
        else
        {
            $data['inscripciones'] = $rows;
        }
        return array($data,$busqueda);
    }

    public function asistencia($id, $presente)
    {
        if($id && $presente){
            $insc = $this->em->getRepository('Entity\Inscripcion')->find($id);
            if($presente == 'presente')
            {
                $this->session->set_flashdata('noticia', 'Se registro el presente de '.$insc->get_nombre().' '.$insc->get_apellido());
                $insc->set_asistencia(1);
            }
            else
            {
                $this->session->set_flashdata('noticia', 'Se registro la ausencia de '.$insc->get_nombre().' '.$insc->get_apellido());
                $insc->set_asistencia(0);
            }
            $this->em->flush();
        }
        redirect('backend/inscripcion');
    }

    public function notificacion($id_inscripcion)
    {
        $data['volver'] = 'backend/inscripcion';
        $data['inscripcion'] = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripcion);
        $this->load->template_backend('inscripcion/notificacion', $data);    
    }

    public function enviar_notificacion($id_inscripcion, $estado)
    {
        $inscripcion = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripcion);
        $inscripcion->set_aceptada($estado);

        $meses = meses_cursos();
        //aceptado
        if ($estado == 1) 
        {
            $subject = "Capacitación RAFAM – Inscripción admitida";
            $message = $this->load->view('backend/template/curso/admitido', array('inscripcion' => $inscripcion), true);
            
        }
        //rechazado
        elseif ($estado == 2) 
        {
            $subject = "Capacitación RAFAM – Inscripción NO admitida";
            $clases = obtener_clases_de_un_anio(date ("Y"), $inscripcion->get_curso(), $inscripcion->get_clase());
            $message = $this->load->view('backend/template/curso/no_admitido', array('inscripcion' => $inscripcion, 'clases' => $clases), true);
            
        }
        $to = $inscripcion->get_email();
        enviar_email($to,$subject,$message,false,'cursosrafam@ec.gba.gov.ar');

        $this->em->flush();

        $this->session->set_flashdata('noticia','Se a enviado un email notificando al inscripto '.$inscripcion.'.');
        redirect('backend/inscripcion');
    }

    public function dar_de_baja($id_inscripcion)
    {
        $inscripcion = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripcion);
        $inscripcion->set_aceptada('2');
        $this->em->flush();

        $this->session->set_flashdata('noticia','Se ha dado de baja con exito al inscripto '.$inscripcion.'.');
        redirect('backend/inscripcion');
    }

    public function mostrar($id)
    {
        if($id)
        {
            $data['volver'] = 'backend/inscripcion';
            $data['inscripcion'] = $this->em->getRepository('Entity\Inscripcion')->find($id);
            $this->load->template_backend('inscripcion/datos', $data);
        }
    }

    public function editar($id_inscripcion)
    {
        $data['inscripcion'] = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripcion);
        $this->load->template_backend('inscripcion/editar', $data);
    }

    public function editar_guardar($id_inscripcion)
    {
        
        if ($this->form_validation->run($this, 'inscripcion_edicion'))
        {
            $inscripcion = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripcion);
            $inscripcion->set_nombre($this->input->post('nombre', true));
            $inscripcion->set_apellido($this->input->post('apellido', true));
            $inscripcion->set_dni($this->input->post('dni', true));
            $inscripcion->set_email($this->input->post('email', true));
            $inscripcion->set_telefono($this->input->post('telefono', true));

            $this->em->persist($inscripcion);
            $this->em->flush();
            
            $this->session->set_flashdata('noticia','Datos de inscripción actualizados correctamente.');
            redirect('backend/inscripcion');
        }
        else
        {
            $this->editar($id_inscripcion);
        }
    }

    public function generar_pdf($id_inscripto)
    {
        $data['inscripcion'] = $this->em->getRepository('Entity\Inscripcion')->find($id_inscripto);        
       
        $clases = obtener_dias($data['inscripcion']->get_clase()->get_dias());
        $data['dia'] = $clases[count($clases)-1];

        $this->load->view('inscripcion/certificado_asistencia', $data);
        $html = $this->output->get_output();
        crearPdf($html, 'certificado de asistencia');
    }
}
