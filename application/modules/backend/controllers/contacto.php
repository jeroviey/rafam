<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Contacto extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {   
        $this->contactos();
    }

    public function contactos($lista = 'todos', $page = 1)
    {
        switch ($lista) 
        {
            case 'solucionado':
                $rows = $this->em->getRepository('Entity\Contacto')->findBy(array('solucionado' => 1),array('created_at' => 'DESC'));
                $url = 'backend/contacto/contactos/solucionado/';
                break;
            case 'no_solucionado':
                $rows = $this->em->getRepository('Entity\Contacto')->findBy(array('solucionado' => 0),array('created_at' => 'DESC'));
                $url = 'backend/contacto/contactos/no_solucionado';
                break;
            case 'leidos':
                $rows = $this->em->getRepository('Entity\Contacto')->findBy(array('leido' => 1),array('created_at' => 'DESC'));
                $url = 'backend/contacto/contactos/leidos';
                break;
            case 'no_leidos':
                $rows = $this->em->getRepository('Entity\Contacto')->findBy(array('leido' => 0),array('created_at' => 'DESC'));
                $url = 'backend/contacto/contactos/no_leidos';
                break;
            default:
                $rows = $this->em->getRepository('Entity\Contacto')->findBy(array(),array('created_at' => 'DESC'));
                $url = 'backend/contacto/contactos/todos';
                break;
        }

        $config = init_pagination($url,count($rows),10,5);
        $this->pagination->initialize($config); 

        $data["p_links"] = $this->pagination->create_links();
        $data['contactos'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('contacto/lista', $data);     
    }

    public function solucionado($id)
    {
        $contacto = $this->em->getRepository('Entity\Contacto')->find($id);
        $this->session->set_flashdata('noticia', 'Se registro el mensaje como solucionado');
        $contacto->set_fecha_solucionado(DateTime::createFromFormat('d/m/Y', date('d/m/Y')));
        $contacto->set_solucionado(1);

        $this->em->flush();

        redirect('backend/contacto');
    }

    public function mostrar($id)
    {
        if($id)
        {
            $contacto = $this->em->getRepository('Entity\Contacto')->find($id);
            $contacto->set_leido(1);

            $this->em->flush();

            $data['contacto'] = $contacto;
            $this->load->template_backend('contacto/ver_mensaje', $data);
        }
    }

    public function hay_mensajes()
    {
        return count($this->em->getRepository('Entity\Contacto')->findBy(array('leido' => 0)));
    }
 
    public function mensaje_json($id_contacto)
    {
        if($this->input->is_ajax_request())
        {
            $contacto = $this->em->getRepository('Entity\Contacto')->find($id_contacto);
            if ($contacto->get_leido() == 0) 
            {
                $contacto->set_leido(1);
                $this->em->flush();
            }

            echo json_encode(array(
                        'de' => $contacto->get_apellido().', '.$contacto->get_nombre().' ('.$contacto->get_email().')',
                        'lugar_trabajo' => ($contacto->get_municipio() ? 'Municipio de '.$contacto->get_municipio()->get_nombre() : $contacto->get_lugar_trabajo()),
                        'oficina' => $contacto->get_oficina(),
                        'cargo' => $contacto->get_cargo(),
                        'asunto' => $contacto->get_asunto(), 
                        'cuerpo' => $contacto->get_mensaje(),
                        'id_contacto' => $contacto->get_id(),
                        'esta_solucionado' => $contacto->get_solucionado(),
                        )
            );
        }
 
    }
}