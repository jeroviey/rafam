<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Contenido extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;   
    }
        
    public function nuevo($id_titulo)
    {
    	$data['titulo'] = $this->em->getRepository('Entity\Titulo')->find($id_titulo);
    	$data['contenido'] = new Entity\Contenido;
        $this->load->template_backend('contenido/nuevo', $data);     
    }

    public function guardar($id_titulo)
    {
    	if ($this->form_validation->run($this, 'contenido'))
    	{
    		$contenido = new Entity\Contenido;
    		$contenido->set_titulo($this->input->post('titulo', true));
    		$contenido->set_descripcion($this->input->post('descripcion', false));

    		$titulo = $this->em->getRepository('Entity\Titulo')->find($id_titulo);
    		$titulo->add_contenidos($contenido);
        
        	$this->em->flush();
   			$this->session->set_flashdata('noticia', 'El contenido con titulo "'.$contenido->get_titulo().'"" ha sido creado correctamente');
            
            redirect('backend/titulo/listar/'.$titulo->get_id());
    	}
    	else
    	{
    		$this->nuevo($id_titulo);
    	}
    }

    public function editar($id_contenido)
    {
        $data['contenido'] = $this->em->getRepository('Entity\Contenido')->find($id_contenido);
        $data['titulo'] = $data['contenido']->get_titulo_padre();
        $this->load->template_backend('contenido/editar', $data);     
    }

    public function actualizar($id_contenido)
    {
        if ($this->form_validation->run($this, 'contenido'))
        {
            $contenido = $this->em->getRepository('Entity\Contenido')->find($id_contenido);
            $contenido->set_titulo($this->input->post('titulo', true));
            $contenido->set_descripcion($this->input->post('descripcion', false));

            $titulo = $contenido->get_titulo_padre();
            $titulo->add_contenidos($contenido);
        
            $this->em->flush();
            $this->session->set_flashdata('noticia', 'El contenido con titulo "'.$contenido->get_titulo().'"" ha sido actualizado correctamente');
            
            redirect('backend/titulo/listar/'.$titulo->get_id());
        }
        else
        {
            $this->editar($id_contenido);
        }
    }

    public function borrar($id_contenido)
    {
        $contenido = $this->em->getRepository('Entity\Contenido')->find($id_contenido);

        $this->em->remove($contenido);
        $this->em->flush();         
        
        $this->session->set_flashdata('noticia', 'El contenido con titulo "'.$contenido->get_titulo().'"" ha sido eliminado correctamente');
        redirect('backend/titulo/listar/'.$contenido->get_titulo_padre()->get_id());
    }
        
}