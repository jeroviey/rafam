<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Usuario extends MY_Controller
{
    
    function __construct()
	{
	    parent::__construct();
	    $this->em = $this->doctrine->em;
	}

	public function index()
	{
		$userdata = $this->session->userdata('session_usuario');

		$datos['usuarios'] = $this->em->getRepository('Entity\Usuario')->findAll();
		$this->load->template_backend('usuario/index', $datos);
	}

    public function ver($id)
    {
        $datos['volver'] = 'backend/usuario';
        $datos['usuario'] = $this->em->getRepository('Entity\Usuario')->find($id);
        $this->load->template_backend('usuario/ver', $datos);
    }

	public function nuevo()
	{
        $datos['volver'] = 'backend/usuario';
		$datos['usuario'] = new Entity\Usuario;
		$this->load->template_backend('usuario/nuevo', $datos);
	}

	public function guardar()
	{
        if($this->form_validation->run($this, 'usuario') == TRUE)
        {        
    		$usuario = new Entity\Usuario;
    		$usuario->set_nombre($this->input->post('nombre',true));
    		$usuario->set_apellido($this->input->post('apellido',true));
    		$usuario->set_email($this->input->post('email',true));
    		$usuario->set_username($this->input->post('username',true));
    		$usuario->set_password(encriptar_contra());
    		$usuario->set_estado(false);
    		$usuario->set_password_generada(true);

    		$this->em->persist($usuario);
    		$this->em->flush();

    		$this->session->set_flashdata('noticia','El usuario "'.$usuario->get_username().'" fue creado exitosamente.');
    		redirect('backend/usuario/');
        }
        else
        {
            $this->nuevo();
        }
	}

    public function editar($id)
    {
        $datos['volver'] = 'backend/usuario';
        $datos['usuario'] = $this->em->getRepository('Entity\Usuario')->find($id);
        $this->load->template_backend('usuario/editar', $datos);
    }

    public function actualizar($id)
    {
        if($this->form_validation->run($this, 'usuario_editar') == TRUE)
        {        
            $usuario = $this->em->getRepository('Entity\Usuario')->find($id);
            $usuario->set_nombre($this->input->post('nombre',true));
            $usuario->set_apellido($this->input->post('apellido',true));
            $usuario->set_email($this->input->post('email',true));

            $this->em->persist($usuario);
            $this->em->flush();

            $this->session->set_flashdata('noticia','El usuario "'.$usuario->get_username().'" fue actualizado exitosamente.');
            redirect('backend/usuario/');
        }
        else
        {
            $this->editar($id);
        }
    }

    public function desactivar($id)
    {
        $usuario = $this->em->getRepository('Entity\Usuario')->find($id);
        $usuario->set_estado(false);

        $this->em->persist($usuario);
        $this->em->flush();
              
        $this->session->set_flashdata('noticia', 'El usuario '.$usuario->get_username().' ha sido desactivado correctamente');

        redirect('backend/usuario/');
    }

    public function activar($id)
    {
        $usuario = $this->em->getRepository('Entity\Usuario')->find($id);
        $usuario->set_estado(true);

        $this->em->persist($usuario);
        $this->em->flush();

        $this->session->set_flashdata('noticia', 'El usuario '.$usuario->get_username().' ha sido activado correctamente');
        redirect('backend/usuario/');
    }

    public function restablecer($id)
    {
        $usuario = $this->em->getRepository('Entity\Usuario')->find($id);

        $contra = random_string('alnum', 8);;
        $usuario->set_password(encriptar_contra($contra));
        $usuario->set_password_generada(true);

        $this->em->persist($usuario);
        $this->em->flush();

        $to = $usuario->get_email();
        $subject = 'RAFAM - Contraseña restablecida';
        $message = $this->load->view('backend/template/usuario/restablecimiento_contra', array('username' => $usuario->get_username(), 'password' => $contra), true);

        enviar_email($to,$subject,$message);

        $this->session->set_flashdata('noticia', 'Se genero exitosamente una nueva contraseña para el usuario "'.$usuario->get_username().'", esta información fue enviada al email del usuario.');
        redirect('backend/usuario/');
    }
}