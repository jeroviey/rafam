<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends MX_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;    
    }

    public function iniciar_sesion()
    {
        if($this->form_validation->run($this, 'login') == TRUE) 
        {
            redirect('inicio/login');
        }
        else
        {   
            $contra = encriptar_contra($this->input->post('password', true));
            $usuario = $this->em->getRepository('Entity\Usuario')->findOneBy(array('username' => $this->input->post('username', true),
                                                                                   'password' => $contra));
            if ( !empty($usuario) ) 
            {
                if($usuario->get_estado() == 1)
                {
                    $sesion_data = array(
                                    'online' => 1,
                                    'id' => $usuario->get_id(),
                                    'username' => $usuario->get_username()
                                    );

                    $this->session->set_userdata('session_usuario', $sesion_data);
                    
                    if($redir = $this->session->userdata('redir')) 
                    {
                        $this->session->unset_userdata('redir');
                    }

                    redirect($redir ? $redir : '/backend/inicio/index');    
                }
                else
                {
                    $this->session->set_flashdata('error_login', 'El usuario "'.$usuario->get_username().'" se encuentra desactivado');
                    redirect('inicio/login');
                }  
            }
            else
            {
                $this->session->set_flashdata('error_login', 'Usuario o Contraseña no válidos.');
                redirect('inicio/login');
            }
        }
    }

    public function cerrar_sesion()
    {
        $this->session->sess_destroy();
        redirect(base_url());  
    }

    function is_logged_in()
	{
	    $is_logged_in = $this->session->userdata('session_usuario');
	    if(!isset($is_logged_in) || $is_logged_in['online'] != true)
	    {
	        $this->session->set_userdata('redir', current_url());
	        $this->session->set_flashdata('error_login', 'Por favor inicie sesión');
	        redirect('inicio/login');      
	    }       
	}
}