<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Email_notificacion extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->listar();
    }

    public function listar($page = 1)
    {
    	list($data,$busqueda) = $this->filtrar_emails($page,"backend/email_notificacion/listar/");
        
        if(count($busqueda) > 1 || $busqueda['nombre'] != '' || $busqueda['email'] != '') { $data['busqueda'] = $busqueda; }

        $this->load->template_backend('email_notificacion/emails', $data);  
    }

    private function filtrar_emails($page,$url,$unpaged=false)
    {
        $busqueda['nombre']= ''; 
        $busqueda['email']= ''; 
        if(!isset($_GET['clear']))
        {
            if(count($this->input->get()) > 0)
            {
            	if ($this->input->get('nombre') != '' )
            	{
            		$busqueda['nombre']= $this->input->get('nombre'); 
            	}

            	if ($this->input->get('email') != '' ) 
            	{
        			$busqueda['email']= $this->input->get('email');
            	}

                if($this->input->get('id_municipio', true) != '')
                { 
                    $busqueda['id_municipio'] = $this->input->get('id_municipio', true); 
                }

                $filtros = '?'.implode('&', array_map(function ($v, $k) { return sprintf("%s=%s", $k, $v); }, $busqueda, array_keys($busqueda)));

                if($busqueda['nombre'] != '' || $busqueda['email'] != '' || sizeof($busqueda) > 1 )
                {
                    $this->session->set_userdata('emails_filtro', array('filtro' => $filtros));
                    $this->session->set_userdata('emails_busqueda', array('busqueda' => $busqueda));
                }
            } 
        }
        else
        {
            $this->session->unset_userdata('emails_filtro');
            $this->session->unset_userdata('emails_busqueda');
            redirect(current_url());
            break;
        }

        $filtros = $this->session->userdata('emails_filtro')['filtro']; 
        $busqueda = $this->session->userdata('emails_busqueda')['busqueda'];
        $rows = $this->em->getRepository('Entity\NotificacionEmail')->emailsGetBusqueda($busqueda);

        if(!$unpaged)
        {
            $config = init_pagination($url,count($rows), 20, 4,$filtros);
            $data['emails'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
            $data["segmento"] = $page;
            $data["p_links"] = $this->pagination->create_links();

            $data['municipios'] = colleccion_listado($this->em->getRepository('Entity\Municipio')->findAll());
            $data['municipios'][''] = 'Municipio';
        array_unshift ($data['municipios'] , 'OTROS');
        }
        else
        {
            $data['emails'] = $rows;
        }
        return array($data,$busqueda);
    }

    public function nuevo()
    {
        $data['volver'] = 'backend/email_notificacion/listar';
        $data['municipios'] = colleccion_listado($this->em->getRepository('Entity\Municipio')->findAll());
        $data['municipios'][''] = 'Seleccione un Municipio';
        array_unshift ($data['municipios'] , 'OTROS');

        $this->load->template_backend('backend/email_notificacion/nuevo',$data);
    }

    public function nuevo_guardar()
    {
        if($this->form_validation->run($this, 'email_notificacion'))
        {
            $email_notificacion = new Entity\NotificacionEmail;
            $email_notificacion->set_nombre($this->input->post('nombre', true));
            $email_notificacion->set_email($this->input->post('email', true));
            if ($this->input->post('id_municipio', true) != 0) 
            {
                $municipio = $this->em->getRepository('Entity\Municipio')->find($this->input->post('id_municipio', true));
                $email_notificacion->set_municipio($municipio);
            }

            $this->em->persist($email_notificacion);
            $this->em->flush();
            
            $this->session->set_flashdata('noticia','Email creado exitosamente.');
            redirect('backend/email_notificacion/listar');
        }
        else
        {
            $this->nuevo();
        }
    }

    public function editar($id)
    {
        $data['notificacion_email'] = $this->em->getRepository('Entity\NotificacionEmail')->find($id);
        $data['municipios'] = colleccion_listado($this->em->getRepository('Entity\Municipio')->findAll());
        $data['municipios'][''] = 'Seleccione un Municipio';
        array_unshift ($data['municipios'] , 'OTROS');
        $data['volver'] = 'backend/email_notificacion/listar';

        $this->load->template_backend('backend/email_notificacion/editar',$data);
    }

    public function editar_guardar($id)
    {
        if($this->form_validation->run($this, 'email_notificacion'))
        {
            $email_notificacion = $this->em->getRepository('Entity\NotificacionEmail')->find($id);
            $email_notificacion->set_nombre($this->input->post('nombre', true));
            $email_notificacion->set_email($this->input->post('email', true));

            $email_notificacion->desvincular_municipio();
            if ($this->input->post('id_municipio', true) != 0) 
            {
                $municipio = $this->em->getRepository('Entity\Municipio')->find($this->input->post('id_municipio', true));
                $email_notificacion->set_municipio($municipio);
            }

            $this->em->persist($email_notificacion);
            $this->em->flush();
            
            $this->session->set_flashdata('noticia','Email editado exitosamente.');
            redirect('backend/email_notificacion/listar');
        }
        else
        {
            $this->nuevo();
        }
    }

    public function borrar($id)
    {
        $email_notificacion = $this->em->getRepository('Entity\NotificacionEmail')->find($id);    
        
        $this->em->remove($email_notificacion);
        $this->em->flush();         

        $this->session->set_flashdata('noticia', "Email eliminado exitosamente.");
        redirect('backend/email_notificacion/listar');
    }
}