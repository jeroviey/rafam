<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Pregunta extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->preguntas();     
    }

    public function preguntas($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Pregunta')->findAll();
        $config = init_pagination('backend/pregunta/preguntas/',count($rows));
        $this->pagination->initialize($config);
        $data["p_links"] = $this->pagination->create_links();
        $data['preguntas'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('pregunta/lista', $data);     
    }    

    public function nuevo()
    {
        $data['volver'] = 'backend/pregunta';
        $data['pregunta'] = new Entity\Pregunta;
        $data['temas'] = $this->em->getRepository('Entity\Tema')->findAll();
    	$this->load->template_backend('pregunta/nuevo', $data);
    }

    public function nuevo_guardar()
    {
        if ($this->form_validation->run($this, 'pregunta'))
    	{
            $pregunta = new Entity\Pregunta;
    		$this->cargar($pregunta);

    		$this->session->set_flashdata('noticia', 'La pregunta ha sido creada correctamente');
            
            redirect('backend/pregunta');
    	}
    	else
    	{
    		$this->nuevo();
    	}
    }

    public function editar($id_pregunta)
    {
        $data['volver'] = 'backend/pregunta';
    	$data['pregunta'] = $this->em->getRepository('Entity\Pregunta')->find($id_pregunta);
        $data['temas'] = $this->em->getRepository('Entity\Tema')->findAll();
    	$this->load->template_backend('pregunta/editar', $data);

    }

    public function editar_guardar($id_pregunta)
    {
        if ($this->form_validation->run($this, 'pregunta'))
    	{
    		$faq = $this->em->getRepository('Entity\Pregunta')->find($id_pregunta);
    		$this->cargar($faq);

    		$this->session->set_flashdata('noticia', 'La pregunta ha sido editado correctamente');
            
            redirect('backend/pregunta');
    	}
    	else
    	{
    		$this->editar($id_pregunta);
    	}
    }

    public function cargar($pregunta)
    {
        $faq = $this->input->post('pregunta', true);
        $pregunta->set_pregunta($faq['pregunta']);
    	$pregunta->set_respuesta($faq['respuesta']);
        $tema = $this->em->getRepository('Entity\Tema')->find($faq['tema']);
        if($tema){
            $pregunta->set_tema($tema);
        }

		$this->em->persist($pregunta);
        $this->em->flush();
    }

    public function borrar($id_pregunta)
    {
    	$faq = $this->em->getRepository('Entity\Pregunta')->find($id_pregunta);

        $this->em->remove($faq);
        $this->em->flush();            

        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente la pregunta");
        redirect('backend/pregunta');
    }    
}