<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Titulo extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }
        
    public function listar($id)
    {
    	$data['titulo'] = $this->em->getRepository('Entity\Titulo')->find($id);
        $this->load->template_backend('titulo/listar', $data);
    }

    public function editar($id)
    {
    	$data['titulo'] = $this->em->getRepository('Entity\Titulo')->find($id);
        $this->load->template_backend('titulo/editar', $data);
    }

    public function actualizar($id)
    {
    	if ($this->form_validation->run($this, 'titulo'))
    	{
    		$titulo = $this->em->getRepository('Entity\Titulo')->find($id);
    		$titulo->set_nombre($this->input->post('nombre', true));
    		$this->em->persist($titulo);
        
        	$this->em->flush();
   			$this->session->set_flashdata('noticia', 'El titulo ha sido actulizado correctamente');
            
            redirect('backend/titulo/listar/'.$id);
    	}
    	else
    	{
    		$this->editar($id);
    	}
    }
    
}