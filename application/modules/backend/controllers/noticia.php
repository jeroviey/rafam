<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Noticia extends MY_Controller
{
    
    function __construct()
	{
	    parent::__construct();
	}

	public function index($page = 1)
	{
		$datos['titulo'] = $this->em->getRepository('Entity\Titulo')->findOneBy(array('clave' => 'noticias'));
		$rows = $this->em->getRepository('Entity\Noticia')->findBy(array(),array('created_at' => 'DESC'));
        $config = init_pagination("backend/noticia/index",count($rows), 10, 4);
        $datos['noticias'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $datos['usuario_logeado'] = true;
        $datos["links"] = $this->pagination->create_links();
		$this->load->template_backend('noticia/index', $datos);
	}
    public function nueva()
    {
    	$datos['volver'] = "backend/noticia";
    	$datos['noticia'] = new Entity\Noticia;
    	$this->load->template_backend('noticia/nueva', $datos);
    }


	public function guardar()
	{
		if($this->form_validation->run($this, 'noticia') == TRUE)
		{
			$noticia = new Entity\Noticia;
			$this->set_noticia($noticia);
			$this->em->persist($noticia);
			$this->em->flush();
			$this->session->set_flashdata('noticia','La noticia ha sido cargada correctamente');
			redirect('backend/noticia/');

		}else
		{
			$this->nueva();
		}
	}

	public function editar($id)
	{
		$datos['volver'] = "backend/noticia";
		$datos['noticia'] = $this->em->find('Entity\Noticia', $id);
        $this->load->template_backend('noticia/editar',$datos);
	}
	
	public function actualizar($id)
	{
		if($this->form_validation->run($this, 'noticia') == TRUE)
		{
			$noticia = $this->em->find('Entity\Noticia', $id);
			$this->set_noticia($noticia);
			$this->em->flush();
			$this->session->set_flashdata('noticia','La noticia ha sido actualizada correctamente');
			redirect('backend/noticia/');

		}else
		{
			$this->nueva();
		}
	}

	public function borrar($id)
	{
		$noticia = $this->em->find('Entity\Noticia', $id);
		$this->em->remove($noticia);
		$this->em->flush();
		$this->session->set_flashdata('noticia','La noticia ha sido borrada correctamente');
		redirect('backend/noticia/');

	}

    public function detalles($id)
    {
        $datos['noticia'] = $this->em->find('Entity\Noticia', $id);
        $this->load->template_backend('noticia/detalles',$datos);           
    }

    private function set_noticia($noticia)
    {
		$noticia->set_titulo($this->input->post('titulo', true));
		$noticia->set_texto($this->input->post('texto', true));
    } 	
}