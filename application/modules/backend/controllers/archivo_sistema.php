<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Archivo_sistema extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;          
    }

    public function index()   
    {
        $this->archivos_sistema();
    }

    public function archivos_sistema($page = 1)
    {
        $rows = $this->em->getRepository('Entity\ArchivoSistema')->findAll();
        $config = init_pagination('backend/archivo_sistema/archivos/',count($rows));
        $this->pagination->initialize($config); 

        $data["p_links"] = $this->pagination->create_links();
    	$data['archivos'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('archivo_sistema/archivos', $data);
    }
    
    public function nuevo()
    {
        $data['volver'] = 'backend/archivo_sistema';
        $data['modulos'] = colleccion_listado($this->em->getRepository('Entity\Modulo')->findAll());
        $data['archivo'] = new Entity\ArchivoSistema;
    	$this->load->template_backend('archivo_sistema/nuevo',$data);
    }

    public function nuevo_guardar()
    {
        if($this->form_validation->run($this, 'archivo_sistema'))
        {
            if (is_uploaded_file($_FILES['archivo']['tmp_name'])) 
            {
                $archivo = new Entity\ArchivoSistema;
                $archivo = $this->cargar_zip($archivo);

                $archivo->set_nombre($this->input->post('nombre', true));

                $this->em->persist($archivo);
                $this->em->flush();
                
                $this->session->set_flashdata('noticia','Archivo creado correctamente.');
                redirect('backend/archivo_sistema');
            }
            else
            {
                $this->session->set_flashdata('error','No ha ingresado ningun Archivo ZIP, no se pudo crear la nueva actualización.');
                redirect('backend/archivo_sistema/nuevo');
            }
        }
        else
        {
            $this->nuevo();
        }
    }

    public function cargar_zip($archivo)
    {
        $config['upload_path'] = $this->config->item('path_archivos_sistema');
        $config['allowed_types'] = 'rar|zip';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('archivo'))
        {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('backend/archivo_sistema/nuevo');
        }
        else
        {
            $archivo->set_archivo(($this->upload->data()['orig_name']));
            return $archivo;
        }
    }

    public function borrar($id)
    {
        $archivo = $this->em->getRepository('Entity\ArchivoSistema')->find($id);

        $path = $this->config->item('path_archivos_sistema').$archivo->get_archivo();
        eliminar_archivo($path);              
        $this->em->remove($archivo);
        $this->em->flush();         

        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente el archivo del sistema.");
        redirect('backend/archivo_sistema');
    }

}