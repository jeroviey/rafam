<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Modulo extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->em;       
    }
        
    public function index()
    {
        $this->modulos();
    }

    public function modulos($page = 1)
    {
        $rows = $this->em->getRepository('Entity\Modulo')->findAll();
        $config = init_pagination('backend/modulo/modulos/',count($rows));
        $this->pagination->initialize($config);
        $data["p_links"] = $this->pagination->create_links();
    	$data['modulos'] = array_slice($rows,($page-1)*$config['per_page'],$config['per_page']);
        $this->load->template_backend('modulo/modulos', $data);     
    }

    public function nuevo()
    {
        $data['volver'] = 'backend/modulo';
    	$this->load->template_backend('modulo/nuevo', $data);
    }

    public function nuevo_guardar()
    {
    	if ($this->form_validation->run($this, 'modulo'))
    	{
    		$modulo = new Entity\Modulo;
    		$this->actualizar($modulo);

    		$this->session->set_flashdata('noticia', 'El modulo ha sido creado correctamente');
            
            redirect('backend/modulo');
    	}
    	else
    	{
    		$this->nuevo();
    	}
    }

    public function editar($id_modulo)
    {
        $data['volver'] = 'backend/modulo';
    	$data['modulo'] = $this->em->getRepository('Entity\Modulo')->find($id_modulo);
    	$this->load->template_backend('modulo/editar', $data);

    }

    public function editar_guardar($id_modulo)
    { 	
    	if ($this->form_validation->run($this, 'modulo'))
    	{
    		$modulo = $this->em->getRepository('Entity\Modulo')->find($id_modulo);
    		$this->actualizar($modulo);

    		$this->session->set_flashdata('noticia', 'El modulo ha sido editado correctamente');
            
            redirect('backend/modulo');
    	}
    	else
    	{
    		$this->editar($id_modulo);
    	}
    }

    public function actualizar($modulo)
    {
    	$modulo->set_nombre($this->input->post('nombre', true));

		$this->em->persist($modulo);
        $this->em->flush();
    }

    public function borrar($id_modulo)
    {
    	$modulo = $this->em->getRepository('Entity\Modulo')->find($id_modulo);

        $this->em->remove($modulo);
        $this->em->flush();            

        $this->session->set_flashdata('noticia', "Se ha eliminado correctamente el modulo: ".$modulo->get_nombre());
        redirect('backend/modulo');
    }    
}