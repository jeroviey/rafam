<?php

$config = array(
                'usuario' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'apellido',
                                        'label' => 'Apellido',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'email',
                                        'label' => 'Email',
                                        'rules' => 'required|min_length[8]|valid_email'
                                     ),      
                                array(
                                        'field' => 'username',
                                        'label' => 'Nombre de Usuario',
                                        'rules' => 'required|min_length[5]|max_length[20]|alpha_dash|is_unique[usuario.username]'
                                     ),                    
                                ),

                'usuario_editar' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'apellido',
                                        'label' => 'Apellido',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'email',
                                        'label' => 'Email',
                                        'rules' => 'required|min_length[8]|valid_email'
                                     )                    
                                ),

                'cambiar_contra' => array(
                                    array(  
                                            'field' => 'oldpassword',
                                            'label' => 'Contraseña actual',
                                            'rules' => 'required|callback_match_pass' 
                                        ),
                                    array(  
                                            'field' => 'password',
                                            'label' => 'Contraseña',
                                            'rules' => 'required|min_length[5]|max_length[20]|alpha_dash|matches[passconf]' 
                                        ),
                                    array(  
                                            'field' => 'passconf',
                                            'label' => 'Confirmar contraseña',
                                            'rules' => 'required' 
                                        ),
                                    ),
    
                'modulo' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),                            
                                ),

                'curso' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'clase',
                                        'label' => 'Clase',
                                        'rules' => 'callback_clases_check'
                                     ),
                                array(
                                        'field' => 'profesor',
                                        'label' => 'Profesor',
                                        'rules' => 'callback_profesor_check'
                                     ),
                                array(
                                        'field' => 'archivo_id',
                                        'label' => 'Contenido',
                                        'rules' => 'callback_archivo_check'
                                     ),
                                ),

                'inscripcion' => array(
                                array(
                                        'field' => 'inscripcion[curso]',
                                        'label' => 'Curso',
                                        'rules' => 'is_natural_no_zero'
                                     ),
                                array(
                                        'field' => 'inscripcion[clase]',
                                        'label' => 'Clase',
                                        'rules' => 'is_natural_no_zero'
                                     ),
                                array(
                                        'field' => 'inscripcion[nombre]',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[apellido]',
                                        'label' => 'Apellido',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[dni]',
                                        'label' => 'DNI',
                                        'rules' => 'required|numeric|min_length[7]|max_length[8]|callback_dni_check[inscripcion]'
                                     ),
                                array(
                                        'field' => 'inscripcion[email]',
                                        'label' => 'e-mail',
                                        'rules' => 'required|valid_email'
                                     ),
                                array(
                                        'field' => 'inscripcion[telefono]',
                                        'label' => 'teléfono',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[cargo]',
                                        'label' => 'Cargo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[oficina]',
                                        'label' => 'Oficina',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[tipo_trabajo]',
                                        'label' => 'Tipo trabajo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'inscripcion[municipio]',
                                        'label' => 'Municipio',
                                        'rules' => 'callback_municipio_check[inscripcion]'
                                     ),
                                array(
                                        'field' => 'inscripcion[lugar_trabajo]',
                                        'label' => 'Lugar de trabajo',
                                        'rules' => 'callback_lugar_trabajo_check[inscripcion]'
                                     ),
                                ),
                'inscripcion_edicion' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'apellido',
                                        'label' => 'Apellido',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'dni',
                                        'label' => 'DNI',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'email',
                                        'label' => 'e-mail',
                                        'rules' => 'required|valid_email'
                                     ),
                                array(
                                        'field' => 'telefono',
                                        'label' => 'teléfono',
                                        'rules' => 'required'
                                     ),
                                ),

                'noticia' => array(
                                array(
                                        'field' => 'titulo',
                                        'label' => 'Titulo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'texto',
                                        'label' => 'Texto',
                                        'rules' => 'required'
                                     ),                         
                                ),

                'actualizacion' => array(
                                array(
                                        'field' => 'modulo_id',
                                        'label' => 'Modulo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'tipo',
                                        'label' => 'Tipo',
                                        'rules' => 'required'
                                     ),                         
                                array(
                                        'field' => 'version',
                                        'label' => 'Version',
                                        'rules' => 'required'
                                    ),
                                ),

                'archivo_sistema' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                    ),
                                ),

                'pregunta' => array(
                                array(
                                        'field' => 'pregunta[pregunta]',
                                        'label' => 'Pregunta',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'pregunta[respuesta]',
                                        'label' => 'Respuesta',
                                        'rules' => 'required'
                                     ),
                                ),

                'tema' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Tema',
                                        'rules' => 'required'
                                     ),
                                ),

                'contacto' => array(
                                array(
                                        'field' => 'contacto[nombre]',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'contacto[email]',
                                        'label' => 'e-mail',
                                        'rules' => 'required|valid_email'
                                     ),
                                array(
                                        'field' => 'contacto[cargo]',
                                        'label' => 'Cargo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'contacto[oficina]',
                                        'label' => 'Oficina',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'contacto[tipo_trabajo]',
                                        'label' => 'Tipo trabajo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'contacto[municipio]',
                                        'label' => 'Municipio',
                                        'rules' => 'callback_municipio_check[contacto]'
                                     ),
                                array(
                                        'field' => 'contacto[lugar_trabajo]',
                                        'label' => 'Lugar de trabajo',
                                        'rules' => 'callback_lugar_trabajo_check[contacto]'
                                     ),
                                array(
                                        'field' => 'contacto[asunto]',
                                        'label' => 'Asunto',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'contacto[mensaje]',
                                        'label' => 'Mensaje',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'g-recaptcha-response',
                                        'label' => 'Captcha',
                                        'rules' => 'required|callback_valid_captcha'
                                        )
                                ),

                'documentacion' => array(
                                array(
                                        'field' => 'tipo_documentacion_id',
                                        'label' => 'Tipo',
                                        'rules' => 'required'
                                     ),
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),                         
                                ),

                'tipo_documentacion' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required|is_unique[tipo_documentacion.nombre]'
                                     ),                         
                                ),
                'titulo' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required'
                                     ),                         
                                ),
                'contenido' => array(
                                array(
                                        'field' => 'titulo',
                                        'label' => 'Titulo',
                                        'rules' => 'required'
                                     ), 
                                array(
                                        'field' => 'descripcion',
                                        'label' => 'Descripcion',
                                        'rules' => 'required'
                                     ),
                                ),
                'profesor' => array(
                                array(
                                        'field' => 'profesor[nombre]',
                                        'label' => 'Nombre',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'profesor[apellido]',
                                        'label' => 'Apellido',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'profesor[email]',
                                        'label' => 'Email',
                                        'rules' => 'required|min_length[8]'
                                     ),                          
                                ),
                'email_notificacion' => array(
                                array(
                                        'field' => 'nombre',
                                        'label' => 'Nombre',
                                        'rules' => 'required|min_length[3]'
                                     ),
                                array(
                                        'field' => 'email',
                                        'label' => 'Email',
                                        'rules' => 'required|min_length[8]|valid_email'
                                     ),                 
                                array(
                                        'field' => 'id_municipio',
                                        'label' => 'Municipio',
                                        'rules' => 'required'
                                     ),
                                ),
);

?>