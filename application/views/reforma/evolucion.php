<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_organigrama.png">
				</div>
			</div>

			<div class="HB_contenido">
				<?php foreach ($titulo->get_contenidos() as $contenido) { ?>
					<p><strong><?= $contenido->get_titulo() ?></strong></p>
					<p align="justify"> <?= $contenido->get_descripcion() ?> </p>
					<br>
				<?php } ?>
			</div>

			<div class="HB_contenido">
				<div class="col-xs-8 col-xs-offset-2">
					<?php if($this->session->userdata('session_usuario')){ ?>
						<a id="lapiz_editar" class="gris_BA pull-right" title="Cambiar imagen" href="<?=base_url('backend/imagen/editar/'.$imagen->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
					<?php } ?>
 					<img src="<?= $imagen->get_ubicacion() ?>" class="imagen_ajustable">
			</div>

		</div>
	</div>
</section>