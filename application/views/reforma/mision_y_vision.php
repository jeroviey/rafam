<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo_mision->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_mision->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_txt.png">
				</div>
			</div>

			<div class="HB_contenido">
				<div class="col-md-5 col-xs-12">
					<?php foreach ($titulo_mision->get_contenidos() as $contenido) { ?>
						<?= $contenido->get_descripcion() ?>
					<?php } ?>
				</div>

				<div class="col-md-4 col-md-offset-1 col-xs-8 col-xs-offset-2">
					<?php if($this->session->userdata('session_usuario')){ ?>
						<a id="lapiz_editar" class="gris_BA pull-right" title="Cambiar imagen" href="<?=base_url('backend/imagen/editar/'.$imagen->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
					<?php } ?>
 					<img src="<?= $imagen->get_ubicacion() ?>" class="imagen_ajustable">	
				</div>
			</div>

			<div class="clearfix"></div>



			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo_vision->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_vision->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>
<!-- 
				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_txt.png">
				</div> -->
			</div>

			<div class="HB_contenido">
				<?php foreach ($titulo_vision->get_contenidos() as $contenido) { ?>
						<?= $contenido->get_descripcion() ?>
					<?php } ?>
			</div>

		</div>
	</div>
</section>