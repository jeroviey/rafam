<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">


			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>Mesa de ayuda</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_mesadeayuda.png">
				</div>
			</div>

			<div class="HB_contenido">
				<div class="col-xs-12 col-sm-6">
 					<img src="<?=base_url()?>assets/imagenes/mesa_de_ayuda_cuadrada.jpg" class="img-thumbnail">
				</div>

				<?php $this->load->view('mesa_ayuda/formulario_contacto.php'); ?>

		</div>
	</div>
</section>