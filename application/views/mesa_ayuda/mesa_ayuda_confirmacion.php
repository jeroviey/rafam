<style>
#icon_menu_secciones {font-size: 4em; padding: 5px 0 0 0; text-align: center !important; margin: 0 auto}
</style>
<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">


			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>Mesa de ayuda</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_mesadeayuda.png">
				</div>
			</div>
			<div class="HB_contenido">
				<div class="col-sm-6 col-xs-12">
 					<img src="<?=base_url()?>assets/imagenes/1377963_30321632.jpg" class="imagen_ajustable">
				</div>
				
				<div class="col-sm-4 col-sm-offset-1 col-xs-12 form_mesa_de_ayuda">
					<center>
					<span class="glyphicon glyphicon-ok" id="icon_menu_secciones"></span>
					<br>
					<br>
					<h3 class="azul_rafam">SU MENSAJE SE ENVIÓ CORRECTAMENTE</h3>
					<br>
					<h3>Pronto se pondrán en contacto a través de la cuenta de e-mail indicada</h3>
					</center>
				</div>
		</div>
	</div>
</section>