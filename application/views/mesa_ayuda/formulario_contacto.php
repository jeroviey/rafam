<script src="<?=base_url();?>assets/js/inscripcion.js"></script>
<?php echo form_open("mesa_ayuda/procesar_contacto");?>
	<div class="col-sm-6 col-xs-12 form_mesa_de_ayuda">

		<label for="nombre">
			<input 
				class="col-xs-12"
				type="text" 
				id="nombre" 
				name="contacto[nombre]" 
		    	value="<?php echo $contacto->get_nombre() ?>" 
		    	placeholder="Nombre"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[nombre]',"<p class='bg-danger'>","</p>"); ?>

		<label for="apellido">
			<input
				class="col-xs-12"
				type="text" 
				id="apellido" 
				name="contacto[apellido]" 
		    	value="<?php echo $contacto->get_apellido() ?>" 
		    	placeholder="Apellido"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[apellido]',"<p class='bg-danger'>","</p>"); ?>

		<div class="radio">
			<label class="radio-inline col-xs-5">
				<input type="radio" name="contacto[tipo_trabajo]" id="tipo_trabajo_1" value="1" <?= ("1" == $tipo_trabajo_selected) ? 'checked' : '' ?> > 
				Municipio
			</label>
			<label class="radio-inline col-xs-5">
				<input type="radio" name="contacto[tipo_trabajo]" id="tipo_trabajo_2" value="2" class="pull-left" <?= ("2" == $tipo_trabajo_selected) ? 'checked' : '' ?>> 
				Otro
			</label>
			<?php echo form_error('contacto[tipo_trabajo]',"<p class='bg-danger'>","</p>"); ?>
		</div>
		

		<div id="campo_municipio" style="<?php echo ("1" == $tipo_trabajo_selected) ? 'display:block' : 'display:none' ?>">
			<label for="municipio">
				<select name="contacto[municipio]" id="municipio" class="form-control">
					<option value="0" > Seleccione un municipio </option>
					<?php foreach($municipios as $municipio): ?>
						<option value="<?= $municipio->get_id() ?>" <?php echo ($municipio->get_id() == $municipio_selected) ? 'selected' : '' ?>> <?= $municipio->get_nombre() ?> </option>
					<?php endforeach ?>
				</select>
			</label>
			<?php echo form_error('contacto[municipio]',"<p class='bg-danger'>","</p>"); ?>
		</div> 

		<div id="campo_lugar_trabajo" style="<?php echo ("2" == $tipo_trabajo_selected) ? 'display:block' : 'display:none' ?>">
			<label for="lugar_trabajo">
				<input 
					class="col-xs-12"
					type="text" 
					id="lugar_trabajo" 
					name="contacto[lugar_trabajo]" 
			    	value="<?php echo $contacto->get_lugar_trabajo() ?>" 
			    	placeholder="Lugar de trabajo"
			    	autocomplete="off"
				/>
			</label>
			<?php echo form_error('contacto[lugar_trabajo]',"<p class='bg-danger'>","</p>"); ?>
		</div>

		<label for="oficina">
			<input 
				class="col-xs-12"
				type="text" 
				id="oficina" 
				name="contacto[oficina]" 
		    	value="<?php echo $contacto->get_oficina() ?>" 
		    	placeholder="Oficina"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[oficina]',"<p class='bg-danger'>","</p>"); ?>

		<label for="cargo">
			<input 
				class="col-xs-12"
				type="text" 
				id="cargo" 
				name="contacto[cargo]" 
		    	value="<?php echo $contacto->get_cargo() ?>" 
		    	placeholder="Cargo"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[cargo]',"<p class='bg-danger'>","</p>"); ?>

		<label for="email">
			<input
				class="col-xs-12"
				type="text" 
				id="email" 
				name="contacto[email]" 
		    	value="<?php echo $contacto->get_email() ?>" 
		    	placeholder="e-mail"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[email]',"<p class='bg-danger'>","</p>"); ?>

		<label for="asunto">
			<input 
				class="col-xs-12"
				type="text" 
				id="asunto" 
				name="contacto[asunto]" 
		    	value="<?php echo $contacto->get_asunto() ?>" 
		    	placeholder="Asunto"
		    	autocomplete="off"
			/>
		</label>
		<?php echo form_error('contacto[cargo]',"<p class='bg-danger'>","</p>"); ?>

		<label for="mensaje" >
			<textarea placeholder="Mensaje" name="contacto[mensaje]" id="mensaje"><?php echo $contacto->get_mensaje() ?></textarea>
		</label>
		<?php echo form_error('contacto[mensaje]',"<p class='bg-danger'>","</p>"); ?>

		<!-- Esta linea carga el template de capta-->
        <?php $this->load->view('plantillas/captcha');?>

	</div>


	<button
		type="submit" 
		class="btn btn-primary pull-right">
		Enviar mensaje
	</button>
<?php echo form_close();?>	  