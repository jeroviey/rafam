<section>
    <div class="container">
        <div class="hoja_blanca col-xs-10 col-xs-offset-1">
            <div class="HB_encabezado">
                <div class="col-xs-10">
                    <h3><?php echo $noticia->get_titulo() ?></h3>
                </div>
                <div class="col-xs-2">
                    <?php echo $noticia->get_fecha() ?></span>
                </div>
            </div>
            <div class="HB_contenido">
    	       	<?php echo $noticia ?>
            </div>
        </div>
    </div>
</section>
