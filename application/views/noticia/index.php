<script type="text/javascript" src="<?=base_url();?>assets/js/texto-noticia.js"></script>
<section>
	<div class="container">
		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
			    <div class="col-xs-10">
			        <h3>
			        	<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
			        </h3>
			    </div>

			    <div class="col-xs-2">
			        <span class="glyphicon glyphicon-list-alt HB_circulo"></span>
			    </div>
			</div>
			<div class="HB_contenido">
                <?php $this->load->view('noticia/noticias', $noticias); ?>
                <?php if($usuario_logeado): ?>
                    <a class="boton pull-right" href="<?=base_url()?>backend/noticia/nueva/"><h5 class="derecha">NUEVA NOTICIA</h5></a>
                <?php endif ?>
            </div>
            <?php echo $links ?>
            </div>
        </div>
	</div>
</section>