<div class="panel-group">
	<?php foreach ($noticias as $noticia) : ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="col-xs-10">
				<h4 class="panel-title"><strong><?php echo $noticia->get_titulo() ?></strong></h4>
			</div>
			<div class="col-xs-2 derecha">
				<?php echo $noticia->get_fecha(); ?>
			</div>
		</div>
		<div class="panel-body text-justify">
			<?php echo truncateHtml($noticia, 300);?>
			<div class="col-xs-2">
				<a href="<?php echo base_url('noticia/detalles/'. $noticia->get_id())?>" class="naranja">ver más</a>
			</div>
			<?php if($usuario_logeado): ?>
			<div class="col-xs-10 derecha">
			 	<a data-toggle="tooltip" data-placement="top" title="Editar" href="<?=base_url()?>backend/noticia/editar/<?= $noticia->get_id() ?>"><span class="glyphicon glyphicon-pencil gris_BA"></a>
                <a data-href="<?=base_url()?>backend/noticia/borrar/<?= $noticia->get_id() ?>" data-toggle="modal" data-target="#confirm-modal" href="#" data-confirm="¿Está seguro que desea borrar la noticia seleccionada?"><span class="glyphicon glyphicon-remove gris_BA"></a></h5>
			</div>
			<?php endif ?> 
		</div>
	</div>
	<?php endforeach ?>
</div>