<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_txt.png">
				</div>
			</div>

			<div class="HB_contenido">

				<?php foreach ($titulo->get_contenidos() as $contenido) { ?>
					<?=$contenido->get_descripcion()?>
				<?php } ?>

				<br>

             	<a class="boton pull-right" href="<?=base_url()?>assets/pdf/decreto/RAFAM_Decreto2980.pdf" target="_blank"><h5 class="derecha">DESCARGAR EL DECRETO COMPLETO</h5></a>

			<!-- <a id="link_sobre_blanco" href="<?=base_url()?>assets/pdf/decreto/RAFAM_Decreto2980.pdf" target="_blank"><span class="glyphicon glyphicon-file"></span>DESCARGAR EL DECRETO COMPLETO</a> -->

			</div>

		</div>
	</div>
</section>