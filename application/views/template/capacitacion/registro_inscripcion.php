<div>
	<div><a href="frontend/inicio/index" data-toggle="tooltip" data-placement="bottom" title="Inicio"><img src="logo_rafam_azul_sinfondo.png"></a></div>
	<br>
	<br>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;">Le comunicamos que usted se ha registrado correctamente para el curso:</h3>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong><?= $inscripcion->get_curso()->get_nombre() ?></strong></p>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;">que se dicta el</p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong><?= $inscripcion->get_clase()->get_dias() ?> de <?= $inscripcion->get_clase()->get_mes_texto() ?></strong></p>
	<p id="comun" style="color: #999; text-align: center; font-size: 1.2em;">en el Ministerio de Economía de la Provincia de Buenos Aires,</p>
	<p id="importante" style="color: #0B5EAA; text-align: center; font-size: 1.3em;"><strong><?= $inscripcion->get_clase()->get_lugar() ?> - Área de capacitación en el horario de 9:30 a 16.</strong></p>
	<p id="comun" style="color: #999; text-align: center; font-size: 2em;">A LA BREVEDAD SE LE ENVIARÁ UN MAIL INFORMANDOLE SI SU INSCRIPCIÓN FUE ACEPTADA O RECHAZADA.</p>
	<br>
	<div id="linea" style="border-top: 1px solid #adadad; padding-top: 20px;">
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;"><strong>Para más información comunicarse a:</strong></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Email: <a href="" id="email" style="color: #999; text-align: center; font-size: 1em;">cursosrafam@ec.gba.gov.ar</a></p> 
		<p id="chico" style="color: #999; text-align: center; font-size: 0.8em;">Teléfono:(0221) 429-4484 / 4509</p> 
	</div>
</div>