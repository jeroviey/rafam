<div>
	<div><a href="frontend/inicio/index" data-toggle="tooltip" data-placement="bottom" title="Inicio"><img src="logo_rafam_azul_sinfondo.png"></a></div>
	<br>
	<br>
	<p id="comun" style="color: #999; text-align: justify; font-size: 1.2em;">Nuevo mensajes mesa de ayuda:</h3>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><strong> De: </strong><?= $contacto->get_nombre().' '.$contacto->get_apellido().' ('.$contacto->get_email().')' ?></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><strong> Lugar de trabajo: </strong><?= ($contacto->get_municipio()) ? 'Municipio de '.$contacto->get_municipio()->get_nombre() : $contacto->get_lugar_trabajo(); ?></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><strong> Oficina: </strong><?= $contacto->get_oficina() ?></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><strong>ASUNTO:</strong></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><?= $contacto->get_asunto() ?></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><strong>MENSAJE:</strong></p>
	<p id="importante" style="color: #0B5EAA; text-align: justify; font-size: 1.3em;"><?= $contacto->get_mensaje() ?></p>
	<br>
</div>