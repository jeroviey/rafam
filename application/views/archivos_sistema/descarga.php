<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

 			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>DESCARGAS RAFAM</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_descarga_archivos_negro.png">
				</div>
			</div>

 			
			<div class="HB_contenido">
				
				<div class="col-xs-12 text-center">
					<a title="Descargar" href="<?=base_url()?>archivo_sistema/descargar_archivo/<?=$archivo->get_id()?>" data-toggle="tooltip" data-placement="top" id="" data-titulo="Descargando archivo"> 
						<h3 class="negro_BA">
							<span class="glyphicon glyphicon-download azul_rafam"></span> 
							<?= $archivo->get_nombre() ?>
						</h3>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>

<?= $this->load->view('plantillas/modal_barra_progreso'); ?>

<script src="<?=base_url();?>assets/js/barra_progreso.js" type="text/javascript"></script>