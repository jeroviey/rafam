<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

 			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_actualizaciones.png">
				</div>
			</div>

 			<?php foreach ($actualizaciones as $actualizacion) { ?>
				<div class="HB_contenido">
					<div class="col-md-4 col-xs-12" id="modulo">
						<h3><?= $actualizacion->get_modulo()->get_nombre() ?></h3>
					</div>

					<div class="cont_iconos_descarga col-xs-8">
						<div class="col-xs-3">
				            <a href="<?=base_url()?>actualizacion/descargar_zip/<?=$actualizacion->get_id()?>">
								<?php if ($actualizacion->get_tipo() == 'VERSION') { ?>
				                	<span class="glyphicon glyphicon-circle-arrow-down" id="icon_descarga_actualizaciones" data-toggle="tooltip" data-placement="top" title="Descargar la nueva versión">
								<?php }else{ ?>
				                	<span class="glyphicon glyphicon-circle-arrow-down" id="icon_descarga_actualizaciones" data-toggle="tooltip" data-placement="top" title="Descargar el nuevo release">
								<?php }?>
				            </a>
				            <h5 class="centrado"><?= $actualizacion->get_tipo() ?></h5>
				            <h3 class="centrado"><?= $actualizacion->get_version() ?></h3>
				        </div>
				        <?php foreach ($actualizacion->get_documentos() as $documento) {?>
					        <div class="col-xs-3" >
					            <a href="<?= base_url()?>actualizacion/ver_pdf/<?=$documento->get_id()?>" target="_blanck">
					                <span class="glyphicon glyphicon-file azul_rafam" id="icon_descarga_actualizaciones" data-toggle="tooltip" data-placement="top" title="Visualizar la documentación">
					            </a>
					                <h5 class="centrado"><?= $documento->get_nombre() ?></h5>
					        </div>
				        <?php } ?>
				    </div>
				</div>
				<div class="clearfix"></div>
 			<?php } ?>

            <a class="boton pull-right" href="<?=base_url('actualizacion/versiones_anteriores')?>"><h5 class="derecha">VERSIONES ANTERIORES</h5></a>

		</div>
	</div>
</section>