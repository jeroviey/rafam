<section>
	<div class="container">
 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">	
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_documentacion.png">
				</div>
			</div>

			<?php foreach ($tipos_documentaciones as $tipo => $documentaciones) { ?>
				<div class="col-xs-10">
					<h3><?=$tipo?></h3>
				</div>
				
				<div class="HB_contenido">
                    <?php if (!empty($documentaciones)) { ?>
                    	<?php foreach ($documentaciones as $documentacion){?>
							<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 pdf_documentacion" >
					            <a href="<?= base_url()?>software/ver_pdf/<?=$documentacion->get_id()?>" target="_blanck">
					                <span class="glyphicon glyphicon-file azul_rafam" id="icon_descarga_actualizaciones" data-toggle="tooltip" data-placement="top" title="Visualizar la documentación"></span>
					            </a>
					                <h4 class="centrado"><?=$documentacion->get_nombre() ?></h4>
					                <h6 class="centrado"><?= date_format($documentacion->get_fecha_subida(), 'd/m/Y') ?></h6>
					                <a class="pull-right" data-toggle="tooltip" data-placement="top" title="Descargar la documentación" href="<?= base_url()?>software/descargar_pdf/<?=$documentacion->get_id()?>"><span class="glyphicon glyphicon-download gris_BA"></span></a>
					        </div>
                        <?php } ?>
                    <?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
</section>