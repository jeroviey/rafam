<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_txt.png">
				</div>
			</div>


			<div class="HB_contenido">

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
 						<?php if($this->session->userdata('session_usuario')){ ?>
							<a id="lapiz_editar" class="gris_BA pull-right" title="Cambiar imagen" href="<?=base_url('backend/imagen/editar/'.$imagen->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
						<?php } ?>
	 					<img src="<?= $imagen->get_ubicacion() ?>" class="imagen_ajustable">
					</div>
				</div>
				<br>

				<?php foreach ($titulo->get_contenidos() as $contenido) {?>
					<?=$contenido->get_descripcion()?>
					<br>
				<?php } ?>


				<!-- <p>BREVE DESCRIPCIÓN FUNCIONAL DE CADA MÓDULO</p> -->
				<p class="azul_rafam">
					<?=$titulo_nucleo->get_nombre()?>
					<?php if($this->session->userdata('session_usuario')){ ?>
						<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_nucleo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
					<?php } ?>
				</p>

				<?php foreach ($titulo_nucleo->get_contenidos() as $contenido) {?>
					<p><strong><?=$contenido->get_titulo()?></strong></p>
					<?=$contenido->get_descripcion()?>
					<br>
				<?php } ?>
				<br>


				<p class="azul_rafam">
					<?=$titulo_ingresos_publicos->get_nombre()?>
					<?php if($this->session->userdata('session_usuario')){ ?>
						<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_ingresos_publicos->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
					<?php } ?>
				</p>
				<?php foreach ($titulo_ingresos_publicos->get_contenidos() as $contenido) {?>
					<?=$contenido->get_descripcion()?>
					<br>
				<?php } ?>
				<br>
			
				<p class="azul_rafam">
					<?=$titulo_personal->get_nombre()?>
					<?php if($this->session->userdata('session_usuario')){ ?>
						<a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_personal->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
					<?php } ?>
				</p>
				<?php foreach ($titulo_personal->get_contenidos() as $contenido) {?>
					<?=$contenido->get_descripcion()?>
					<br>
				<?php } ?>

			</div>

		</div>
	</div>
</section>