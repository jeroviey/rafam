<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>Recursos humanos</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_recursoshum.png">
				</div>
			</div>

			<div class="HB_contenido">
				<p align="justify">Es necesario que el Municipio designe un Coordinador RAFAM, responsable de impulsar la implantación del sistema informático, siendo a su vez un interlocutor válido con el personal técnico de la Dirección Provincial de Coordinación Económica.

Se deberán establecer que agentes municipales se harán cargo de cada módulo del Sistema RAFAM, recomendando un mínimo de 2 por cada uno.

Se deberá contar con personal idóneo para realizar las distintas tareas que los subsistemas requieran.
				</p>
			</div>



			<div class="HB_subtitulo">
				<div class="col-xs-10">
					<h3>Requerimientos técnicos</h3>
				</div>

                <div class="col-xs-2">
                    <img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_recursostec.png">
                </div>
			</div>

			<div class="HB_contenido">
				
				 <table class="tabla table-responsive table table-hover table-condensed">

                <thead>
                    <tr>
                        <th class="campos"> </td>
                        <th class="campos col_caract"> <strong>CARACTERISTICAS</strong> </th>
                        <th class="campos col_cant"> <strong>CANTIDAD</strong> </th>
                        <th class="campos col_obs"> <strong>OBSERVACIONES</strong> </th>
                    </tr>
                </thead>

                <tbody>
                        <tr>
                            <td class="azul_rafam derecha"> <strong>SERVIDOR DE APLICACIONES Y DE BASE DE DATOS</strong></td>
                            <td class="col_caract">
                                <h5><strong>Procesador:</strong> INTEL o AMD de 4 o más núcleos. <strong>Memoria:</strong> 4GB recomendado. 2GB mínimo
                                    <strong>Espacio libre:</strong> 100GB. 10GB mínimo. <strong>Arquitectura:</strong> 32 bits
                                    <strong>Sistema Operativo:</strong> Microsoft Windows Server 2003 o superior
                                    <strong>Base de datos:</strong> Oracle 10g o superior.
                                </h5>
							</td>

                            <td class="col_cant">
                                <h5>1 (ambiente de producción)</h5>
                            	<h5>1 (ambiente de pruebas)</h5>
                            </td>
                            <td class="col_obs">
                                <h5>Evaluar cada caso en particular para ajustar los requerimientos a las necesidades 
                                    puntuales tanto del ambiente de producción como del ambiente de pruebas. 
                                </h5>
                                    <br>
                                <h5>
                                    <strong>Nota:</strong> Disponer de un equipo para ambiente de prueba preferentemente portatil 
                                    (utilizado principalmente para capacitacion y pruebas de usuarios y de migración si existieran).
							     </h5>
                            </td>
                        </tr> 


                        <tr>
                            <td class="azul_rafam derecha"> <strong>ESTACIONES DE TRABAJO</strong></td>
                            <td class="col_caract">
                                <h5><strong>Procesador:</strong> INTEL o AMD de 4 o más núcleos.<strong> Memoria:</strong> 2GB recomendado. 1GB mínimo
                                    <strong>Espacio libre:</strong> 1GB. Arquitectura: 32 bits. <strong>Sistema Operativo:</strong> Microsoft Windows XP o Windows 7
                                </h5>
                            </td class="col_cant">
                            <td>
                                <h5>A determinar por el municipio.</h5>
                            </td>
                            <td class="col_obs"></td>
                        </tr> 

                        <tr>
                            <td class="azul_rafam derecha"> <strong>DISPOSITIVOS DE IMPRESIÓN</strong></td>
                            <td class="col_caract"><h5>Impresoras laser</h5></td>
                            <td class="col_cant"><h5>1 mínimo</h5></td>
                            <td class="col_obs"><h5>A determinar por el municipio.</h5></td>
                        </tr> 

                        <tr>
                            <td class="azul_rafam derecha"> <strong>DISPOSITIVOS ADICIONALES</strong></td>
                            <td class="col_caract"><h5>Cableado estructurado 100Mbits y de ser necesario, lectores de códigos de barra estándares</h5></td>
                            <td class="col_cant"></td>
                            <td class="col_obs"></td>
                        </tr> 

                        <tr>
                            <td class="azul_rafam derecha"> <strong>UNIDADES DE MANTENIMIENTO DE ENERGÍA (UPS)</strong></td>
                            <td class="col_caract"><h5>Estándares del mercado</h5></td>
                            <td class="col_cant"><h5>1</h5></td>
                            <td class="col_obs"></td>
                        </tr> 
                </tbody>

            </table>
				
			</div>

		</div>
	</div>
</section>