<!DOCTYPE html>
<html lang="ES">
<head profile="http://www.w3.org/2005/10/profile">
	<?php
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>RAFAM - Reforma de la Administración Financiera</title>

	<!-- P A R A   R E S P O N S I V E -->
	<meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1">

<!-- 	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
 -->	<!-- BOOTSTRAP -->
	<link href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- JS -->
    <script src="<?=base_url();?>assets/js/jquery-1.11.0.min.js"></script>
	<script src="<?=base_url();?>assets/js/modernizr-2.5.3.js"></script>
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.js"></script>
    <script src="<?=base_url();?>assets/js/menu_desplegable.js"></script>

    <!-- E S T I L O S (CSS) -->
    <link href="<?=base_url();?>assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets/css/fuentes.css" rel="stylesheet" type="text/css">

</head>

<div class="fondo">
	<?php if($error=$this->session->flashdata('error_login')){ ?>
		<div class="alert alert-danger fade in">
			<button class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<span class="glyphicon glyphicon-remove-sign"></span> <?= $error ?>
		</div>
	<?php } ?>
	<div class="col-sm-4 col-sm-offset-4 col-lg-2 col-lg-offset-5" >
		<a  title="Inicio" href="<?=base_url()?>" ><img id="loguito_login" src="<?=base_url()?>assets/imagenes/loguito_rafam_grande_sombra.png"></a>
	</div>
	<?php echo form_open('backend/login/iniciar_sesion');?>
	<div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-lg-2 col-lg-offset-5 marco_login">
		<div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				<input type="text" name="username" class="form-control" id="login_BE" placeholder="Usuario">
				<?php echo form_error('username',"<p class='bg-danger'>","</p>"); ?>
		</div>
		
		<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
			<input type="password" name="password" class="form-control" id="login_BE" placeholder="Contraseña">
			<?php echo form_error('password',"<p class='bg-danger'>","</p>"); ?>
		</div>
		<button	type="submit" class="btn btn-primary col-xs-12">INGRESAR</button>
	</div>
	<?php echo form_close();?>
	<div class="clearfix"></div>

	<div class="col-sm-4 col-sm-offset-4 col-lg-2 col-lg-offset-5">
		<hr>
	</div>

	<div class="col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4">
		<h3 class="blanco centrado"> Esta sección está destinada </h3>
		<h3 class="blanco centrado">a la administración del sitio.</h3>
	</div>
</div>