<section>
	<div class="container">
	 	<div class="row">
	 		<div class=" col-md-4 col-xs-12">
	 			<!-- chequear si va row -->
	 			<div class="col-xs-6 col-sm-3 col-md-6">
	 				<a href="<?=base_url('actualizacion')?>"><img src="<?=base_url()?>assets/imagenes/AD_actualizaciones_150.png" id="cien" alt="actualizaciones" class="img-thumbnail"></a>
	 			</div>
	 			<div class="col-xs-6 col-sm-3 col-md-6">
	 				<a href="<?=base_url('mesa_ayuda')?>"><img src="<?=base_url()?>assets/imagenes/AD_mesadeayuda_150.png" id="cien" alt="mesa de ayuda" class="img-thumbnail"></a>
	 			</div>
				<div class="col-xs-6 col-sm-3 col-md-6">
	 				<a href="https://twitter.com/rafam_ba" target="_blank"><img src="<?=base_url()?>assets/imagenes/AD_twitter_150.png" id="cien" alt="twitter" class="img-thumbnail"></a>
	 			</div>
	 			<div class="col-xs-6 col-sm-3 col-md-6">
	 				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
		      	 				<a href="<?=base_url('software/documentacion')?>"><img src="<?=base_url()?>assets/imagenes/AD_Documentacion_150.png" id="cien" alt="documentacion" class="img-thumbnail"></a>
						    </div>
						    <div class="item">
						      <a href="<?=base_url('capacitacion/cronograma')?>"><img src="<?=base_url()?>assets/imagenes/AD_Capacitacion_150.png" id="cien" alt="capacitacion" class="img-thumbnail"></a>
						    </div>
						  </div>
					</div>
	 			</div>
	 		</div>
	 		
	 		<!-- seccion noticias -->
	 		<div id="scroller" class="noticia col-md-6 col-xs-12">
	 			<?php $this->load->view('noticia/noticias'); ?>
	 		</div>
	 		
	 		<div class="col-md-2 col-xs-12">
	 			<div class="col-md-12 col-xs-6">
	 				<a href="http://www.simco.rafam.ec.gba.gov.ar" target="_blank"><img src="<?=base_url()?>assets/imagenes/AD_simco_150.png" id="cien" alt="mesa de ayuda" class="img-thumbnail"></a>
	 			</div>
	 			<div class="col-md-12 col-xs-6">
	 				<a href="<?=base_url('normativa')?>"><img src="<?=base_url()?>assets/imagenes/AD_Normativa_150.png" id="cien" alt="mesa de ayuda" class="img-thumbnail"></a>
				</div>
	 		</div>
	 	</div>
	</div>
</section>
