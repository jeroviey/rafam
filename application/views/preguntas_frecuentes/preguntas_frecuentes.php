<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">

			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>Preguntas frecuentes</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_mesadeayuda.png">
				</div>
			</div>

			<div class="HB_contenido">
				<?php foreach($temas as $tema): ?>
				<!-- GRUPO A -->
				<div class="panel-group" id="GRUPO_<?= $tema->get_id() ?>" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">

						<div class="panel-heading" role="tab" id="heading_A">
					    	<h4 class="panel-title">
						    	<a data-toggle="collapse" data-parent="#GRUPO_<?= $tema->get_id() ?>" href="#collapse_<?= $tema->get_id() ?>" aria-expanded="false" aria-controls="collapse_<?= $tema->get_id() ?>">
									<p>
										<strong><?= $tema->get_nombre() ?><span class="pull-right glyphicon glyphicon-chevron-down"></span></strong>

									</p>
								</a>
							 </h4>
						</div>

						<div id="collapse_<?= $tema->get_id() ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $tema->get_id() ?>">
					    	<div class="list-group">
								<!-- INTERIOR DE GRUPO A -->
			 					<div class="panel-group" id="accordion_<?= $tema->get_id() ?>" role="tablist" aria-multiselectable="true">  <!-- SI QUIERO QUE SE PUEDAN ABRIR VARIAS PREGUNTAS A LA VEZ DEBO COMENTAR ESTA LINEA, ES POR EL id="accordion" -->
			 						<?php $preguntas = $tema->get_preguntas_ordenadas() ?>
			 						<?php foreach($preguntas as $pregunta): ?>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading_<?= $tema->get_id() ?><?= $pregunta->get_id() ?>">
										    	<h4 class="panel-title">
											    	<a id="preguntas_frecuentes" data-toggle="collapse" data-parent="#accordion_<?= $tema->get_id() ?>" href="#collapse_<?= $tema->get_id() ?><?= $pregunta->get_id() ?>" aria-expanded="true" aria-controls="collapse_<?= $tema->get_id() ?><?= $pregunta->get_id() ?>">
														<?= $pregunta->get_pregunta() ?>
													</a>
												 </h4>
											</div>
											<div id="collapse_<?= $tema->get_id() ?><?= $pregunta->get_id() ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $tema->get_id() ?><?= $pregunta->get_id() ?>">
										      <div class="panel-body">
												<p><?= $pregunta->get_respuesta() ?></p>
										      </div>
										    </div>
										</div>
									<?php endforeach ?>
								 </div> 
								<!-- FIN INTERIOR DE GRUPO A -->
					      </div>
					    </div>
					</div>
				</div>
				<!-- FIN DE GRUPO A -->
				<?php endforeach ?>
				

			</div>

		</div>
	</div>
</section>