<style>
#icon_menu_secciones {font-size: 4em; padding: 5px 0 0 0; text-align: center !important; margin: 0 auto}
</style>

<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">


			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>Planilla de inscripción</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_capacitacion.png">
				</div>
			</div>

			<div class="HB_contenido">
				<div class="col-sm-6 col-xs-12">
 					<img src="<?=base_url()?>assets/imagenes/Spanish_class_students_medium.jpg" class="imagen_ajustable">
				</div>

				<div class="col-sm-4 col-sm-offset-1 col-xs-12 form_mesa_de_ayuda">
					<center>
					<span class="glyphicon glyphicon-ok" id="icon_menu_secciones"></span>
					<br>
					<br>
					<h3 class="azul_rafam">LA INSCRIPCIÓN SE REALIZÓ CORRECTAMENTE</h3>
					<br>
					<h3>Se le enviará un mensaje de confirmación a la cuenta de e-mail indicada</h3>
					</center>
				</div>

				<div class="clearfix"></div>

				<div class="HB_subtitulo">
					<div class="col-xs-10">
						<h3>Contacto</h3>
					</div>
				</div>

			<div class="HB_contenido">
				<p><strong>Email:</strong> cursosrafam@ec.gba.gov.ar</p>
				<p><strong>Teléfono:</strong>(0221) 429-4484 / 4509</p>			
			</div>


			<div class="HB_subtitulo">
				<div class="col-xs-10">
					<h3>Comunicado importante</h3>
				</div>
			</div>

			<div class="HB_contenido">
				<p>Por cuestiones de organización interna, les solicitamos por favor que tanto para las inscripciones 
					como para darse de baja en cualquiera de los cursos, avisen a esta Dirección con la mayor 
					anticipación posible. No asistir a los Cursos RAFAM sin la previa contestación de confirmación de 
					inscripción.</p>
				
			</div>
		</div>
	</div>
</section>