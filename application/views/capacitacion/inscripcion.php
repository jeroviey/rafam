<section>
	<div class="container">
 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">
			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
						<?=$titulo->get_nombre()?>
                        <?php if($this->session->userdata('session_usuario')){ ?>
                            <a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        <?php } ?>
					</h3>
				</div>

				<div class="col-xs-2">
					<img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_capacitacion.png">
				</div>
			</div>


			<div class="HB_contenido">
				<div class="col-sm-6 col-xs-12">
 					<img src="<?=base_url()?>assets/imagenes/1338212_30238506.jpg" class="imagen_ajustable">
				</div>
			
				<?php $this->load->view('capacitacion/formulario_inscripcion'); ?>
					
				<div class="clearfix"></div>

				<div class="col-sm-5 col-xs-12">
					<div class="HB_subtitulo">
						<div class="col-xs-10">
							<h3>
								<?=$titulo_importante->get_nombre()?>
		                        <?php if($this->session->userdata('session_usuario')){ ?>
		                            <a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_importante->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
		                        <?php } ?>
							</h3>
						</div>
					</div>

					<div class="HB_contenido">
						<?php foreach ($titulo_importante->get_contenidos() as $contenido) { ?>
		                    <?=$contenido->get_descripcion()?>
		                <?php } ?>
					</div>
				</div>

				<div class="col-sm-5 col-sm-offset-2 col-xs-12">
					<div class="HB_subtitulo">
						<div class="col-xs-10">
							<h3>
								<?=$titulo_contacto->get_nombre()?>
		                        <?php if($this->session->userdata('session_usuario')){ ?>
		                            <a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo_contacto->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
		                        <?php } ?>
							</h3>
						</div>
					</div>

					<div class="HB_contenido">
						<?php foreach ($titulo_contacto->get_contenidos() as $contenido) { ?>
		                    <?=$contenido->get_descripcion()?>
		                <?php } ?>		
					</div>
				</div>
			</div>


	</div>
</section>
			