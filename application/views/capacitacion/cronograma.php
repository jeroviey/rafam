<section>
	<div class="container">

 		<div class="hoja_blanca col-xs-10 col-xs-offset-1">


			<div class="HB_encabezado">
				<div class="col-xs-10">
					<h3>
                        <?=$titulo->get_nombre()?>
                        <?php if($this->session->userdata('session_usuario')){ ?>
                            <a id="lapiz_editar" class="gris_BA" title="Editar título" href="<?=base_url('backend/titulo/listar/'.$titulo->get_id())?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        <?php } ?>
                    </h3>
				</div>

				<div class="col-xs-2">
                    <img class="HB_circulo" src="<?=base_url()?>assets/imagenes/icon_capacitacion.png">
				</div>
			</div>


			<div class="HB_contenido">
			    <?php foreach ($titulo->get_contenidos() as $contenido) { ?>
                    <?=$contenido->get_descripcion()?>
                <?php } ?>	
                <br>


                <table class="tabla table-responsive table table-hover table-condensed">

                    <thead>
                        <tr>
                            <th class="campos"></th>
                            <th class="campos centrado"> <strong>ABR </strong> </td>
                            <th class="campos centrado"> <strong>MAY</strong> </th>
                            <th class="campos centrado"> <strong>JUN</strong> </th>
                            <th class="campos centrado"> <strong>JUL</strong> </th>
                            <th class="campos centrado"> <strong>AGO</strong> </th>
                            <th class="campos centrado"> <strong>SEPT</strong> </th>
                            <th class="campos centrado"> <strong>OCT</strong> </th>
                            <th class="campos centrado"> <strong>NOV</strong> </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach($cursos as $curso): ?>
                            <?php $meses = $cursoclases[$curso->get_id()]; ?>
                            <tr>
                                <td class="col-xs-4 azul_rafam"> <strong><?= $curso->get_nombre() ?></strong></td>
                                <td class="td_gris centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(4, $meses)) ? $meses[4] : '' ?></td>
                                <td class="centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(5, $meses)) ? $meses[5] : '' ?></td>
                                <td class="td_gris centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(6, $meses)) ? $meses[6] : '' ?></td>
                                <td class="centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(7, $meses)) ? $meses[7] : '' ?></td>
                                <td class="td_gris centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(8, $meses)) ? $meses[8] : '' ?></td>
                                <td class="centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(9, $meses)) ? $meses[9] : '' ?></td>
                                <td class="td_gris centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(10, $meses)) ? $meses[10] : '' ?></td>
                                <td class="centrado col-xs-1" id="centrado_vertical"><?= (array_key_exists(11, $meses)) ? $meses[11] : '' ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>

                <a class="boton pull-right" href="<?=base_url('capacitacion/inscripcion')?>"><h5 class="derecha">IR A PLANILLA DE INSCRIPCIÓN</h5></a>
    				
			</div>

		</div>
	</div>
</section>