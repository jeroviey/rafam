<script src="<?=base_url();?>assets/js/inscripcion.js"></script>
				<?php echo form_open("capacitacion/procesar_inscripcion");?>
				<div class="col-sm-6 col-xs-12 form_mesa_de_ayuda">

					<label for="nombre">
						<input 
							class="col-xs-12"
							type="text" 
							id="nombre" 
							name="inscripcion[nombre]" 
					    	value="<?php echo $inscripcion->get_nombre() ?>" 
					    	placeholder="Nombre"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[nombre]',"<p class='bg-danger'>","</p>"); ?>

					<label for="apellido">
						<input
							class="col-xs-12"
							type="text" 
							id="apellido" 
							name="inscripcion[apellido]" 
					    	value="<?php echo $inscripcion->get_apellido() ?>" 
					    	placeholder="Apellido"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[apellido]',"<p class='bg-danger'>","</p>"); ?>

					<label for="dni">
						<input
							class="col-xs-12"
							type="text" 
							id="dni" 
							name="inscripcion[dni]" 
					    	value="<?php echo $inscripcion->get_dni() ?>" 
					    	placeholder="DNI"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[dni]',"<p class='bg-danger'>","</p>"); ?>

					<label for="email">
						<input
							class="col-xs-12"
							type="text" 
							id="email" 
							name="inscripcion[email]" 
					    	value="<?php echo $inscripcion->get_email() ?>" 
					    	placeholder="e-mail"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[email]',"<p class='bg-danger'>","</p>"); ?>

					<label for="telefono">
						<input
							class="col-xs-12"
							type="text" 
							id="telefono" 
							name="inscripcion[telefono]" 
					    	value="<?php echo $inscripcion->get_telefono() ?>" 
					    	placeholder="télefono"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[telefono]',"<p class='bg-danger'>","</p>"); ?>


					<label for="curso">
						<select name="inscripcion[curso]" id="curso" class="form-control">
								<option value="0" data-infoclases="0=Fecha y lugar"> Seleccione un curso </option>
							<?php foreach($cursos as $curso): ?>
								<option value="<?= $curso->get_id() ?>" <?php echo ($curso->get_id() == $curso_selected) ? 'selected' : '' ?> data-infoclases="<?= $curso->get_info_clases_pendientes(); ?>"> <?= $curso->get_nombre() ?> </option>
							<?php endforeach ?>
						</select>
					</label>
					<?php echo form_error('inscripcion[curso]',"<p class='bg-danger'>","</p>"); ?>

					<label for="clase">
						<?php $clases= array('0' => "Fecha y lugar",);?>
			          	<?php echo form_dropdown('inscripcion[clase]', 
			                                  $clases, 
			                                  null,
			                                  "class=form-control id=clase data-selected=".$clase_selected) 
			          	?>
					</label>
					<?php echo form_error('inscripcion[clase]',"<p class='bg-danger'>","</p>"); ?>

 					<div class="radio">
						<label class="radio-inline col-xs-5">
							<input type="radio" name="inscripcion[tipo_trabajo]" id="tipo_trabajo_1" value="1" <?= ("1" == $tipo_trabajo_selected) ? 'checked' : '' ?> > 
							Municipio
						</label>
						<label class="radio-inline col-xs-5">
							<input type="radio" name="inscripcion[tipo_trabajo]" id="tipo_trabajo_2" value="2" class="pull-left" <?= ("2" == $tipo_trabajo_selected) ? 'checked' : '' ?>> 
							Otro
						</label>
						<?php echo form_error('inscripcion[tipo_trabajo]',"<p class='bg-danger'>","</p>"); ?>
					</div>
				

					 <div id="campo_municipio" style="<?php echo ("1" == $tipo_trabajo_selected) ? 'display:block' : 'display:none' ?>">
						<label for="municipio">
							<select name="inscripcion[municipio]" id="municipio" class="form-control">
								<option value="0" > Seleccione un municipio </option>
								<?php foreach($municipios as $municipio): ?>
									<option value="<?= $municipio->get_id() ?>" <?php echo ($municipio->get_id() == $municipio_selected) ? 'selected' : '' ?>> <?= $municipio->get_nombre() ?> </option>
								<?php endforeach ?>
							</select>
						</label>
						<?php echo form_error('inscripcion[municipio]',"<p class='bg-danger'>","</p>"); ?>
					</div> 

					<div id="campo_lugar_trabajo" style="<?php echo ("2" == $tipo_trabajo_selected) ? 'display:block' : 'display:none' ?>">
						<label for="lugar_trabajo">
							<input 
								class="col-xs-12"
								type="text" 
								id="lugar_trabajo" 
								name="inscripcion[lugar_trabajo]" 
						    	value="<?php echo $inscripcion->get_lugar_trabajo() ?>" 
						    	placeholder="Lugar de trabajo"
						    	autocomplete="off"
							/>
						</label>
						<?php echo form_error('inscripcion[lugar_trabajo]',"<p class='bg-danger'>","</p>"); ?>
					</div>

					<label for="oficina">
						<input 
							class="col-xs-12"
							type="text" 
							id="oficina" 
							name="inscripcion[oficina]" 
					    	value="<?php echo $inscripcion->get_oficina() ?>" 
					    	placeholder="Oficina"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[oficina]',"<p class='bg-danger'>","</p>"); ?>

					<label for="cargo">
						<input 
							class="col-xs-12"
							type="text" 
							id="cargo" 
							name="inscripcion[cargo]" 
					    	value="<?php echo $inscripcion->get_cargo() ?>" 
					    	placeholder="Cargo"
					    	autocomplete="off"
						/>
					</label>
					<?php echo form_error('inscripcion[cargo]',"<p class='bg-danger'>","</p>"); ?>


				</div>


					<button
						type="submit" 
						class="btn btn-primary pull-right">
						Inscribirse
					</button>
		<?php echo form_close();?>	  