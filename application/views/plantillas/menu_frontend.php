<div class="menu_azul">
	<div class="container">
		<div class="col-xs-1" id="icono_home">
			<a href="<?=base_url()?>" title="Inicio" data-toggle="tooltip" data-placement="top" ><span class="glyphicon glyphicon-home"></span></a>
		</div>
		<div class="col-xs-1" id="loguito_rafam_menu">
			<a  href="<?=base_url()?>" title="Inicio" data-toggle="tooltip" data-placement="top"><img src="<?=base_url()?>assets/imagenes/loguito_rafam_25px.png"></a>
		</div>
		
		<div class="col-xs-11">
			<div class="row">
				<a class="mobile-menu touch-menu" href="#"><span class="glyphicon glyphicon-tasks azul_rafam"></span></a>
				<nav>
				    <ul id="menu" class="menu pull-right">
						<li><a href="javascript:void(0)">La Reforma</a>
						   <ul class="sub-menu">
							   <li><a href="<?=base_url('reforma/evolucion')?>">Evolución</a></li>
							   <li><a href="<?=base_url('reforma/mision_y_vision')?>">Misión y Visión</a></li>
							   <li><a href="<?=base_url('reforma/alcance')?>">Alcance</a></li>
					  		</ul>
						</li>

						<li><a href="javascript:void(0)">El software</a>
							<ul class="sub-menu">
							   <li><a href="<?=base_url('software/descripcion')?>">Descripción</a></li>
							   <li><a href="<?=base_url('actualizacion')?>">Actualizaciones</a></li>
							   <li><a href="<?=base_url('software/documentacion')?>">Documentación</a></li>
					  		</ul>
						</li>

						<li>
							<a  href="<?=base_url('normativa')?>">Normativa</a>
						</li>

						<li><a  href="javascript:void(0)">Capacitación</a>
							<ul class="sub-menu">
								<li><a href="<?=base_url('capacitacion/objetivos')?>">Objetivos</a></li>
								<li><a href="<?=base_url('capacitacion/cronograma')?>">Cronograma</a>
								<li><a href="<?=base_url('capacitacion/inscripcion')?>">Inscripción</a>
						   </ul>
						</li>

						<li>
							<a  href="http://www.simco.rafam.ec.gba.gov.ar/" target="_blank">SIMCo</a>
						</li>

						<li>
							<a  href="<?php echo base_url('noticia')?>">Noticias</a>
						</li>

					 	<li>
						 	<a  href="<?=base_url('inicio/login')?>"  id="login_menu_frontend"></i>Uso interno</a>
						</li> 
					</ul>
				</nav>

			</div><!-- CIERRA row -->
		</div><!-- CIERRA COL XS 11 -->
	</div><!-- CIERRA CONTAINER -->
</div> <!-- CIERRA MENU -->
<?php $this->load->view('plantillas/flashes'); ?>