<?php if($noticia=$this->session->flashdata('noticia')){ ?>
	<div class="alert alert-success fade in">
	    <button class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <span class="glyphicon glyphicon-ok-sign"></span> <?= $noticia ?>
	</div>
<?php } ?>

<?php if($advertencia=$this->session->flashdata('advertencia')){ ?>
	<div class="alert alert-warning fade in">
	    <button class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <span class="glyphicon glyphicon-exclamation-sign"></span> <?= $advertencia ?>
	</div>
<?php } ?>

<?php if($error=$this->session->flashdata('error')){ ?>
	<div class="alert alert-danger fade in">
	    <button class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <span class="glyphicon glyphicon-remove-sign"></span> <?= $error ?>
	</div>
<?php } ?>