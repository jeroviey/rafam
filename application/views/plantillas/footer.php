	<footer class="azul_rafam_BG footer_rafam">
				<div class="container">
					<div class="row">
						<div class="col-xs-10">
							<div class="col-xs-12 col-sm-5 ">
								<!-- <h5>CONTACTO:</h5> -->
								<a href="https://www.google.com.ar/maps/place/Ministerio+de+Econom%C3%ADa+de+la+Provincia+de+Buenos+Aires/@-34.912472,-57.954195,15z/data=!4m2!3m1!1s0x0:0x293ba721910fb262" target="_blank"><h5 class="blanco">DIRECCIÓN DE SISTEMAS DE INFORMACIÓN FINANCIERA MUNICIPAL</h5></a>
								<p><span class="glyphicon glyphicon-map-marker"></span>Calle 45 entre 7 y 8 - La Plata (1900)</p> 
								<p>3er Piso - Pasillo Amarillo - Oficina 319</p>
								<p><span class="glyphicon glyphicon-phone"></span>(0221) 429-4484 / 4509</p>
								<p><span class="glyphicon glyphicon-envelope"></span>cai@ec.gba.gov.ar</p>
								<a href="https://twitter.com/rafam_ba" target="_blank"><img src="<?php echo base_url('assets/imagenes/loguito_twitter.png')?>"> /rafam_ba</a>

							</div>
							<div class="hidden-xs col-sm-3 col-sm-offset-4">
								<a href="<?=base_url('inicio/preguntas_frecuentes')?>">
									<h5 class="blanco">PREGUNTAS FRECUENTES</h5>
										<div class="texto_footer">
										<p>Configuración General</p> 
										<p>Bienes Físicos</p>
										<p>Contabilidad</p>
										<p>Contrataciones</p>
										<p>Administración de Personal</p>
										<p>Administración de Ingresos Públicos</p>
								</a>
								</div>
							</div>
						</div>
					</div>
				</div>
		</footer>
 
 		<footer class="naranja_BG">
				<div class="container">
					<div class="row">		
						<div class="col-sm-6 col-xs-12">
							<a href="https://www.google.com.ar/maps/place/Ministerio+de+Econom%C3%ADa+de+la+Provincia+de+Buenos+Aires/@-34.912472,-57.954195,15z/data=!4m2!3m1!1s0x0:0x293ba721910fb262" target="_blank"><h5 class="blanco">MINISTERIO DE ECONOMÍA - SUBSECRETARIA DE COORDINACIÓN ECONÓMICA</h5></a>
							<p class="blanco">Dirección Provincial de Coordinacion Municipal - Dirección de Sistemas de Información Financiera Municipal </p>
							<p class="blanco">RAFAM 1.0</p>
						</div>
						<div class="hidden-xs col-sm-2 pull-right">
							<img src="<?=base_url()?>assets/imagenes/logo_Activa_blanco_sinfondo.png">
						</div>
						<div class="hidden-xs col-sm-2 pull-right" id="logo_BA_footer">
							<img src="<?=base_url()?>assets/imagenes/logo_BA_blanco_sinfondo.png">
						</div>
					</div>
				</div>

		</footer>


	</body>
</html>