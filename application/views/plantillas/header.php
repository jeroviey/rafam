<!DOCTYPE html>
<html lang="ES">
<head profile="http://www.w3.org/2005/10/profile">
	<?php
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="icon" type="image/png" href="<?=base_url('assets/imagenes/loguito_rafam_azul_25px.png')?>" />
	<title>RAFAM - Reforma de la Administración Financiera</title>

	<!-- P A R A   R E S P O N S I V E -->
	<meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1">

<!-- 	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
 -->	<!-- BOOTSTRAP -->
	<link href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">

    <!-- JS -->
    <script src="<?=base_url();?>assets/js/jquery-1.11.0.min.js"></script>
	<script src="<?=base_url();?>assets/js/modernizr-2.5.3.js"></script>
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.js"></script>
    <script src="<?=base_url();?>assets/js/menu_desplegable.js"></script>    
    <script src="<?=base_url();?>assets/js/application.js"></script>
    <script src="<?=base_url();?>assets/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?=base_url();?>assets/js/scroller.js"></script>

    <!-- E S T I L O S (CSS) -->
    <link href="<?=base_url();?>assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>assets/css/fuentes.css" rel="stylesheet" type="text/css">
    <?php if($backend){ ?>
    <link href="<?=base_url();?>assets/css/estilos_backend.css" rel="stylesheet" type="text/css">
	<?php }else{ ?> 
	    <header class="azul_rafam_BG">
	    	<div class="container">

	    		<div class="col-xs-6 " id="logo_rafam_menu">
					<a href="<?=base_url()?>" data-toggle="tooltip" data-placement="bottom" title="Inicio"><img src="<?=base_url()?>assets/imagenes/logo_rafam_blanco_sinfondo.png"></a>
				</div>

				<div class="col-sm-6 col-xs-6 pull-right">
					<a class="pull-right" id="logo_BA_header" data-toggle="tooltip" data-placement="left" title="BA - Gob. Daniel Scioli" href="http://www.gba.gob.ar/" target="_blank"><img src="<?=base_url()?>assets/imagenes/logo_BA_FondoNaranja_chico_sombra.png"></a>
					<a class="pull-right" id="logo_min_eco_header" href="http://www.ec.gba.gov.ar/" target="_blank" >
	                    <h4 class="blanco right" data-toggle="tooltip" data-placement="left" title="Ministerio de Economía de la Provincia de Buenos Aires">MINISTERIO DE ECONOMÍA</h4>
	                </a>
				</div>
			</div>
		</header>

	<?php } ?>

</head>

<body>
<?php $this->load->view('plantillas/modal'); ?>
<?php $this->load->view('backend/plantillas/modal_backend'); ?>