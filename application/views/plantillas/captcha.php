<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<div class="form-group">
	<div class="recaptcha">
		<div class="g-recaptcha" data-sitekey="<?=$recapcha_key?>"></div>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=es-419">	</script>
	</div>
	<?php echo form_error("g-recaptcha-response","<p class='bg-danger'>","</p>"); ?> 
</div>